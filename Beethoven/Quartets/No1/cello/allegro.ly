cello-allegro = \new Voice \relative c {
  \set Staff.instrumentName = #"Cello "
  \clef bass

  f4~ f8( g16 f) e8-. f-. |
  c4 r r |

  f4~ f8( g16 f) e8-. f-. |
  d4 r r |

  d2.\<( |
  e\>)( |

  f4\!)( d bf) |
  c2. | \break


  f4~\f f8( g16 f) e8-. f-. |
  c4 r r |

  f4~ f8( g16 f) e8-. f-. |
  d4 r r |

  r2. |
  << cs'2.\p { s8.\< s s\> s\! } >> |

  r2. |
  << fs2. { s8.\< s s\> s\! } >> |

  r2. |
  bf,4._"cresc." bf8 bf bf | \break

  
}
