\version "2.18.2"
\language "english"

\header {
  title = "“Le Trille du Diable”"
  subtitle = "pour Violoncelle"
  composer = "G. Tartini"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
  ragged-last-bottom = ##f
  bottom-margin = 24
}

global = {
  \key g \minor
  \numericTimeSignature
  \time 12/8
  \tempo "Larghetto Affetuoso."
}

cello = \relative c' {
  \global
  % Music follows here.
  \clef tenor
% line one		bar 1
  R1*12/8
  d8.-1_\markup{\italic \dynamic p "espressivo"}\downbow ef16-- d8-- ef16-1-- f--\< g8( ef\!)-2 ef(\> d2\!) d8(-4
  c4 c8)^\markup{\raise #1.5 \small "D"} bf8.(_\markup{\italic "cresc."} \startTextSpan a16)-- bf8 bf(\> a4~ a4.\!)
\break % line two	bar 4
  bf8.-1 c16-- bf8 b4.-1 c8.( b16)-- c8\< c4\upbow cs8\!\upbow
  d8-- c!16( bf a) g( bf8. c16) a8 g2. \stopTextSpan
  f'8.\pp g16( f8) g16-- a bf8 g~ g8 f2 f8--
\break % line three	bar 7
  ef4~ ef8^\markup{\small "D"} \startTextSpan df8. c16-- df8 df( c4~ c4.) \stopTextSpan
  bf8. c16-- bf8 f'( d bf) bf4( a8) bf4.
  bf8.\downbow\< c16--\downbow <<bf8 {s16 s\sf\>}>> g'16(-- f-- ef)-- d-- c(^\markup{\small "D"} bf\!) bf4( a8) bf4.
\break % line four	bar 10
  g'4\upbow g8 c,( d bf) a4( bf8--) c4( ef8)
  ef8. c16-- bf8 \acciaccatura c8~ c4.\trill( \grace {bf16) c^(} bf8^)->
          <g g'>4->\upbow\f\< <g g'>->\downbow <g g'>8--\downbow\!
  \acciaccatura f8 <f f'>8--\upbow ef'16( d) c bf\downbow c4.(\trill \grace {bf16) c} bf8->
          <g g'>4->\upbow\f\< <g g'>->\downbow <g g'>8--\downbow
\break % line five	bar 13
  \acciaccatura f8 <f f'>8--\downbow\> ef'16--\downbow d\p( c bf) \grace{bf16_( c\downbow}
          c4.\trill)( \grace {bf16 c)} <d, bf'~>4. bf'4 r8
  \repeat volta 2 { \bar "[|:"
    b8.(\upbow c16)-- b8 f'4( b,8) c8. b16-- c8 c4.
    cs8. d16-- cs8 cs4( g'8) fs8. e16 d8 d4.
\break % line six	bar 16
    << \new Voice = "top" { \stemUp \tieUp \slurUp
      d8. d16 d8 d4~ d8 ef8. f16 ef8 ef4( d8)
      c8. c16 c8 c4~ c8 d8. ef16 d8 d4( c8)
      bf8. bf16 bf8 bf4~ bf8 c8. d16 c8 c4( bf8)
      }
       \new Voice = "bottom" { \stemDown \tieDown \slurDown
      bf8. c16 bf8 bf4( a8) g8. g16 g8 g4~ g8
      a8. bf16 a8 a4( g8) f8. f16 f8 f4~ f8_\markup{\italic "cresc.   poco   a   poco"}
      g8. a16 g8 g4( f8) ef8. ef16 ef8 ef4~ ef8
      }
    >>
\break % line seven	bar 19
    << \new Voice = "top" { \stemUp \tieUp \slurUp
      a8. bf16 a8 a4( c8)
      }
       \new Voice = "bottom" { \stemDown \tieDown \slurDown
      fs,8. g16 fs8 fs4( <a, d>8)
      }
    >>
    <g c'>8(\f bf'16) a-- g8\downbow g4 bf8\downbow^\markup{\small "D"}
    a8 g16( fs ef d) <<d4 {s8 s\p\<} >> c'8\downbow bf4 \acciaccatura ef,8 ef'\upbow d4 fs8--\!
    g2.~\downbow\mf\< << g4.~ {s8 s s\upbow} >> g4 ef8\downbow\!
\break % line eight	bar 22
    d8. c16( bf a32 g) a4.(\trill \grace {g16 a)} g8
          <g, e' e'>4^>\upbow <g e' e'>^>\downbow <e' e'>8_-\downbow
  \acciaccatura d8 <d d'>8.\upbow\> c'16( bf\! a32)\p g( bf8.) c16( a8) g
          <g, e' e'>4^>\downbow <g e' e'>^>\upbow <e' e'>8_-\upbow
  }
  \alternative {
    {\acciaccatura d8 <<d'8. {s16 s s\>}>> c16( bf a32) g(\! bf8.)\p c16--\downbow a8\upbow <g, g'>2.\downbow}
    { \stemNeutral \acciaccatura d'8 <d d'~>4.\upbow\f d'8[ c16(\> bf a)}
  }
  \stemDown g16]\! bf8.\downbow c16(\upbow g8)\> \stemNeutral \grace {g32(_\(\downbow a bf a}
      \afterGrace a4.)\trill { g32 bf a\)}
  <g, g'>1.\downbow\fermata\pp \bar "|."
  % fin
}

\score {
  \new Staff \with {
    instrumentName = ""
  } {\cello }
  \layout { }
}
