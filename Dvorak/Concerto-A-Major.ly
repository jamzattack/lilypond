\version "2.19"
\language "english"

\header {
  title = "Concerto in A Major"
  subtitle = "for Violoncello"
  composer = "Antonín Dvořák"
  tagline = ##f
}

\paper {
  #(set-default-paper-size "a4")
}

global = {
  \compressFullBarRests
  \set Score.markFormatter = #format-mark-circle-numbers
  \autoPageBreaksOff
  \set Score.barNumberVisibility = #all-bar-numbers-visible
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \set Score.barNumberVisibility = #(every-nth-bar-number-visible 5)
  \override Score.BarNumber.self-alignment-X = #LEFT
}

\include "Andante.ly"
\include "Andante-Cantabile.ly"
\include "Rondo.ly"

\markup \abs-fontsize #20 {
  \fill-line {
  [I]
} }

\score {
  \new Staff \Andante
  \layout {}
}

\markup \abs-fontsize #20 {
  \fill-line {
  [II]
} }

\score {
  \new Staff \Andante-Cantabile
  \layout {}
}

\markup \abs-fontsize #20 {
  \fill-line { \center-column {
  [III] \line {
  Rondo
} } } }

\score {
  \new Staff \Rondo
  \layout {}
}

