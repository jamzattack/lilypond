\version "2.19"
\language "english"

\header {
  title = "Terzetto"
  subtitle = "für Violine, Violoncello und Gitarre"
  composer = "N. Paganini"
  tagline = "Violoncello"
}

\paper {
  #(set-default-paper-size "a4")
  page-breaking = #ly:minimal-breaking
}

global = {
  \override Score.BarNumber.font-size = #1.5
  \override Score.BarNumber.stencil
    = #(make-stencil-boxer 0.1 0.25 ly:text-interface::print)
  \set Score.barNumberVisibility = #all-bar-numbers-visible
  \override Score.BarNumber.break-visibility = #end-of-line-invisible
  \override Score.BarNumber.self-alignment-X = #LEFT
  \compressFullBarRests
  \override NonMusicalPaperColumn.line-break-permission = ##f
  \override NonMusicalPaperColumn.page-break-permission = ##f
}

Allegro = \relative c' {
  \global
  \key d \major
  \time 4/4
  \clef tenor
  \tupletSpan 4
  \repeat volta 2 {
    d2 fs |
    \set Score.barNumberVisibility = #(every-nth-bar-number-visible 5)
    a2. g8 fs |
    fs8( ds) e2 g8 fs |
    e8( cs) d4. r8 r4 |
%bar 5
    d2 fs |
    \clef treble
    d'2. cs8 b | \break
    b8( a) gs a cs b a gs |
    a8 r r4 a'8 r8 r4 |
    \clef bass
    <d,,,, a' fs'>^\markup{\italic "pizz."} r d' r |
%bar 10
    <d, a' fs'> r d' r |
    cs r cs, r |
    d r a'' fs | \break
    ds r ds, r |
    e r g g' |
%bar 15
    a r a, r |
    <d, a' fs'> r r2 |
    r4 d'8^\markup{\italic "arco"} r8 cs2^>( |
    d8) r r4 r2 |
    r4 d8 r cs2^>( | \break
%bar 20
    d8) r r4 r2 |
    r4 b8 r as2^>( |
    b8) r r4 r2 |
    b'8^\markup{\italic "pizz."}_\markup{\italic "dolce"} r r4 d8 r8 r4 |
    cs8 r r4 a8 r r4 |
%bar 25
    fs8 r r4 b,2^>^\markup{\italic "arco"}( | \break
    e8) r e'\f r ds r d r |
    cs r a r gs r a r |
    e r e r ds r d r |
    \set Score.currentBarNumber = #30
%bar 30
    cs r a r gs r a r | \break
    e' r r4 r \tuplet 3/2 { b'8\f( a g) }
    e8 r r4 r \tuplet 3/2 { b8( a g) } |
    e8 r r4 r b |
    e8 r r4 <b' gs' e'>8 r8 r4 |
%bar 35
    e8 r r4 \clef tenor e'2 |
    fs-> \afterGrace gs(\trill  { fs32 gs } | \break
    b8) r a2-> gs8-. fs-. |
    fs e e2-> d8-. b |
    a8 r r4 r2 |
    \clef bass
%bar 40
    a,4\p^\markup{\italic "pizz."} r cs, r |
    d r r2 |
    e4 r e' r | \break
    a,4 r r2 |
    a4 r cs r |
%bar 45
    fs r a, r |
    gs a b b |
    e, r r2 |
    b'4 r e, r |
    a r r2 | \break
%bar 50
    b4 r e, r |
    a r r r8 cs |
    d4 r r2 |
    cs4 r d r |
    e r e, r |
%bar 55
    a4 r r2 |
    gs2\p^\markup{\italic "arco"}( b8) r r4 | \pageBreak

    a8 r r4 r2 |
    gs2( b8) r r4 |
    a8 r r4 r r8 cs^\markup{\italic "pizz."} |
%bar 60
    d4 r r2 |
    cs r d r |
    e r e, r | \break
    a4 a'8^\markup{\italic "arco"} r gs r g r |
    fs r b r as r a r |
%bar 65
    gs r e r ds r d r |
    cs r d r e r e, r |
    a4 a'8 r gs r g r |
    fs r b r as r a r |
    gs r e\f^\markup{\italic "arco"} r ds r d r |
%bar 70
    cs8 r d r e r e, r | \break
    a8 r r4 r \tuplet 3/2 { e''8 d b } |
    a8 r  r4 r \tuplet 3/2 { e8 d b } |
    a8 r r4 r2 |
    \tuplet 3/2 { a8 cs e a cs e a e cs a e cs }
%bar 75
    a2 r | \break
  }
  \repeat volta 2 {
    \clef tenor
    fs''2_\markup{\italic "dolce"}^\markup{\italic "Solo"} as |
    cs2. b8( as) |
    as( fss) gs2 b8 a |
    gs8( es) fs2 r4 |
%bar 80
    \clef bass
    <d,, a' fs'>4^\markup{\italic "pizz."}_\markup{\italic "dolce"} r r2 |
    <d a' fs'>4 r d'' r |
    <cs,, a'> r cs'' r | \break
    d,, r <d a' fs' d'> r |
    \clef tenor
%bar 85
    b''2 d |
    fs2. e8( d) |
    d( bs) cs2 e8( d) |
    cs( as) b2 d8( c) |
    b8 gs a2 b8(_\markup{\italic "cresc."} c) | \break
    d4( g2) fs8 e |
%bar 90
    e( cs) d2 c4 |
    b4( g'2) fs8 e |
    e8( cs) d2( c8 a) |
    g8 r r4 r2 |
    \clef bass |
    r4 g,8\f r fs2( | \break
%bar 95
    g8) r r4 r2 |
    r4 g8 r fs2( |
    gs8) r r4 r2 |
    r4 g8 r ds2( |
    e8) r r4 r2 |
%bar 100
    <c c'>4^\markup{\italic "pizz."} r c'' r |
    b4 r c r | \break
    r4 cs,8 r cs2(^\markup{\italic "arco"} |
    b8) r b'\f r as r a r |
    g r e r fs r e r |
%bar 105
    b r b r as r a r |
    g r e r fs r e r | \break
    b'\ff r r4 e,8 r r4 |
    b'8 r r4 e,8 r r4
    b'8 r r4 r2 |
%bar 110
    b8 r r4 b'8 r r4 |
    b,2 r |
    R1*3 |
%bar 115
    R1 | \pageBreak

    \clef tenor
    e'2(~ e8 \acciaccatura fs e16 ds e8[) r16 fs] |
    g2(~ g8 \acciaccatura a g16 fs g8[) r16 a] |
    b2( a4. c8) |
    b2.. r16 c |
%bar 120
    b2( a4) r8 b |
    a2( g4) r8 g | \break
    fs2( g8 a16 g fs8 e) |
    fs2. r4 |
    \clef bass
    e,,4^\markup{\italic "pizz."} r <g' b> r |
%bar 125
    e,4 r <g' b> r |
    g, <g' b> d, <fs' c'>
    g, b' g d |
    g d b g | \break
    fs fs' d b' |
%bar 130
    fs r fs, r |
    b b' r2 |
    \clef tenor
    b'2\<^\markup{\italic "arco"}(~ b8 \acciaccatura c b16 as b8[) r16 cs] |
    d2\> a |
    a4\!~ a a4.( b8) | \break
%bar 135
    c2 g~ |
    g4 g g4.( a8) |
    b2( fs4.) fs8 |
    g4.( a16 g fs4) e |
    fs2 r |
%bar 140
    e2(~ e8 \acciaccatura fs e16 ds e8[) r16 fs] | \break
    gs2(~ gs8 \acciaccatura as gs16 fs gs8[) r16 a] |
    b2( e4. d8) |
    d2 c |
    c->_\markup{\italic "decresc."} a4( f) |
%bar 145
    e4.( fs8) g2~ |
    g4 fs \acciaccatura fs8 e4 ds | \break
    b'1_\markup{\italic "cresc."}( |
    b4 c a) f |
    e4._\markup{\italic "decresc."}( fs8) g2^\markup{\italic "ritard. a piacere"} |
%bar 150
    g4 fs( c'\fermata) b |
    e,2^\markup{\italic "a tempo"} r |
    \clef bass
    g,,4 d'' b g |
    fs, fs' a d | \break
    f, c' a f |
%bar 155
    e, e' g c |
    e, b' g e |
    ds, r b'' ds, |
    e e' c, c' |
    b, b' a fs |
%bar 160
    e, e' b' gs | \break
    e_\markup{\italic "cresc."} b' gs e' |
    e b gs e |
    a e c a |
    f f' c' a |
%bar 165
    b,_\markup{\italic "decresc."} b' g e |
    b r b'r |
    g,_\markup{\italic "cresc."} e' b g | \break
    a_\markup{\italic "decresc."} r r r |
    b b' g^\markup{\italic "rit."} e |
%bar 170
    <b ds> r r2 |
    e,4 r r2 |
    \clef tenor
    e''2_\markup{\italic "dolce"}^\markup{\italic "arco"} gs |
    b2. a8 gs |
    gs( es) fs2 a8 gs | \break
%bar 175
    fs( ds) e2 r4 |
    \clef bass
    c,,^\markup{\italic "pizz."} r g'' r |
    e r c r |
    g r b r |
    c r <c, g' e' c'> r |
%bar 180
    \clef tenor
    a''2^\markup{\italic "arco"}_\markup{\italic "dolce"} c |
    e2. d8 c | \break
    c( as) b2 d8 cs |
    b( gs) a2 c8 bf |
    a( fs) g2 a8_\markup{\italic "cresc."} bf |
%bar 185
    c4( f2) e8 d |
    d( b) c2( bf4) |
    a( f'2) e8 d | \pageBreak

    d8( b) cs2 e,4 |
    f8 r r4 r2 |
%bar 190
    \clef bass
    r4 f,8\f r e2( |
    f8) r r4 r2 |
    r4 f8 r e2( |
    f8) r r4 r2 | \break
    r4 d8 r cs!2( |
%bar 195
    d8) r r4 r2 |
    bf'8^\markup{\italic "pizz."}_\markup{\italic "dolce"} r r4 bf'8 r r4 |
    f8 r r4 bf,8 r r4 |
    bf'8 r r4 bf,2^\markup{\italic "arco"}_\markup{\italic "cresc."}( |
    a8) r a' r gs r g r | \break
%bar 200
    f r d r cs! r d r |
    a8 r a r gs r g r |
    f r d r cs! r d r |
    a' r r4 <d, a' f'>8 r r4 | \break
    <cs! a' e'>8 r r4 <d a' f'>8 r r4 |
%bar 205
    <a'_4 cs^2>8 r r4 r2 |
    a,8 r r4 a'8 r r4 |
    a8 r r4 a'2-> |
    b-> \afterGrace cs\trill( {b32 cs} |
    e8) r d2-> cs8-. b-. | \break
%bar 210
    b8 r a2-> g8-. e-. |
    d r r4 r2 |
    d,4_\markup{\italic "dolce"}^\markup{\italic "pizz."} r fs r |
    g r r2 |
    a4 r r2 |
%bar 215
    d,4 r r2 |
    d4 r fs r | \break
    b r d r |
    cs d e e, |
    a r r2 |
%bar 220
    cs1\p^\markup{\italic "arco"}( |
    d8) r r4 r2 |
    cs1( |
    d8) r r4 r r8 fs,^\markup{\italic "pizz."} | \break
    g4 r r2 |
%bar 225
    fs4 r g r |
    a r a r |
    d, r r2 |
    cs'2^\markup{\italic "arco"}~ cs8 r r4 |
    d8 r r4 r2 | \break
%bar 230
    cs2~ cs8 r r4 |
    d8 r r4 r r8 fs,^\markup{\italic "pizz."} |
    g4 r r2 |
    fs4 r g r |
    a r a r |
%bar 235
    d,8 r d''^\markup{\italic "arco"} r cs r c r | \break
    b r e r ds r d r |
    cs r a r gs r g r |
    fs r g r a r a, r|
    d r d r cs r c r | \break
%bar 240
    b r e r ds r d r |
    cs r a'^\markup{\italic "arco"} r gs r g r|
    fs r g r a r a, r |
    d r r4 a8 r r4 |
    d8 r r4 <a e' cs'>8 r r4 | \break
%bar 245
    <d d'>8 r r4 <a e' cs'>8 r r4 |
    <a fs' d'>8 r r4 <a e' a>8 r r4 |
    <d, a' d>8 r r4 r2 |
    \tuplet 3/2 {d8\f^\markup{\italic "arco"} fs a} \omit TupletNumber
      \tuplet 3/2 {d fs a d a fs d a fs} |
    d2 r \bar "|." \pageBreak
  }
}

Minuetto = \relative c' {
  \global
  \key d \major
  \clef bass
  \time 3/4
  \tempo "Allegro Vivace"
  \repeat volta 2 {
    \partial 8 r8 |
%bar 1
    d,2._\markup{\italic "dolce"}~ |
  \set Score.barNumberVisibility = #(every-nth-bar-number-visible 5)
    d8 r r4 r |
    d2.~ |
    d8 r r4 r |
%bar 5
    d8 r r4 d8 r |
    r4 cs8 r r4 |
    cs2.( |
    fs,8) r r4 r | \break
    d'2.\fz~ |
%bar 10
    d8 r r4 r |
    d2.~ |
    d8 r r4 r |
    d8_\markup{\italic "dolce"} r r4 d8 r |
    r4 cs8 r r4 |
%bar 15
    cs2. |
    fs,8 r r4 \break
  }
  \repeat volta 2 {
    r4 |
    c''2._\markup{\italic "dolce"}( |
    b8) r r4 r |
    c2.( |
%bar 20
    b8) r r4 r |
    d2.( |
    cs!8) r r4 r |
    d2.(_\markup{\italic "cresc."} |
    cs!8) r r4 r |
%bar 25
    e2.( | \break
    d8) r r4 r |
    b8\p r r4 r |
    as2.( |
    a!8) r r4 r |
%bar 30
    gs2.( |
    g!8) r r4 r |
    fs2._\markup{\italic "cresc."}( |
    b,8) r r4 r | \break
    <a e'>2.( |
%bar 35
    <a e'>
    <a e'>
    <a e'>
    <a e'>8) r r4 r |
    R1*3/4
%bar 40
    R1*3/4
    d2._\markup{\italic "dolce"}~ |
    d8 r r4 r |
    d2.~ |
    d8 r r4 r | \break
%bar 45
    d8 r r4 d8 r |
    r4 g8 r r4 |
    e8 r r4 e8 r |
    r4 a8_\markup{\italic "cresc."} r r4 |
    a8 r r4 r |
%bar 50
    b8\f r r4 r |
    g8 r r4 r |
    a8 r r4 r | \break
    a8 r r4 r |
    <d, a'>2._\markup{\italic "dolce"}( |
%bar 55
    <d a'> |
    <d a'> |
    <d a'>) |
    <d,( a'(>\pp |
    <d a'> |
%bar 60
    <d) a')>8 r r4 r |
    <a' e' cs'>8\ff r r4 r |
    <d, a' fs' d'>8 r r4_\markup{\bold "Fine"} \break \bar ":|."
  }
  \repeat volta 2 {
    \key g \major
    d'4-._\markup{\italic "dolce"}^\markup{\large\bold "Trio"} |
    \set Score.barNumberVisibility = #all-bar-numbers-visible
    \set Score.currentBarNumber = #1
%bar 1
    d2.~ |
    \set Score.barNumberVisibility = #(every-nth-bar-number-visible 5)
    d4 e8-. fs-. g-. a-. |
    b( c) d2~ |
    d8 r e( d) as[( b]) |
%bar 5
    d( c) gs4( a) |
    r d8( c) gs[( a]) | \break
    c( b) fs[( g]) cs,!( d) |
    b4-. g^.( d'-.) |
    d2.~ |
%bar 10
    d4 e8-. fs-. g-. a-. |
    b( c) d2-> |
    r4 b8 cs d e |
    fs( g) fs2-> | \break
    r4 fs,8 g fs g |
%bar 15
    fs( g fs e cs d) |
    b4 r
  }
  \repeat volta 2 {
    cs'!8( d) |
    ds( e) b[( c]) gs( a) |
    fs4-. d-. d'\>~ |
    << d {s8 s\!} >> b8-. c d[( b]) | \break
%bar 20
    c( a) g4 cs8( d) |
    ds( e) b[( c]) gs( a) |
    fs4-. d-. d'->~ |
    d b8-. c-. d[( b]) |
    c( a) g4-. fs8-. g-. |
%bar 25
    a( g) fs( g f g) |
    e2 gs8-. a-. | \break
    b( a) gs( a e a) |
    g4( fs) as8-. b-. |
    c[( b]) as( b a b) |
%bar 30
    g2 b8-. c-. |
    d( c b c) g( c) |
    b4( a) cs8-.( d-.) |
    e( d e d) c-.[( b-.]) | \pageBreak

    d( c d c) b[( a]) |
%bar 35
    g[( ds]) e-. fs-. g-. a-. |
    b_\markup{\italic "cresc."} c ds e fs g |
    g( fs e) d-. cs-. d-. |
    e( d) b( c) a-. d-. |
  }
  \alternative {
    {g,2. | r4 r \break}
%bar 40
    {g2. | r4 }
  }
  a8-. b-. c-. d-. |
  ds4-. e2-> |
  r4 e8-. d-. c-. b-. |
  d4 c2-. |
%bar 45
  r4 gs8-. a-. b-. c-. | \break
  b( d cs d) c-.[ a-.] |
  b( as b g) a[ fs] |
  g2.~ |
  g4 a8( b) c-. d-. |
%bar 50
  ds4 e2-> |
  r4 e8-. d-. c-. b-. | \break
  d4-. c2-> |
  r4 gs8-. a-. b-. c-. |
  b\f( d fs g) e-.[( c-.]) |
%bar 55
  b( cs e d) c-.[( a-.]) |
  g2. |
  r4  r  \bar "||"
}

Andante = \relative c' {
  \global
  \key a \major
  \time 9/8
  \clef tenor
  \tempo "Cavate"
  \repeat volta 2 {
%bar 1
    e4. e fs8( e ds) |
    \set Score.barNumberVisibility = #(every-nth-bar-number-visible 5)
    e4.~( e4 d8) cs4 r8
    cs4 d8( b4 fs'->) e8 ds |
    e8. fs16-.( d-. e-.) cs4 r8 r4 r8 |
%bar 5
    e4. e( a8) fs e | \break
    ds8. e16-.( fs-. gs-.) a4 b8 a4 g8 |
    b8( a) fs a( g) e gs( fs) ds |
    fs4.\trill e4. r4 r8 |
  }
  \repeat volta 2 {
    d8( cs) d b4 cs8 d( fs) e |
%bar 10
    cs4( d16) ds e4.( a16 gs) fs-.( e-. d-. cs-.) | \break
    e8._>( d16) cs-.( d-.) e8._>( d16) cs-.( d-.) b8( fs'_>) e16-.[( d-.]) |
    cs8. cs16-.( d-. ds-.) e4 r8 a\< gs g |
    g4\! fs16-.( e-.) es4\>( fs16) r\! b8 as a |
    a4 gs16-.( fs-.) fs4 e8 fs-.( gs-. a-.) | \break
%bar 15
    gs8. fs16-.( e-.) d( fs8.) e16-.( d-.) cs( e8.)  d16-.( cs-. b-.) |
  }
  \alternative {
    {\acciaccatura cs8 b4.\trill a r4 r8}
    {a4 r8}
  }
  cs'8._>( b16-. a-. gs-. fs-. e-. d-. cs-. b-. cs-.) |
  b4->( a16) r a'8.( gs16-. fs-. e-. d-. cs-. b-. a-. gs-. a-.) | \break
  fs4\f\> gs16-.( a-. b-. cs-. d-. e-. fs-. gs-.) \clef treble
    a-.( b-. cs-. d-.^\markup{\italic "ritard."} e-. fs-.\!) \clef tenor |
  fs,8_>^\markup{\italic "a tempo"}( e) cs-. fs8_>( e) cs-. e_>( d) b-. |
%bar 20
  cs4 r8 cs'8._>( b16-. a-. gs-. fs-. e-. d-. cs-. b-. cs-.) | \break
  b4->( a16) r a'8.( gs16-. fs-. e-. d-. cs-. b-. a-. gs-. a-.) |
  fs4\f\> gs16-.( a-. b-. cs-. d-. e-. fs-. gs-.) \clef treble
    a-.( b-. cs-. d-.^\markup{\italic "ritard."} e-. fs-.\!) \clef tenor |
  fs,8_>^\markup{\italic "a tempo"}( e) cs-. fs8_>( e) cs-. e_>( d) b-. | \break
  d4 r8 fs8\pp( e) cs-. e( d) b-. |
%bar 25
  cs4 r8 \clef treble fs'( e) cs-. e( d) b-. |
  a4 r8 a'4\flageolet r8 a4\flageolet r8 |
  a2_\markup{\italic "morendo"}~ a4 r8\fermata \bar "|." \pageBreak
}

Rondo = \relative c' {
  \global
  \time 6/8
  \key d \major
  \clef bass
  \tempo "Allegretto"

  \partial 8 r8
  d,,4_\markup{\italic "dolce"} r8 r4 r8|
  \set Score.barNumberVisibility = #(every-nth-bar-number-visible 5)
  d4 r8 r4 r8|
  cs4 r8 r4 r8|
  d4 r8 r4 r8|
%bar 5
  d4 r8 r4 r8|
  d4 r8 r4 r8|
  e4 r8 e4 r8 | \break
  a4 r8 r4 r8 |
  d,4 r8 <fs' d'>4 r8 |
%bar 10
  d,4 r8 <fs' d'>4 r8 |
  cs,4 r8 <e' cs'>4 r8 |
  d,4 r8 <fs' d'>4 r8 |
  d,4 r8 <fs' d'>4 r8 |
  d,4 r8 <fs' d'>4 r8 |
% bar 15
  <e, e' a>4 r8 <e b' gs'>4 r8 | \break
  a4 r8 r4 a'8^\markup{\italic "arco"}_\markup{\italic "dolce"} |
  e'4( fs8-.) g4( fs8-.) |
  e4.( a,8) r a |
  \appoggiatura {a32 cs} b4( a8) a-. b-. cs-. |
%bar 20
  d8-.[( r e-.]) fs8 r r |
  b,4\f r8 cs4 r8 |
  d4 r8 e,4 fs16( g) | \break
  a8.( b16) cs-. d-. e8( g) cs, |
  d d-.( \acciaccatura fs e-.) d8 r a_\markup{\italic "dolce"} |
%bar 25
  e'4( fs8-.) g4( fs8-.) |
  e4.( a,8) r a |
  \appoggiatura {a32 cs} b4 a8 a b cs |
  d4( e8-.) fs r r | \break
  b,4\f r8 cs4 r8 |
%bar 30
  d4 r8 e,4 fs16( g) |
  a8.( b16) cs-. d-. e8( g) cs, |
  d,,\ff <fs' d'> r d, <fs' d'> r |
  d, <fs' d'> r d, r r |
  <cs a'>[ r <cs a'>] r <cs a'> r | \break
%bar 35
  <cs a'>4.~ <cs a'>8 r r |
  d <fs' d'> r d, <fs' d'> r |
  d, <fs' d'> r d, r r |
  <as' fs'>8[ r <as fs'>] r <as fs'> r |
  <as fs'>4.~ <as fs'>8 r r |
%bar 40
  b8 b' r d, r r | \break
  e4.( gs8) r r |
  a, a' r a, r r |
  b4.( b'8) r r |
  e,2.~ |
%bar 45
  e |
  e,~ |
  e~ |
  e8 r r r4 r8 |
  a4.~\ff a8 r r | \break
%bar 50
  gs8 r r a r r |
  d r r e r r |
  a, r r r4 r8 |
  \clef treble
  cs''4._\markup{\italic "dolce"}~ cs8 d-.( cs-.) |
  b4.~ b4 fs8 |
%bar 55
  gs4. \grace a8 g-.( fs-. e-.) | \break
  e4.( a4) r8 |
  cs4.~ cs8 bs-.( cs-.) |
  e4.~ e4 cs8 |
  bs4( cs8) ds( fs) e |
%bar 60
  cs2. |
  b4.~ b8 cs-.d-. |
  fs8 e4.-> d8-. cs-. | \break
  cs8-. as-. b4-> d8-.( cs-.) |
  gs-.( a-.) e4-> r8 r |
%bar 65
  b'4.~ b8 cs-.( d-.) |
  fs8 e4.-> d8-. cs-. |
  cs8-. as-. b4 d8-.( cs-.) |
  a8 r r a\f( b) a-. | \pageBreak

  a8 r r a( b) a-. |
%bar 70
  a8 r a'4-> gs8-.( fs-.) |
  fs( e) cs-. e( d) b-. |
  bs4 cs8 a8( b) a-. |
  bs4 cs8 a8( b) a-. |
  a'2.\ff | \break
%bar 75
  a4 fs8( a) gs fs-. |
  e4 fs16( gs) a( gs fs) e-. ds-. e-. |
  fs16( e d!) cs-. bs-. cs-. d-. cs-. b-. a-. gs-. fs-. |
  e8-. e'-. cs-. a'-. a,-. d-. | \break
  \acciaccatura cs8 b2.\trill |
%bar 80
  a4. r4 r8 |
  \clef bass
  a,,4^\markup{\italic "pizz."}_\markup{\italic "dolce"} r8 a'4 r8 |
  ds,,4 r8 <b' fs'> 4 r8 |
  e,4 r8 <e' d'>4 r8 |
  a,4 r8 <e' cs'>4 r8 |
%bar 85
  a,4 r8 a'4 r8 |
  gs,4 r8 cs'4 r8 | \break
  gs,4 r8 bs'4 r8 |
  cs4 r8 cs,4 r8 |
  d4 r8 e4 r8 |
%bar 90
  cs4 r8 b4 r8 |
  gs4 r8 e4 r8 |
  a4 r8 cs4 r8 |
  d4 r8 e4 r8 | \break
  cs4 r8 a4 r8 |
%bar 95
  gs4 r8 e4 r8 |
  a4 r8 g'!8^\markup{\italic "arco"} r r |
  fs r r cs r r |
  d r r ds4.( |
  e8) r r e, r r | \break
%bar 100
  a8 r r g'! r r |
  fs r r cs r r |
  f\ff r r e r r |
  ds4.~ ds8 r r |
  e4 r8 r4 r8 |
%bar 105
  R1*6/8
  e,8 r r r4 r8 | \break
  e'8 e r e, r r |
  a r r r4 r8 |
  d4.\ff\>~ d8\! r r |
%bar 110
  a8 r r r4 r8 |
  d4.\ff~ d8 r r |
  a8 r r d4.\ff-> |
  a8 r r d4.-> | \break
  a8 r r r4 r8 |
%bar 115
  R1*6/8*2
  d,4^\markup{\italic "pizz."}_\markup{\italic "dolce"} r8 r4 r8 |
  d4 r8 r4 r8 |
  cs4 r8 r4 r8 |
%bar 120
  d4 r8 r4 r8 |
  d4 r8 <fs' d'>4 r8 | \break
  d,4 r8 <fs' d'>4 r8 |
  <e, cs' a'>4 r8 <e b'gs'>4 r8 |
  a4 r8 r4^\markup{\italic "arco"} a'8_\markup{\italic "dolce"} |
%bar 125
  e'4( fs8-.) g4( fs8-.) |
  e4.( a,8) r a |
  \appoggiatura {a32 cs} b4( a8) a b cs |
  d-.[( r e-.]) fs r r | \break
  b,4\f r8 cs4 r8 |
%bar 130
  d4 r8 r4 fs,16( g) |
  a8.( b16) cs-. d-. e8( g) cs, |
  d d-.( \acciaccatura fs e-.) d r a_\markup{\italic "dolce"} |
  e'4( fs8-.) g4( fs8-.) |
  e4.( a,8) r a | \break
%bar 135
  \appoggiatura {a32 cs} b4( a8) a b cs |
  d-.[( r e-.]) fs r r |
  b,4\f r8 cs4 r8 |
  d4 r8 e,4 fs16( g) |
  a8.( b16) cs-. d-. e8( g) cs, |
%bar 140
  d4. r4 \bar "||" \pageBreak

  \key g \major
  \set Score.barNumberVisibility = ##f
  r8^\markup{\large\bold "Trio"} |
  \set Score.barNumberVisibility = #all-bar-numbers-visible
  \set Score.currentBarNumber = #1
  R1*6/8*3
  \set Score.barNumberVisibility = #(every-nth-bar-number-visible 5)
  g,,8^\markup{\italic "pizz."}_\markup{\italic "dolce"} b d g[ d]
  \repeat volta 2 {
    b8 |
%bar 5
    g4 r8 g'4 r8 |
    c,,4 r8 c'4 r8 |
    g4 r8 g'4 r8 |
    d,4 r8 d' c a | \break
    g4 r8 g'4 r8 |
%bar 10
    c,,4 r8 c'4 r8 |
    d4 r8 d,4 r8 |
    g'8 d b g4
  }
  \repeat volta 2 {
    r8 |
    b2.\p^\markup{\italic "arco"}~ |
    b8 r r r4 r8 |
%bar 15
    b' r r b, r r | \break
    e r r r4 r8 |
    a,2.~ |
    a8 r r r4 r8 |
    a' r r a, r r |
%bar 20
    d r r r4 r8 |
    g,4^\markup{\italic "pizz."} r8 g'4 r8 |
    c,,4 r8 c'4 r8 |
    g4 r8 g'4 r8 | \break
    d,4 r8 d' c a |
%bar 25
    g4 r8 g'4 r8 |
    c,,4 r8 c'4 r8 |
    d4 r8 d,4 r8 |
  }
  \alternative {
    {g'8 d b g4}
    {g8\ff^\markup{\italic "arco"}[ g']}
  }
  r8 f r r |
  e4. r4 r8 |
%bar 30
  a, a' r g r r |
  fs4. r4 r8 |
  b, b' r a r r |
  g r r g r r |
  fs2.~ |
%bar 35
  fs2. |
  fs,2.~ |
  fs2.~ | \break
  fs8 r r r4 r8 |
  b4.\ff->~ b8 r r |
%bar 40
  fs r r r4 r8 |
  b4.\ff->~ b8 r r |
  fs r r b4.-> |
  fs8 r r b4.-> |
  fs8 r r r4 r8 |
%bar 45
  R1*6/8*4  \bar "||" \break
  \key d \major
  d4^\markup{\italic "pizz."} r8 r4 r8 |
%bar 50
  d4 r8 r4 r8 |
  cs4 r8 r4 r8 |
  d4 r8 r4 r8 |
  d4 r8 <fs' d'>4 r8 |
  d,4 r8 <fs' d'>4 r8 |
%bar 55
  <e, cs' a'>4 r8 <e b' gs'>4 r8 |
  a4 r8 r4 a'8^\markup{\italic "arco"} | \break
  e'4_\markup{\italic "dolce"}( fs8-.) g4( fs8-.) |
  e4.( a,8) r a |
  \appoggiatura {a32 cs} b4( a8) a b cs |
%bar 60
  d-.[( r e-.]) fs r r |
  b,4\f r8 cs4 r8 |
  d4 r8 e,4 fs16( g) |
  a8.( b16) cs-. d-. e8( g) cs, | \break
  d d( \acciaccatura fs e-.) d r a_\markup{\italic "dolce"} |
%bar 65
  e'4( fs8-.) g4( fs8-.) |
  e4.( a,8) r a |
  \appoggiatura {a32 cs} b4( a8) a b cs |
  d-.[( r e-.]) fs r r |
  b,4\f r8 cs4 r8 |
%bar 70
  d4 r8 e,4 fs16( g) | \break
  a8.( b16) cs-. d-. e8( g) cs, |
  d\ff d r as r r |
  b b r fs r r |
  g g r gs r r |
%bar 75
  a2. |
  d,8 d r as r r | \break
  b b r fs r r |
  g g r gs r r |
  a2. |
%bar 80
  d8\ff d' r <a, e' cs'> r r |
  <a fs' d'> r r <a e' a> r r |
  d, d' r  <cs, a' e'> r r | \break
  <d b' fs'> r r <cs a' e'> r r |
  <d a' d>4 r8 r4 r8 |
%bar 85
  r4 r8 r4 a''8\ff-> |
  fs4-> d8-> a4-> fs8-> |
  d4. r4\fermata_\markup{\large\bold "Fine"} \bar "|." \pageBreak
%fin
}

\markup \abs-fontsize #20 {
  1. Allegro con brio
}
\markup \abs-fontsize #8 {
  "(numbers from bar 30 are shown one bar ahead. e.g. bar \"40\" is actually bar 39)"
}

\score {
  \new Staff \Allegro
}

\markup \abs-fontsize #20 {
  2. Minuetto
}

\score {
  \new Staff \Minuetto
}

\markup \abs-fontsize #20 {
  3. Andante Larghetto
}

\score {
  \new Staff \Andante
}

\markup \abs-fontsize #20 {
  4. Rondo
}

\score {
  \new Staff \Rondo
   \layout {
    \context {
      \Score
      \override NonMusicalPaperColumn.line-break-permission = ##f
      \override NonMusicalPaperColumn.page-break-permission = ##f
    }
  }
}
