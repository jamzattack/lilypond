\version "2.20"
\language "english"

\header {
  tagline = ##f
  title = "Concerto No. 1"
  opus = 107
  composer = "Dmitri Shostakovich"
}

global = {
  \compressFullBarRests
}

% Movement One
\include "1.ly"
\markup \fill-line {
  \center-column {
    \fontsize #5 { "I." }
  }
}
\score {
  \new Staff {
    \cello_one
  }
  \midi { }
  \layout { }
}

% Movement Two
\include "2.ly"
\markup \fill-line {
  \center-column {
    \fontsize #5 { "II." }
  }
}
\score {
  \new Staff {
    \cello_two
  }
  \midi { }
  \layout { }
}

