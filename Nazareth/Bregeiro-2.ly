\version "2.20"
\language "english"

\header {
  title = "Bregeiro"
  composer = "Ernesto Nazareth"
}

\include "2.ly"

breaks = {
  \time 2/4
% bar 1
  \repeat unfold 5 { s2 } \break

% bar 6
  \repeat unfold 5 { s2 } \break

% bar 11
  \repeat unfold 6 { s2 } \break

% bar 17
  \repeat unfold 5 { s2 } \break

% bar 22
  \repeat unfold 5 { s2 } \break

% bar 27
  \repeat unfold 6 { s2 } \break

% bar 33
  \repeat unfold 5 { s2 } \break

% bar 38
  \repeat unfold 5 { s2 } \break

% bar 43
  \repeat unfold 4 { s2 } \break

% bar 47
  \repeat unfold 4 { s2 } \break

% bar 51

}

\score {
  \new Staff << \II \breaks >>
}

