\version "2.19"
\language "english"

Rondo = \relative c' {
  \global
  \key a \major
  \time 6/8
  \clef bass
  \tempo "Allegro risoluto" 4=92
  R1*3/4*12 | \noBreak
  \mark #80
  R1*3/4*12 | \noBreak
  \mark \default
  R1*3/4*14 \noBreak
  \mark \default
  R1*3/4*15 | \noBreak
  \mark \default
  R1*3/4*19 | \break

  \mark \default \clef treble
  a'4.->_3\f fs-> |
  e16_2( fs e) d-. b-._4 gs-. \clef bass e_1( d) b-._4 gs-._1 e8-. |
  r8 e16_\thumb( e'_3) gs,( gs') b,( b') d,( d') f,( f') | \clef treble
  a,16( a') gs,( gs') b,( b') e,( e') r8 r\fermata | \break

  a,4->\p gs16( fs) fs4-> e16_3( ds) |
  e16( fs e d b_4\upbow gs) \clef bass e_1\downbow( d b_4 gs e8\upbow) |
  r8 e'^\markup{\italic"pizz."} gs b d f | \clef treble
  a_2 gs b e^3 r r\fermata | \break

  a,4.->\p^\markup{\italic"arco"} fs->_3 |
  e16( fs e cs a b) cs4( e8-.\upbow) |
  a4.-> fs-> |
  e16_2( ds gs fs e ds e4\upbow) cs8\downbow( |
%bar 85
  a'4) gs16( fs) fs4 e16( ds) |
  e16( fs e) d-. b-. gs-. \clef bass e4\upbow e'8\downbow( | \break

  \clef treble
  cs'4.->) a->_3 |
  gs16( fs^\thumb b a) cs,--^1^\markup{\small"I"} ds-- e4( es8) |
  \mark \default
  fs4.\pp(~ fs4 a8_3) |
%bar 90
  a4.(~ a4 fs8) |
  e4.\downbow~ e8 \clef bass a,,16\downbow cs e a |
  cs4.~ cs8 r r |
  cs4.(~ cs4 fs8) | \break

  fs4.(~ fs4 e8) |
%bar 95
  e4.~ e8 cs16\downbow a e cs |
  a4.~ a8 r8. fs''16 |
  fs4.(~ fs4 a8) |
  a4.(~ a4 fs8) |
  e4.~ e8 a,,16\downbow( cs e a) |
%bar 100
  cs4.~ cs8 r r | \break

  \mark \default \clef tenor
  cs16_2 cs, cs es gs cs cs fs^2 fs cs^4 cs a^0 |
  cs16_2 cs, cs es gs cs cs fs fs cs^4 a^1 fs |
  e'!16^2 e,^4 e gs b e e a^0 a e^4 e cs |
  e16^2 e,^4 e gs b e e a^0 a e^4 e cs | \break

%bar 105
  b'8^2\trill[( a]) gs^2\trill[( fs]) e^2\trill[( d]) |
  cs16^4( b as b^1 d b) fs4( b'8^2) |
  b8\trill[( a]) gs\trill[( fs]) e\trill[( d]) |
  cs16^4( b as b^1 d b) fs4( fs'8^2) |
  gs16-._\markup{\italic"spiccato"} e-. ds-. e-. fs-. e-.
    b'-.^3 a-. gs-. a-. b-. a-. |

%bar 100
  e16^4_\markup{\italic"simile"} d! cs d e d a'^3 gs fs gs a gs |
  d^4 cs b cs d cs gs'^4 fs es fs gs fs |
  cs^4 b as b cs b fs'^4 e! ds e fs e |
  \mark \default \tupletSpan 8
  \set subdivideBeams = ##t \omit TupletBracket
  \tuplet 3/2 { gs^2-. gs-. gs-. as-. as-. as-. gs-. gs-. gs-.
    as-. as-. as-. gs-. gs-. gs-. as-. as-. as-. } |

  \tuplet 3/2 { gs-. gs-. gs-. as-. as-. as-. gs-. gs-. gs-.
    \omit TupletNumber as-. as-. as-. gs-. gs-. gs-. as-. as-. as-. } |
%bar 115
  \tuplet 3/2 { gs-. gs-. gs-. as-. as-. as-. gs-. gs-. gs-. }
  \set subdivideBeams = ##f fs8( as8.^1 cs16) |
  cs4(~ cs16 gs) gs4 r8 |
  es16^1[( gs^3) gs-. gs,-._\thumb] ds'^\thumb[( fs)
    fs-. gs,-.] cs^3[( es) es-. gs,-.] | \pageBreak

%page break

  bs16_2( ds_\thumb) ds-. cs,-._\thumb^\markup{\small"III"}
    as'_\thumb[( cs_3) cs-. cs,-.] gs'_\thumb( cs) cs-. cs,-. |
  fs_3( as) as-. cs-._\thumb cs-.[ fs-.] fs-.[ d-.^1 cs-. fs-. as-.^1 cs-.] |
%bar 120
  cs4(~ cs16 gs) gs4( as8) |
  \mark \default \clef treble
  \set subdivideBeams = ##t \undo \omit TupletNumber
  \tuplet 3/2 { c16-.^2 c-. c-. ds-. ds-. ds-. c-. c-. c-.
    ds-.[ ds-. ds-. c-. c-. c-. ds-. ds-. ds-.] } | \break

  \tuplet 3/2 { c!16^\markup{\italic"simile"} c c d d c c c d d d d c c c d d d } |
  \tuplet 3/2 { c c c d d d c c c } bf8( d8.^2 f16-.) |
  f4(~ f16 c^3) c4 r8 | \break

  \set subdivideBeams = ##f

%bar 125
  a16_1( c_3) c-. c,-._\thumb g'[( bf) bf-. c,-.] f( a) a-. c,-. |
  \clef tenor
  e16( g_\thumb) g-. f,-._\thumb^\markup{\small"III"}
    d'( f[) f-. f,-.] c'( e) e-. f,-. |
  bf16( e) e-. f,-. bf[( d]) \clef treble f( bf^2) bf( d^1) d( f) | \break

  f4\<(~ f16 c) c4. |
  \mark \default
  cs!4\!^1_\markup{\dynamic"f"\italic"molto espress."}( fs!8) fs4( e8^3) |
%bar 130
  e8( cs8.^1 a16^\thumb) d4.^2 |
  cs4^1( gs'8^3) gs4( fs8^3) |
  fs8( e8.) ds16-. e4. |
  e4->^3 d!-> cs-> | \break

  b16_3( bf a gs_4 g fs) es( fs gs fs es fs) |
%bar 135
  e'4--\p d-- cs-- |
  b16( bf a gs g fs) es( fs gs fs es gs) |
  f'4->_3\f e-> d-> | \break

  c16_3( b bf a_0 gs_3 g) fs( g af g fs g) |
  f'4--\p e-- d-- |
%bar 140
  c16( b bf a gs\< g) fs( g af g fs g) |
  \mark \default
  ds4_1\mf( gs8_4) gs4( fs8) |
  gs8( ds8._4 b16) e4._2 |
  ds4( as'8_3\<) as4( gs8) | \break

  gs8_4\f( fs8.) es16-. fs4. |
%bar 145
  fs16-._4\pp e!-. ds-. e-. fs-. e-. gs-._4 fs-. es-. fs-. gs-. fs-. |
  fs_4_\markup{\italic"simile"} e ds e fs e ds_3 cs bs cs ds cs |
  e_4 d! cs d e d fs_4 e ds e fs e | \break \clef tenor

  e^4 d! cs d e d cs^4 b as b cs b |
  a8.\downbow\p e16\downbow cs'8.\upbow a16\upbow e'8.^4\downbow cs16\downbow |
%bar 150
  d8.^\markup{\italic"simile"} e16^2 d8.[ cs16^4] a8. fs16^4 |
  d8._0_\markup{\italic"cresc."} e16 cs'8.[ a16] e'8. cs16 |
  d8. e16 d8.[ b16] gs8. e16 | \break

  \mark \default
  a'4.->^3\f fs-> |
  e16( fs e) d-. b-. gs-. \clef bass e( d) b-. gs-. e8-. |
%bar 155
  r8 e16( e') gs,( gs') b,( b') d,( d')f,( f') | \clef treble
  a,16( a') gs,( gs') b,( b') e,( e') r8 r\fermata | \break

  a,4\p gs16( fs) fs4 e16( ds) | \clef bass
  e16( fs e d b gs e d b gs e8) |
  r8 e'^\markup{\italic"pizz"} gs b d f | \clef treble
%bar 160
  a8 gs b e r r\fermata | \pageBreak

%page break

  \mark \default \clef tenor
  a,4.->\p fs-> |
  e16( fs e cs a b) cs4 e8 |
  a4.-> fs-> |
  e16( ds gs fs e ds) e4 cs8( |
%bar 165
  a'4.->) fs-> |
  e16( fs e) d-. b-. gs-. e4 e'8( |
  cs'4.->) a-> | \break

  gs16( fs b a cs, ds) e4 r8 |
  \mark \default \clef bass
  a,,16-. e'-. <e cs'>( a) <e cs'>( a) a,-. fs'-. <e d'>( a) <e d'>( a) |
%bar 170
  a,16-. e'-. <e cs'>( a) <e cs'>( a) e'( cs a) e-. cs-. e-. | \break

  a,16-. e'-. <e cs'>( a) <e cs'>( a) a,-. fs'-. <fs d'>( a) <fs d'>( a) |
  a,16-. e'-. <e cs'>( a) <e cs'>( a) e'( cs a) e-. cs-. e-. |
  cs-. gs'-. <e cs'>( gs) <e cs'>( gs) cs,-. fs-. <fs a>( cs') <fs, a>( cs') | \break

  cs,-. gs'-. <e cs'>( gs) <e cs'>( gs) e'^1( gs e) cs-.^2 gs-. e-. |
%bar 175
  cs-. gs'-. <e cs'>( gs) <e cs'>( gs) cs,-. fs-. <fs a>( cs') <fs, a>( cs') |
  cs,-. gs'-. <e cs'>( gs) <e cs'>( gs)
    e'^1( cs^4 gs'^2) e-.^\thumb cs-.^2 gs-.^3 | \break

  \clef tenor
  e-.^1 b'-.^1 <b gs'^2>( e^\thumb) <b gs'>( e)
    e,-.^1 cs'-.^2 <cs a'^3>( e) <cs a'>( e) |
  e,-. b'-. <b gs'>( e) <b gs'>( e) b'^3( gs e^\thumb) b-. gs-. b-. |
  e,-. b'-. <b gs'>( e) <b gs'>( e) e,-. cs'-. <cs a'>( e) <cs a'>( e) | \break

%bar 180
  e,-. b'-. <b gs'>( e) <b gs'>( e) b'( gs e) b-. gs-. b-. |
  \mark \default
  gs'^4( e cs^4) gs-.^\thumb e-. ^2 gs-.^3 \clef bass
    cs,-.^\thumb cs'-.^3 e,-. cs'-. cs,( cs') |
  e^1( cs^2) a e-. cs-. e-. a,-.^1 a'-.^4 cs,-.^3 a'-. a,( a') | \break

  cs( a) fs cs-. a-. cs-. fs,-.^1 fs'-.^4 a,-. fs'-. fs,( fs') |
  a( fs) d a-. fs-. a-. d,-.^1 d'-.^4 fs,-. d'-. d,( d') |
%bar 185
  b4 b32( cs d^1 e fs4.^4)~ |
  fs8 e^2( ds) e( cs'8. b16) | \break

  \clef treble
  cs4_2 cs32( ds es_1 fs gs4.)~ |
  gs8 fs( es) \stemDown fs( d'8.^3 cs16) | \stemNeutral
  b4-> a-> gs-> |
%bar 190
  fs-> e-> d-> |
  cs8( b8.) b'16-. b4( a8) | \break

  a16_1( gs_\thumb d'_3 cs b a_0)
    gs_3_\markup{\small"II"}( fs e d cs_\markup{\small"III"} b) |
  as8_1( b8.) b'16-._\markup{\small"I"} b4( a8_0) |
  a16_3_\markup{\small"II"}( gs_2 e'_3_\markup{\small"I"})
    e,-._\thumb d'-. e,-. b'-. e,-. gs-. e-. fs-. e-. | \pageBreak

%page break

  d16_3_\markup{\small"III"}( e_\thumb) b-. e-. a,-._0_\markup{\small"I"} e'-._4
    \clef bass gs,-. e'-. fs,-. e'-. e,-. e'-. |
  d,^\markup{\italic"rit."}( e) b-. e-. a,-. e'-. gs,-. e'-. fs,-. e'-. e,-. e'-. |
  \mark \default \tempo "a tempo"
  a,8 r r r4 r8 |
  R1*6/8*15 | \break

  \mark \default
  R1*6/8*16
  R1*6/8*1
%bar 230
  R1*6/8*3
  \mark \default
  gs4._3\pp(~ gs8 cs_1\upbow ds) |
  e8\downbow( ds16 cs gs'4^2\upbow_\markup{\small"III"})
    cs8\downbow_\markup{\small"II"}( ds) | \break

%bar 235
  e8\upbow( ds16 cs gs'4^2\downbow_\markup{\small"II"})
    \clef treble cs8\upbow_\markup{\small"I"}( ds) |
  e4( cs8^3) a4( cs8) |
  gs4._1~ gs8 \clef bass cs,^1\upbow[( ds]) |
  e8( ds16 cs gs4) cs,8\upbow( ds) |
  e8( ds16 cs gs4) cs8( ds) | \break

%bar 240
  e4( cs8^4) a4( cs8_3) |
  b8.^1 a'16^4( gs fs) fs8. e'16( ds cs) | \clef treble
  cs8. a'16_3( gs fs) fs8. e'16^3( ds cs) |
  cs4.(~ cs8 b\upbow gs) |
  e4.~ e8-. \clef bass cs8\upbow[( b32 gs e cs]) | \break

%bar 245
  \mark \default
  b16\p( a'^4) a( gs) gs( fs) fs( e') e( ds) ds( cs) | \clef treble
  cs16( a'_3) a( gs) gs( fs) fs( e'^3) e( fs) fs( cs^2) |
  cs4.~ cs16 b_3( as b gs_4 e) | \clef bass
  cs4.^2~ cs16 b( gs e cs b) | \break

  gs4.(~ gs8 gs'\upbow_3_\markup{\small"III"} fs~ |
%bar 250
  fs8) e16^4\downbow( ds cs4) e'8\upbow( cs^2) |
  b4.^1~ b8 \clef treble b'^3\upbow( a~ |
  a8) gs16_4\downbow( fs e4) gs'8^3\upbow( e^2 |
  ds4.\downbow~ ds4) as8_2( | \break

  ds4.^1~ ds4) b8^3( |
%bar 255
  \mark \default
  e8.^2[) d!16^3]( b8.[) e,16_2]( d8.[) b16_4]( | \clef bass
  e,8.[) d16^4]( b8.[) e,16]( e'8.^3[) d16]( | \bar "||"
  \key c \major
  c16_4 b c a f' e) ds^1( e f e a^0 b) | \break

  \clef treble
  c16( b c a f'^3 e) ds( e f e a_\thumb b |
  c8.[) d16]( c8.[) g16^4]( e8.[) d16_4]( |
%bar 260
  c8.[) d16]( c8.[) g16]( \clef bass c,8.[) bf16_3]( |
  a16 gs a_1 f_2 d'_4 c) b( c d c f^1 g!) | \break

  a( fs a f^2 d'^4 c) b( c d c f^1 g!) |
  a^3 d,,^0 g' d, f' d, e'^3 d, f' d, d' d, |
  a'' d,, g' d, f' d, e' d, f' d, d' d, | \break

%bar 265
  f' g,, d''^4 g,, b' g, e''^4 g,, c' g, a'^0 g, |
  d''^4 g,, b' g, g'^4 g, c'^2 g, a'^0 g, f' g, |
  d''^3^\flageolet^\markup{\small"II"} g,, b' g, g' g, c'^4 g, a'^4 g, f' g, | \break

  b'^4 g, g'^4 g, e' g, a'^4 g, f' g, d' g, |
  b'^4 g, g'^4 g, e' g, a'^4 g, f' g, d' g, |
%bar 270
  g' g, e'^\markup{\italic"rit."} g, c g f' g, d' g, b g |
  \mark \default
  \tempo "a tempo"
  < \parenthesize c4 > r8 r4 r8 |
  R1*6/8*3 | \pageBreak

%page break

%bar 275
  \mark \default
  g''2._\markup{\dynamic"pfz"} |
  e\fz |
  a4.^3~ a8\< g( f) |
  e4.^3\>( d4) g,8-.\! |
  g4.~ g8 a16^0( b) b8-. |
%bar 280
  c4^1( d8) e4( d8) |
  d4.\<~ d8 e16^1( fs) fs8-. | \clef treble
  g4( a8_1) c4( b8) | \break

  b4.\!_\markup{\italic"poco a poco cresc."} a8_0( g_4 e) |
  d4._1 e4( d8) |
%bar 285
  b'4^3~ b8-. a( g e) |
  d4. e4( d8) |
  d4._2 c8( d e) |
  f4_2( g8) f4( e8) |
  d4. c8( d e) | \break

%bar 290
  f4( g8) f4( e8) |
  g4._2 f8( g a_1) |
  bf4( c8) bf4( a8) |
  g4.\< f8( g a) |
  bf4( c8) << bf4( {s8. s16\f} >> a8_0) |
%bar 295
  g4._4_\markup{\dynamic"p"\italic"sub."}( fs) |
  f( e) | \break

  d4._2\< b'4^3^\markup{\small"I"}( a8) |
  gs4.\> f8( e d) | \clef bass
  \mark \default
  c4.\! a,16( c e a c e) |
%bar 300
  ds4. e4( b8) |
  c4\downbow( a,8\downbow) a16( c e\< a c^1 e) |
  ds4. e4( b8) |
  c4.->\f a'4.->^3 | \break

  g16^\markup{\small"II"}( a g) f-. d-. b-. g( f) d b-. g8-. |
%bar 305
  c,4.-> a'''4-> a8-. |
  g16( a g) f -. d-. b-. g( f) d b-. g8-. |
  R1*6/8 | \clef treble
  e''4.\downbow(~ e8 f g\< |
  a2.\upbow) | \break

%bar 310
  b,16\!( c^\markup{\italic"rit."}) c( d) d_1( e) e( f) f_1( g) g( a) |
  g4._2\pp^\markup{\italic"a tempo"}~ g8 a_1( b) |
  c4^1( d8) e4.(~ |
  e8 d e) g4( f8) |
  d8 r^\markup{\italic"rit."} r r4 r8 | \break

%bar 315
  \mark \default \clef bass \tempo "a tempo (meno allegro)"
  g,,16^2( c,) c( b) c( d^\thumb) d( e) e( f) f( g) |
  g16^2( b,) b( c) c( d^\thumb) d( e) e( f) f( g) |
  e16( c'_\markup{\italic"cresc."}) c( b) b( a^0) gs^2( f'^4) f( e) e^4( d) | \break

  \clef treble
  c16_1( b'_3) b( a_\thumb) a( g_3_\markup{\small"II"})
    fs^1( e'^3) e( d) d^3( c) |
    b\f( d) d( c) c( b) b_1( a_\thumb) a_0( af_3) af( g) |
%bar 320
  g( af) af( g) g( fs_3) fs_\markup{\italic"dim."}( g) g( g,_2) af( fs) | \break

  a16( f'_4) f( e) e( d) \clef bass d^4( c) c( b) b( a) |
  a^4( g) g( f) f^2^\markup{\italic"rit."}( e) e( d) d_0( c_2) c( b) |
  \tempo "a tempo"
  c\p( g' c, b c^1 d) e( c d^0 e f g) | \break

  b,( g' b, a b c) d( g, d'_\markup{\italic"cresc."} e f g) |
%bar 325
  e( a b c b a) gs^1( b d^1 f e^4 d) | \clef treble
  c( d e b'_1 a_\thumb g) fs( a^\thumb b^1 e^3 d c) | \break

  b^1\f( d) b-. g-.^1 b-. g-. fs_1( a) fs-. d_1-. fs-. d-. | \clef bass
  b^1_\markup{\italic"dim."}( d) b-. g-.^1 b-. g-. fs^1( a) fs-. d-.^0 fs-. d-. |
  \mark \default
  b^1( d) b-. g-. d'-. g,-. d'-. d,-. a'-. d,-. d'-. d,-. | \pageBreak

%page break

%bar 330
  b'16( d) b-. g-. d'-. g,-. d'-. d,-. a'-. d,-. d'-. d,-. |
  a' d, d' d, a' d, a' d, d' d, a' d, |
  a'^\markup{\italic"poco rit."} d, d' d, a' d, a' d, d' d, a' d, | \break

  \tempo "a tempo"
  ef4.\f~ ef8 bf'_2 g |
  ef'8( bf'^4 g) \clef treble ef'( bf'_3 g) |
%bar 335
  af4.\< g8_2( f8. c'16_3) |
  c4.\>( b8\!\fermata) r r | \clef bass
  g,,4.\f~ g8 d'_4 b | \break

  \clef treble
  g'8^\thumb^\markup{\small"III"}\<( d'^\markup{\small"II"} b) g'( d'^\markup{\small"I"} b) |
  c4.\! b8\>( a8. e'16^3) |
%bar 340
  e4.( ds8\pp\fermata) r r |
  e4( b8) d4( c8) |
  b4.(~ b4 a8) |
  g4^4( e8) g4( e8) | \break

  e4.^\markup{\italic"rit."}(~ e4 d8) |
%bar 345
  \tempo "a tempo"
  c4.-> a'->_3 |
  g16( a g) f-. d-._4 b-. \clef bass g( f) d-. b-. g8-. |
  c'4.-> b'4->^3( a8) |
  g16^2( a g) f-. d-.^4 b-. g\<( f) d-. b-.\! g8-. | \break

  \mark \default \clef treble
  c'4.\f->_\markup{\italic"con molta forza"} c'-> |
%bar 350
  d,--_\thumb^\markup{\small"II"} d'->^\markup{\small"I"} |
  e,-- e'->^2 |
  f8-. \clef bass d,,16^0( e f^1 g a g f g a^0 b) | \clef treble
  c4->( c'8^3) c4->( c,8_\thumb) | \break

  d4--_\thumb( d'8^3) d4->\<( d,8) |
%bar 355
  e4--->^\thumb( e'8^3) e4->^0( e,8\!^\markup{\small"I"}) |
  \clef bass
  \grace {d,16^0\downbow( bf'^2} f'2.^2\startTrillSpan)~ |
  f2.\upbow~ |
  \afterGrace f4.\stopTrillSpan( {e16 f)}
    %allow for accidental in trill spanner
    \override TrillSpanner.bound-details.left.text = \markup{
      \musicglyph #"scripts.trill" \raise #0.65 \teeny  \sharp }
    \grace {cs,\downbow( a'} e'4.\startTrillSpan~ |
  e2.\upbow)~ |
%bar 360
  \afterGrace e4.\stopTrillSpan( {ds16 e)} \grace {d,^0( fs'^3}
    %no more accidental
    \override TrillSpanner.bound-details.left.text = \markup{
      \musicglyph #"scripts.trill" }
    a4.^2_\markup{\dynamic"sffz"}\startTrillSpan)~ |
  \mark \default
  \afterGrace a2.( {gs16\stopTrillSpan a)} | \break

  \clef tenor
  a,16^0( fs'^\thumb b^3 a) a8-. \afterGrace a4.\startTrillSpan( {gs16\stopTrillSpan a)} |
  a,16^0( fs'^\thumb b^3 a) a8-. \afterGrace a4.\startTrillSpan( {gs16\stopTrillSpan a)} |
  a,16( fs' b a) a( fs) a,( fs' b a) a( fs) |
%bar 365
  \acciaccatura a,8_0( \afterGrace cs'2.^2\startTrillSpan)( {b16 cs\stopTrillSpan)} |
  a,16^0( cs'^2 b a^\thumb b8-.) \afterGrace
    cs4.\upbow\startTrillSpan( {bs16 cs\stopTrillSpan)} | \break

  a,16( cs' b a b8-.) \afterGrace cs4.\startTrillSpan\upbow( {b16 cs\stopTrillSpan)} |
  a,16^0( cs'^2 b a^\thumb b cs) a,( cs' b a) b( cs) |
  d4-> b->^3 gs-> |
%bar 370
  fs->^4 d-> b8-.^4 gs-. |
  e-. r r r4 r8 | \pageBreak

%page break

  \clef treble
  R1*6/8*3 |
%bar 375
  R1*6/8 |
  R1*6/8 |
  \mark \default \tempo "Quasi recitativo"
  r4 r8 b'4.\p\downbow(~ |
  b8 d e) f4_1( g8) |
  a4.\fermata~ a8 r r | \break

%bar 380
  r4 r8 b4._\markup{\italic"più"\dynamic"p"}\downbow(~ |
  b8 a_4 gs) fs4( d'8^3) |
  d4.\fermata~ d4^\markup{\italic"rit."}( e,8_1) |
  e4.\pp\upbow(~ e8 fs gs) |
  a4_1( b8) cs4.(~ | \break

%bar 385
  cs8 b\downbow cs^1) e4\upbow( d8) |
  a4_1( b8) cs4\fermata( b8) |
  a4^\markup{\italic"a tempo"} r8 r4 r8 |
  R1*6/8*2 |
%bar 380
  R1*6/8*1 | \bar "||" \break

  \mark \default \key a \major
  a4 r8 r4 r8 |
  R1*6/8*11 |
  \mark \default
  R1*6/8*2 |
  R1*6/8*4 |
  R1*6/8*10 |
  e2.\>\f\> |
%bar 420
  fs-> | \break

  e4.\p b'8_3( gs_4 fs) |
  e16( fs e) d-._4 b-. gs-. \clef bass e( d) b-. gs-. e8-. |
  f2.~ |
  f8 a_1( cs_3 e^4 f^1 a) | \break

%bar 425
  g,2._4~ |
  g8 b_1( ds_3 e^4 g^1 b) |
  a,2._1~ |
  a8 c( e f^1 a c) |
  \mark #111
  d4. g8^4( f d) | \break

%bar 430
  c16^2( d c) bf-. g-. e-. c( bf) g-. e-. c8-. |
  f''4.^1 bf8^4( g f) |
  e16( fs e) d-.^4 b-. gs-. e( d) b-. gs-. e8-. | \break

  \clef treble r8 r a''-._\thumb d( b a^3) |
  gs16( a gs) fs-. ds-._4 bs-._1 \clef bass gs^4( fs) ds-.^1 bs-.^4 gs8-. |
%bar 435
  \clef treble cs'4\downbow cs'8^1\downbow fs^3( ds^3 cs) | \break

  b16( cs b) a-._0 fs-._4 ds-. \clef bass b^1( a) fs-. ds-. b8-. |
  cs16( e gs) e-. gs-. e-. cs( fs gs) fs-.  gs-. gs,-. |
  \mark \default
  cs( e gs) e-. gs-. e-. cs( fs gs) fs-. gs-. gs,-. | \pageBreak

%page break

  cs( e gs) e-. fs-. e-. cs( fs g) fs-. gs-. gs,-. |
%bar 440
  cs( e gs) e-. gs-. e-. gs e cs gs cs e |
  d( f a) f-. a-. f-. d( g a) g-. a-. a,-. | \break

  d( f a) f-. a-. f-. d( g a) g-. a-. a,-. |
  d( f a) f-. a-. f-. d( g a) g-. a-. a,-. |
  d( f a) f-. a-. f-. a f d f d a | \break

%bar 445
  gs( b d f e d) b( d f^1 af^4 g f) |
  d( es gs b^4 as gs) es^1( gs b^1 d cs b) |
  f'8._\markup{\italic"cresc. poco a poco"}( e16 f8.[ e16]) f8.( e16 | \break

  f8. e16) f8.[( e16] f8. e16) | \clef treble
  a8.->_3( gs16 a8.[ gs16]) a8.->( gs16 |
%bar 450
  a8. gs16) a8.->[( gs16] a8. gs16) |
  cs16\fz^2( b cs b cs b) cs\fz( b cs b cs b) | \break

  cs16\fz( b cs b cs b) cs\fz( b cs b cs b) |
  \mark \default
  a4.\ff->_3^\markup{\italic"Solo"} fs-> | \clef bass
  e16^2( fs e) d-. b-.^4 gs-. e_1( d) b-. gs-. e8 | \break

  r8 e16_\thumb( e'_3) gs,_\thumb( gs'_3) b,( b') d,( d') f,( f') |
  \clef treble a,( a') gs,( gs') b,( b') e,( e') r8 r\fermata |
  a,4->\p gs16( fs) fs4-> e16_2( ds) | \break

  \clef bass e16( fs e) d!-. b-. gs-. e( d) b-. gs-. e8-. |
  r8 e'^\markup{\italic"pizz."} gs b d f | \clef treble
  a_2 gs b e^3 r r\fermata | \pageBreak

%page break

  a,4.->\p^\markup{\italic"arco"} fs-> |
  e16( fs e cs a_0 b) cs4( e8) |
  a4.-> fs-> |
  e16_2( ds gs fs e ds_1) e4 cs8( |
%bar 465
  a'4.) fs | \break

  e16( fs e d b gs) \clef tenor e4 e'8( |
  cs'4.->\<) a-> |
  gs16^3\!( fs^\thumb b^3\> a cs,^1 ds) <<e4( {s8 s\!}>> es\upbow) |
  \mark \default
  fs4.\pp(~ fs4 a8^3) |
%bar 470
  a4.(~ a4 fs8) | \break

  e4.~ e8 a,,16\pp[( cs e a]) |
  cs4.~ cs8 r r |
  cs4.(~ cs4 fs8^3) |
  fs4.(~ fs4 e8) |
%bar 475
  e4.~ e8 cs16_2( a_0 e cs) |
  a4.~ a8 r8. fs''16^1 | \break

  fs4.(~ fs4 a8) |
  a4.(~ a4 fs8) |
  e4.~ e8 a,,16\pp[( cs e a] |
%bar 480
  cs4.~ cs8) r r |
  \mark \default
  cs16-._4 cs,-._1 cs( es_4) es-. gs-._1 cs^4( fs^2) fs-. cs-.^4 cs-. a-.^0 | \break

  cs16-._4 cs,-. cs( es) es-. gs-. cs( fs) fs-. cs-. cs-. a-. |
  e'-.^2 e,-.^4 e( gs^1) b-.^4 e-.^2 e( a^0) a-. e-.^4 cs-. a-. |
  e'-.^2 e,-. e( gs) b-. e-. e( a) a-. e-. cs-. a-. | \break

%bar 485
  b'8^2\trill( a) gs^2\trill[( fs]) e^2\trill( d) |
  cs16^2( b as b^1 d b) fs4( b'8^2) |
  b8\trill( a) gs\trill[( fs]) e\trill( d) |
  cs16^4( b as b^1 d b) fs4( fs'8) | \break

  fs16-.^4 e-. ds-. e-. fs-. e-. b'-.^3 a-. gs-. a-. b-. a-. |
%bar 490
  e^4^\markup{\italic"sim."} d! cs d e d a'^3 gs fs gs a gs |
  d^4 cs b cs d cs gs'^4 fs es fs gs fs | \break

  cs^4 b as b cs b fs'^4 e ds e fs e |
  \mark \default \set subdivideBeams = ##t
  \tuplet 3/2 { gs16-.^2 gs-. gs-. as-. as-. as-. gs-. gs-. gs-.  \omit TupletNumber
    as-._\markup{\italic"sim."} as-. as-. gs-. gs-. gs-. as-. as-. as-. } | \break

  \tuplet 3/2 { gs-. gs-. gs-. as-. as-. as-. gs-. gs-. gs-.
    as-. as-. as-. gs-. gs-. gs-. as-. as-. as-. } |
%bar 495
  \tuplet 3/2 { gs-. gs-. gs-. as-. as-. as-. gs-. gs-. gs-. } fs8( as8.^1 cs16) |
  cs4(~ cs16 gs) gs4 r8 | \break

  \set subdivideBeams = ##f
  es16^1( gs^3) gs-. gs,-.^\thumb ds'^\thumb[( fs) fs-. gs,-.] cs^3( e^1) e-. gs,-. |
  bs_1^\markup{\small"II"}( ds) ds-. cs,-._\thumb^\markup{\small"III"}
    as'_1[( cs-3) cs-. cs,-.] gs'_3( bs_1) bs-. cs,-. |
  fs-.[ as-. as-. cs,-.] cs16( fs) as^1( cs^4) fs( as^1) as( cs^3) | \pageBreak

%page break

  \clef treble
%bar 500
  cs4(~ cs16 gs_1) gs4 r8 |
  \set subdivideBeams = ##t
  \tuplet 3/2 { d'16-.^2 d-. d-. e-. e-. e-. d-. d-. d-.
    \omit TupletNumber e-. e-. e-. d-. d-. d-. e-. e-. e-. } |
  \tuplet 3/2 { d-. d-. d-. e-. e-. e-. d-. d-. d-.
    e-. e-. e-. d-. d-. d-. e-. e-. e-. } | \break

  \tuplet 3/2 { d-. d-. d-. e-. e-. e-. d-. d-. d-. } c8( e8.^1 g16) |
  \set subdivideBeams = ##f
  g4(~ g16 d) d4 r8 | \clef bass
%bar 505
  b,16( d) d-. d,-. a'[( c) c-. d,-.] g( b) b-. d,-. | \break

  fs16^1( a^4) a-. g,-. e'_1[( g_4) g-. g,-.] d'( fs) fs-. g,-. |
  c( e) e-. g,-. c-. e-. g( c) c( e^1) e( g) |
  g4(~ g16 d) d4 r8 | \break

  \clef tenor \mark \default
  ds4^1_\markup{\dynamic"f"\italic"molto espress."}( gs8^4) gs4( fs8) |
%bar 510
  fs8( ds8.^4 b16) e4.^2 |
  ds4( as'8^3) as4( gs8) |
  gs^4( fs8. es16) fs4. |
  fs4->^4\f e!-> ds-> | \break

  cs16^3( c b bf a^0 gs^2) fss_1( gs_2 a_3 gs fss gs) |
%bar 515
  fs'4->\p e-> ds-> |
  cs16( c b bf a gs) fss( gs a gs fss gs) |
  g'4--^4\f fs-- e-- | \break

  d16( cs c b bf^1 a^0) gs!_1( a_2 bf_3 a gs a) |
  g'4--\p fs-- e-- |
%bar 520
  d16( cs c b bf a\<) gs( a bf a gs a) |
  es'4^1\f( as8^3) as4( gs8) | \break

  gs8^4( es8.^1 cs16^4) fs4. |
  \clef treble es4( bs'8^3) bs4( as8) |
  as8_3( gs8. fss16) gs4. |
%bar 525
  \mark \default
  gs16-._4 fs-. es-. fs-. gs-. fs-. as-._3 gs-. fss-. gs-. as-. gs-. | \break

  gs16-._4 fs-. es-. fs-. gs-. fs-. e_4_\markup{\italic"sim."} ds css ds es ds |
  fs_4 e! ds e fs e gs_4 fs es fs gs fs |
  fs_4 e! ds e fs e ds_4 cs bs cs ds cs | \break

  fs16_1 a ds,_1 fs b,_4 ds e_1 gs cs,_1 e a,_0 cs | \clef bass
%bar 530
  ds^1 fs b,^1 ds gs,^4 b cs^1 e a,^0 cs fs, a |
  b ds gs, b e, gs a cs fs, a ds,^1 fs | \break

  b^1 ds b gs^4 e gs a cs a fs^4 ds fs |
  b^1 ds b gs^4 e gs a^\markup{\italic"poco a poco rit."} cs a fs^4 ds fs |
  gs^1 b gs e^4 cs e fs^1 a fs ds^4 b ds |
  \mark \default \tempo "a tempo"
  e4 r8 r4 r8 |
  R1*6/8*3 | \pageBreak

%page break

  \clef treble
  b''2.->^3\p |
%bar 540
  gs->\< |
  << cs4.^3~ {s8 s\! s} >> cs8 b\>( a) |
  gs4._4\! fs4( b,8_1) |
  b4.\p~ b8 cs16_\markup{\italic"cresc. poco a poco"}( ds) ds8-. |
  e4_1( fs8) gs4( fs8) |
%bar 545
  fs4.~ fs8 gs16( as) as8-. | \break

  b4( cs8^1) e4( ds8^3) |
  \mark \default
  ds4._\markup{\italic"più espress."} cs8( b gs_4) |
  fs4. gs4( fs8) |
  ds'4--^3~ ds8-. cs( b gs) |
%bar 550
  fs4. gs4( fs8) |
  fs4. e8( fs gs) | \break

  a4( b8) a4( gs8) |
  fs4. e8( fs s) |
  a4( b8) a4( gs8) |
%bar 555
  b4.\f a8( b cs) |
  d4( e8) d4( cs8) |
  b4. a8( b cs) | \break

  d4\<( e8) d4( cs8) |
  b4.\!_\markup{\italic"sub."\dynamic"mf"}\>( as) |
%bar 560
  a!4._3( << gs) {s8 s\! s} >>  |
  fs4.\p ds'4^3( cs8) |
  bs4.\< a!8_0( gs_4 fs) |
  \mark \default \clef bass
  e4.^2\mf cs,16^1( e gs^1 cs^4 e gs) | \break

  fss4. gs4( ds8\<) |
%bar 565
  e4( cs,8) cs16^1( e gs cs e gs)  |
  fss4. gs4\!( ds8) |
  e4.--\f \clef treble c'--^3 |
  b16( cs b) a-._0 fs-._4 ds-. \clef bass b( a) fs-.^4 ds-. b8-. |
  e,4 \clef treble e''8_2^\markup{\small"II"}( cs'4.^3^\markup{\small"I"}) | \break

%bar 570
  b16( cs b) a-. fs-. ds-. \clef bass b( a) fs-. ds-. b8-. |
  R1*6/8
  gs'4.\mf(~ gs8 a^1\< b) |
  cs2.\> |
  ds,16^1\!( e^1) e( fs) fs( gs\>) gs( a^0) a( b) b( cs) | \break

%bar 575
  b4.\p(~ b8 cs\upbow ds) | \clef treble
  e4\<( fs8) gs4.(~ |
  gs8 fs\downbow gs\!) b4( a8\>) |
  fs8 r\! r r4 r8 |
  \mark \default \clef bass
  b,16\p( e,^2) e_\markup{\italic"cresc."}( ds) e( fs) fs( gs^4) gs( a^0) a( b^2) | \break

%bar 580
  b16( ds,) ds( e^2) e( fs^4) fs( gs^1) gs( a) a( b) | \clef treble
  gs_3( e') e_4( ds) ds_4( cs) bs( a'_\thumb) a( gs_3^\markup{\small"II"}) gs( fs) |
  e_1( ds'_3) ds( cs) cs( b) as^\thumb( gs'^3) gs( fs) fs( e) | \break

  ds^1\f( fs) fs( e) e( ds) ds^3( cs) cs( c) c( b) |
  b( c) c( b) b( as^2) as( b_\markup{\italic"dim."}) b( fs_4) fs( ds) |
%bar 585
  cs_2( a'!_3) a( gs) gs( fs) fs_4( e) e( ds) ds_4( cs) | \clef bass \break

  cs^\markup{\italic"rit."}( b) b( a) a^4( gs) gs( fs) fs( e) e^2( ds) |
  \tempo "a tempo"
  e( b' e, ds e^1 fs) gs( e fs gs a^2 b^4) |
  ds,^3( b'^4 ds, cs ds^1_\markup{\small"III"} e) fs( b,^1 fs' gs a^2 b) | \pageBreak

%page break

  \clef treble
  gs16_3( cs ds e ds cs) bs_1( ds fs_1 a gs fs) |
%bar 590
  e\f( fs gs ds' cs b) as\thumb( cs^1 e^3 gs^3 fs e) |
  ds^1( fs) ds-. b-.^1 ds-. b-. as_1( cs) as-. fs-._1 as-. fs-. | \break

  \clef bass
  ds16^1( fs) ds-. b-.^1 ds-. b-. as^1( cs) as-. fs-.^1 as-. fs-. |
  \mark \default
  ds16^1_\markup{"["\dynamic"fp""]"}( fs ds b^4) ds[( fs ds b]) ds( fs ds b) |
  ds( fs) ds-. b-. ds-. b-. ds( fs) ds-. b-. ds-. b-. | \break

%bar 595
  d16^0( f^2 d b^3) d[( f d b]) d( f d b) |
  d( f) d-. b-. d-. b-. d( f) d-. b-. d-. b-. |
  d( f d b) f'^1[( af f d]) af'^1( cf^3 af f^4) | \break

  gs^2( e'^3) d-.^1 e-. d-. e-. gs,( e') d-. e-. d-. e-. |
  \clef treble
  d_1( f_3 d b_4) f'_1_\markup{\italic"cresc."}[( af_3 f d_4_0]) af'_1( cf_2 af f_3) |
%bar 600
  gs^2( e'^3) d-.^1 e-. d-. e-. gs,( e') d-. e-. d-. e-. | \break

  e^1 g e cs^3^\markup{\small"II"} a cs d^1^\markup{\small"I"} fs d b^3 g b |
  cs_1 e cs a_3 fs a b_1 d b g_3 e g |
  cs_1 e cs a_3 fs a b_1 d b g_3 e g | \break

  a_1 cs a fs_3 d fs g_1 b g e_3 cs e |
%bar 605
  a_1 cs a fs_3 d fs g_1 b g e_3 cs e |
  fs_1 a fs d_3 b d e_1 g e cs_4 a cs | \break

  \clef bass
  <f, d'^4>8. gs,!16( <f' d'>8.[) gs,16]( <f' d'>8.) gs,16( |
  <gs' f'^4>8.) d16( <gs f'>8.[) <b, gs'>16]( <gs' f'>8.) <b, gs'>16( |
  \clef tenor
  <b' gs'>8.) <d, b'>16( <b' gs'>8.\<[) <d, b'>16]( <b' gs'>8.) <d, b'>16( |
%bar 610
  <d' b'>8.) <f, d'>16( <d' b'>8.[) <f, d'>16]( <d' b'>8.) <f, d'>16 |
  \mark \default \clef treble
  fs''16\ff fs e e fs[ fs e e] fs fs e e | \break

  fs fs e e fs[ fs_\markup{\italic"dim."} e e] fs fs e e |
  fs fs e e fs[ fs e e] fs fs e e |
  fs fs e e fs[ fs e e] fs fs e e | \break

%bar 615
  fs fs gs, gs e'[ e fs,^2 fs] d' d e, e |
  cs' cs d,_0 d b'_3[ b cs, cs] a'_0 a b,_2 b | \clef bass
  gs' gs a,^0 a fs'^4[ fs gs, gs] e' e fs,^3 fs | \break

  d'^\markup{\italic"rit."} e,\> cs' d, b' cs, a' b, gs' a, fs' gs, |
  \mark \default \tempo "Poco meno"
  e'4.^2\pp(~ e8 fs^1 gs) |
%bar 620
  a4^1\upbow( b8) cs4.\downbow~ |
  cs8 b\upbow^\markup{\small"II"}( cs^\markup{\italic"rit."} e4\downbow d8) |
  cs4.^4\upbow( b4 e8\upbow^\markup{\small"I"}) | \break

  \clef treble
  e4.(~ e8 fs gs) |
  a4( b8) cs4.\downbow(~ |
%bar 625
  cs8 b\upbow cs^1) e4^3\downbow( d8) |
  cs4.^2\fermata(~ cs4 b8^2) | \bar "||"
  \time 2/2 \tempo "Allegro vivace"
  a4 r r2 |
  R1*11 |
  \mark \default
  R1*15 | \pageBreak

%page break

  \mark \default \tempo "Un poco sostenuto"
  \clef bass
  r2 <fs, a>4-.\p <gs b>-. |
%bar 655
  \grace {a,16^( fs'} cs'2--) \grace {gs,16^( es'} cs'4.) cs8-. |
  \grace {a,16^( fs'} cs'2) \grace {b,16^( fs'} d'4) \grace {d,16^1^( b'} fs'4) |
  \grace {a,,16^( fs'} cs'2--) \grace {cs,16^4^( es^2} b'4.^1) b8 |
  \grace {fs,16^( cs' fs} a2) <a cs>4-. <b d>-. |
  \grace {cs,16^1^( a'} e'2) \grace {d,16^2^( b'^4} e4.^2) e8-. | \break

%bar 660
  \grace {cs,16^( a'} e'2) \grace {d,16^( a'} f'4) \grace {d,16^( a' f'} a4^1) |
  \clef treble
  \acciaccatura c,8^( c'2^3) a4._3( g8) |
  f2 f4\<( a) |
  c2\!( d4.\> c8) |
  c2\! f,4^\markup{\italic"poco a poco rall."}( a\<) |
%bar 665
  c2\!( \acciaccatura e8 d4. c8) | \break

  c2\>( a4) r\! | \bar "||"
  \mark \default \time 6/8
  % <cs, cs'!>4 <fs fs'>8~ <fs fs'>4 <e e'>8 |
  << {cs!4^3 fs8^3~ fs4 e8~}
    {cs,4_\thumb_\markup{\italic"molto espress"} fs8_\thumb~ fs4 e8~} >> |
  << {e8 cs8.( a16 d4.)} {e'8 cs8. a16 d4.} >> |
  << {cs4 gs'8~ gs4 fs8} {cs,4 gs'8~ gs4 fs8} >> |
%bar 670
  << {fs8( e8. ds16-.) e4.~} {fs'8( e8. ds16-.) e4.~} >> | \bar "||" \break

  \time 4/4
  <e e,>4 r r2 |
  \clef bass \tempo "Allegro ma non troppo"
  \tuplet 3/2 {fs,,,8_4_\markup{\italic"sempre"\dynamic"f"}
    cs' a fs' cs a gs_1 cs_4 b es_1 cs b } |
  \omit TupletNumber
  \tuplet 3/2 {a fs' cs a'^0 fs cs b fs' d b' fs d} |
  \tuplet 3/2 {cs^3 a'^4 fs cs' a fs cs^4 gs' es cs' gs e} | \break

%bar 675
  \tuplet 3/2 {fs cs' a fs'^2 cs a fs_2 cs a} fs r |
  \tuplet 3/2 {a e' cs a' e cs a fs' e b' fs e} |
  \tuplet 3/2 {a, e' cs a' e cs d a' f d' a f} | \break

  \tuplet 3/2 {c^2 a'^4 f^1 c' a^0 f c b c c' b c} |
  \tuplet 3/2 {f^2 c^4\> a c a f a f c f c\! a} |
%bar 680
  \tuplet 3/2 {f_\markup{\italic"più"\dynamic"p"} c' a f' c a f c' g e' c bf} | \break

  \tuplet 3/2 {f c' a f' c f a^1 c a f_2 c a} |
  \tuplet 3/2 {f c' a f' c a f c' g e' c bf} |
  \tuplet 3/2 {f c' a f' c a} f4\fermata a'8^0\f b | \bar "||" \break

  \mark \default \time 6/8 \tempo "Tempo I."
  cs4_\markup{\italic"con espressione"} fs8^3~ fs4 e8~ |
%bar 685
  e cs8.( a16 d4.) |
  cs4 gs'8~ gs4 fs8 |
  fs8( e8.) ds16 e4.\fermata | \clef treble
  fs4\p( d'8^3) d4( cs8) | \break

  cs8->( b8. fs16) b4. |
%bar 690
  e,4( c'8) c4( b8) |
  d8->( a8. e16) a4\fermata bs,8 |
  cs4( fs8) fs4( e8) |
  e8( cs8. a16) d4.~ | \break

  d4. \clef bass d16\<( b gs^4 f^1 d b) |
%bar 695
  \mark \default
  g_1\!( b d_0 f e d) b^3( d f^1 af g f) |
  d( es gs^1 b as gs) es^1 gs b^1 d cs b | \pageBreak

%page break

  \clef treble
  gs_1( b d_1 f e d) b_1( d f_1 af g f) |
  d_1^\markup{\small"II"}( es_3 gs_\thumb^\markup{\small"I"} b as gs)
    e_1^\markup{\small"I"}( gs b_\thumb^\markup{\small"I"} d cs b) |
  cs^1_\markup{\small"II"}( b d\thumb_\markup{\small"I"} f e d) gs,( b d f e d) | \break

%bar 700
  gs,( b d fs! e ds) e4.(~ |
  e8 cs^2 a^0) e_4( cs a) |
  gs16_1( b d f_3 e ds) e4.~ | \clef bass
  e8 cs( a e cs a) | \break

  gs16_1\p( b_4 d_1 fs e ds) e4. |
%bar 705
  gs,16^\markup{\small"IV"}( b d^\markup{\small"III"} fs e ds) e4. |
  g,16_\markup{\italic"cresc."}( b d fs e ds) \afterGrace e4.\trill( {ds16 e)} |
  g,16( b d fs e ds) \afterGrace e4.\trill( {ds16 e)} | \break

  gs,16( b d_1 fs_4 e_4 ds) e( e') e-. e,-.^1 d'-.^4 e,-. | \clef treble
  gs_1( b_4 d_\thumb^\markup{\small"II"} fs_2 e ds_\thumb)
    e_\thumb( e') e-. e,-. d'!-. e,-. |
%bar 710
  e' e, d' e, e' e, d' e, e' e, d' e, | \break

  e'\< e, d' e, e' e, d' e, e' e,\! d' e, |
  \mark \default
  cs'4^2\ff( b16 a) a4( gs16_4 fs) |
  fs4 e8( e'4^3) d8 |
  cs4( b16 a) a4( gs16 fs) |
%bar 715
  fs4 e8( e'4^3) d8( | \break

  cs4) a8_\thumb( e'4) d8( |
  cs4) a8( e'4) d8 |
  cs16-.^1 e-.^3 e-. cs-. cs-. a-.^\thumb a-. e-._4 e-. cs-.  cs-. a-. |
  \clef bass
  a e' e cs cs a a e e cs cs a | \break

%bar 720
  a4 <a e' cs'>\downbow <a e' cs'>\downbow
  <a e' cs'>\downbow <a e' cs'>\downbow <a e' cs'>\downbow
  \mark \default
  a r8 r4 r8 |
  R1*6/8*7 | \bar"|."
}

% Local Variables:
% LilyPond-master-file: "Concerto-A-Major.ly"
% End:
