\version "2.19"
\language "english"

\header {
  title = "Concerto in D Major"
  subtitle = "For violoncello"
  composer = "Haydn, op. 701"
  tagline = ##f
}

\paper {
  #(set-default-paper-size "a4")
}

global = {
  \key d \major
  \time 4/4
  \tempo "Allegro Moderato"
  \compressFullBarRests
}

music = \relative c' {
  \global
  \clef bass
  % Music goes here
  d,4\p r r2 |
  d4 r r2 |
  R1*4
  d8\f d' d d d2:8 |
  d,8\p d' d[ d] d4( cs) |
  fs,8\f r e r d r cs r |
  d4\f ds e2:8 |
  e2:8 e2:8 |
  e2:8 e8 e e r | \break

  a,4\p r a r |
  a8 r e' r a,4 r16 a cs a |
  e'4 r8 d8 cs4 r8 cs |
  d8 d e e a,16 a' a, a' a,[ a' a, a'] |
}

\score {
  \new Staff \music
  \layout {}
}

