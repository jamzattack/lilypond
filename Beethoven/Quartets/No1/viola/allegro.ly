viola-allegro = \new Voice \relative c {
  \set Staff.instrumentName = #"Viola "
  \clef alto

  f4~ f8( g16 f) e8-. f-. |
  c4 r r |

  f4~ f8( g16 f) e8-. f-. |
  d4 r r |

  d'2.\<( |
  c2.\>~) |

  c4\!( d g) |
  c8( b c b c4) | \break

}
