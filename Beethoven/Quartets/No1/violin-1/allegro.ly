violin-one-allegro = \new Voice \relative c' {
  \set Staff.instrumentName = #"Violin 1 "

  f4~ f8( g16 f) e8-. f-. |
  c4 r r |

  f4~ f8( g16 f) e8-. f-. |
  d4 r r |

  f'4~\< f8( g16 f) e8-. f-. |
  g2\>( bf,4\!) |

  a2( d8. bf16) |
  a2( g4) | \break


  f4~\f f8( g16 f) e8-. f-. |
  c4 r r |

  f4~ f8( g16 f) e8-. f-. |
  d4 r r |

  f'4~\p f8( g16 f) e8-. f-. |
  a2\<( g4\>) |

  g4~\! g8( a16 g) fs8-. g-. |
  bf2\<( a4\>) |

  a4~\! a8( bf16 a) g8-. a-. |
  c4._"cresc."( bf8 a g) | \break

  
  f2( \grace {g16 f e f} a8 g) |
  f4\p r r |
  
  r2. |
  f4.\sf( g16 f) e8-. f-. |

  r2. |
  g4.\sf( a16 g) fs8-. g-. |

  r2. |

  c4.\sf( d16 c) b8-. c |
  d4.\sf( ef16 d) cs8-. d-. | \break
}
