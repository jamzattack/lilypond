\version "2.19"
\language "english"

\header {
  title = "Quartet No. 1"
  subtitle = "For 2 Violins, Viola, and Cello"
  composer = "Ludwig Van Beethoven"
  tagline = ##f
}

global= {
  \time 3/4
  \key f \major
}

\include "violin-1/allegro.ly"
\include "violin-2/allegro.ly"
\include "viola/allegro.ly"
\include "cello/allegro.ly"

\score {
  \new StaffGroup <<
    \new Staff << \global \violin-one-allegro >>
    \new Staff << \global \violin-two-allegro >>
    \new Staff << \global \viola-allegro >>
    \new Staff << \global \cello-allegro >>
  >>
  \layout { }
  \midi { }
}
