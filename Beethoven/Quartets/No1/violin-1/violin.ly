\version "2.19"
\language "english"

\include "allegro.ly"

global = {
  \time 3/4
  \key f \major
 }

\score {
  \new Staff \with {
  } { \global \violin-one-allegro }

  \layout { }
  \midi { }
}