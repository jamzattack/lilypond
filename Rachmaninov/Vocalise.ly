\version "2.18.2"
\language "english"

\header {
  dedication = "Antonina Vasilievna Nezhdanova"
  title = "Vocalise"
  subtitle = "Op.34 Nº.14 (Comp. 1915) orig. C# minor"
  composer = "Sergei Rachmaninov\n (1873-1943)"
  poet = "For 6 Cellos"
  arranger = "Orfeo Mandozzi"
  tagline = ##f
}

\paper {
  #(set-default-paper-size "a4")
  annotate-spacing = ##f
  ragged-last-bottom = ##f
  bottom-margin = 24
}

global = {
  \key e \minor
  \time 4/4
  \tempo "Lentamente. Molto cantabile"
}



cello = \relative c'' {
  \global
  #(define afterGraceFraction (cons 15 16))
  \clef tenor
  % Music follows here.
  \repeat volta 2 {
    r4 r8 g16(\mp fs g4.) e16\<( fs |
    \time 2/4
    g8 fs16\! e\> fs8 d16 e\! |
    \time 4/4
    << fs2~ {s8\< s4.\>} >> fs8\<) e16( d e8\> c16 d) |
    << e2~\! {s8\< s4.\>} >> e8\! << d16( {s64 s s s\<} >> c16 << b8. {s8 s16\!} >> a16 | \break
% bar 5,	line 2
    b8.\> << c16 {s32\! s}>> b4~\< b16) c( e g a8. << b16 {s32 s\!} >> |
    g4.) f8( e4\> \afterGrace ds\trill {cs16 ds} |
    e2) \tempo "poco più animato" << r8 {s16 s\mf} >> << e4( {s16 s s\< s\!} >> d!8\! |
    cs2~ cs8) << cs8( {s16 s\<} >> d8\! e8) |
    e(\tenuto\> d16 e\! fs4~ fs8) b,16( << cs16 {s32 s\<} >> d8 e16 fs | \break
% bar 10, line 3
    gs2\!~ gs8) << es8( {s16 s\<} >> fs8\! gs) |
    gs(\f\< fs16 gs\! a4~ a8) gs\>( fs e! |
    fs4.\mf) e16\tenuto( ds\tenuto e4.) b16\p( cs |
    ds8\< e16 fs\! g!4\f~ g8) fs(\> e d) | \break
% bar 14, line 4
    d\mf( cs16 d e4~ e8) d16( cs d8 a16 b |
    cs8\< d16 e\! f4\f~ f8)\> e( d c) |
    b2\! as4.\p( e'8) |
    \time 2/4 \tempo "ritenuto" fs8.( e16) d16( e cs8) | \time 4/4
  }
  \alternative {
    { b1 | \break }
    { b2 r8 \tempo "poco più mosso" fs'4\mf\<( e8) | }
  }
% bar 19, line 5
  \repeat volta 2 {
    ds4(\> fs~\! fs8) fs4(\< e8
    ds4\> fs~\< fs8)\! g\f( fs e |
    d! \tempo " un poco ritenuto" e fs\> << d8 { s16\! s} >> b4~
    b16) cs( ds^\markup {\italic dolciss. } a') | \break
% bar 23, line 6
    g2\p \tempo "a tempo" r8 g4\mf( << fs8 { s32 s\< s\! s} >> |
    e4\tenuto g~ g8) g( fs e16 d) |
    e4\tenuto( << g4 {s16 s\< s s\!} >> g8) a(\f g fs) |
    e fs << g8 { s16 s\> } >> e8\! c4~ c16\< d( e g) | \break
% bar 27, line 7
    a2(\! << b,4.) {s8 s\> s\<}>> a'8( |
    b2.\ff a4 |
    gs8 fs16 gs << a4~ {s16 s\> s s} >> a8) g(^\markup { \italic dim. } fs << e8) {s16\! s} >> |
    ds8( d16 ds e4~ e8) d4( c8 |
    \time 2/4 b8.\p a16 g a fs8 | \time 4/4
  }
% bar 32. line 8
  \alternative {
    { e2) r8 fs'4(\mf e8) | }
    { e,2 r4 g8(\p a | }
  }
  \time 2/4 b8 a16 b c8 b16 c |
  \time 4/4 d4.) c16( d e4 fs) |
  g4. a8 b2~ | \break
% bar 37, line 9
  b8 \clef treble c \tempo " rit." d e \tempo " tempo" c( b) a( g16 a) |
  b4. a8 g4 \afterGrace fs(\trill { e16 fs) }
  e1^~ |
  \time 2/4 e2~ |
  \time 4/4 e1\fermata \bar "|."|
}

\score {
  \new Staff \with {
    instrumentName = "Cello 1"
  } { \clef bass \cello }
  \layout { }
}
