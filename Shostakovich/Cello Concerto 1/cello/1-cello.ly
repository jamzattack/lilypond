\version "2.20"
\language "english"

global = {
  \compressFullBarRests
}

\include "1.ly"

\markup \fill-line {
  \center-column {
    \fontsize #5 { "I." }
  }
}
\score {
  \new Staff {
    \cello_one
  }
  \midi { }
  \layout { }
}
