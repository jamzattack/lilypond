\version "2.20"
\language "english"

\header {
  title = "Superior"
  composer = "Brendan Taaffe"
  poet = "Old Primitive Baptist Hymnal"
  tagline = ##f
}

global = {
  \key f \minor
  \time 3/2
  \tempo 2 = 80
  \partial 2
  \set melismaBusyProperties = #'()
}

soprano = \relative c'' {
  \global

  \slurDashed
  af2 |
  af1 af2 |
  af1 df4( c) |
  ef1 ef2 |
  c1 af2 |
  \slurSolid
  af1 af4( bf) |
  c1 c4( bf) |
  bf1 bf2 | \break

  bf1 f2 |
  af1 bf2 |
  af2 c1 |
  ef1 ef4( c) |
  df1 af2 |
  af1 g2 |
  af1. | \bar "|."
}

alto = \relative c' {
  \global

  \slurDashed
  c2 |
  df1 f2 |
  af1 f4( f) |
  g1 af2 |
  af1 f2 |
  df1 ef2 |
  ef1 f2 |
  ef1 ef2 |

  f1 f2 |
  f2.( f4) \slurSolid f( af) |
  af2 ef1 |
  ef1 ef2 |
  f1 ef2 |
  c1 df2 |
  c1. | \bar "|."
}

tenor = \relative c' {
  \global
  \clef "treble_8"

  af2 |
  f1 af2 |
  ef1 af2 |
  bf1 af2 |
  af1 c2 |
  df1 c4( bf) |
  af1 af2 |
  bf1 bf2 |

  bf1 c2 |
  df1 df2 |
  c2 af1 |
  bf1 bf4( c) |
  df1 c4( bf) |
  af1 bf2 |
  af1. | \bar "|."
}

bass = \relative c {
  \global
  \clef "bass"

  af2
  df1 df2 |
  c1 af4( c) |
  ef1 ef2 |
  ef1 f2 |
  f1 ef2 |
  c1 ef2 |
  ef1 ef2 |

  df1 c2 |
  bf1 bf2 |
  ef2 af1 |
  g1 g4( af) |
  af1 ef2 |
  ef1 ef2 |
  af,1. | \bar "|."
}

\score {
  \new ChoirStaff
  <<
    \soprano
    \addlyrics {
      \tiny
      How of -- ten has my im -- pat -- ient heart stood re -- bel _ to the _ sky? And
      yet, and yet, O match -- less grace, thy _ thun -- der si -- lent lies.
    }
    \alto
    \addlyrics {
      \tiny
      How of -- ten have I _ cap -- tive lay in sor -- row's sweet, sweet arms? And
      yet, and yet, the world _ calls me home with her un -- end -- ing charms.
    }
    \tenor
    \addlyrics {
      \tiny
      When time has come to leave this world, and wings I _ wish to fly. And
      throw my flesh, my soul, my all. And _ weep, and _ love, and die.
    }
    \bass
  >>
  \layout {
  }
}