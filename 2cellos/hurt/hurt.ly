\version "2.19"
\language "english"

% Source: https://invidio.us/watch?v=ozNEdMcWZvQ

\header {
  title = "Hurt"
  subtitle = "for Two Cellos"
  composer = "Nine Inch Nails?"
  tagline = ##f
}

global = {
  \key a \minor
  \time 4/4
}

first = \relative c {
  \global
  \clef "bass"
  R1*4 \break

  \set Staff.midiInstrument = "synthbrass 1"

% bar 5
  r2. e8 g~ |
  g4 e8 d~ d4 c8 e8~ |
  e2. a,8 c~ |
  c4 a8 d~ d4 a8 e'~ |
  e2. e8 g~ |

% bar 10
  g4 e8 d~ d4 c8 e8~|
  e2. a,8 c~ |
  c4 a8 d~ d4 a8 a~ |
  a2. \breathe e'8 g~ |

% bar 14
  g4 e8 d~ d4 c8 e8~ |
  e2. a,8 c~ |
  c4 a8 d~ d4 a8 e'~ |
  e2. e8 g~ |

% bar 18
  g4 e8 d~ d4 c8 e8~|
  e2. a,8 c~ |
  c4 a8 d~ d4 g,8 g~ |
  g2. r4 |

  \set Staff.midiInstrument = "electric bass (finger)"

% bar 22
  <a e' c'>4^"pizz." <a e' c'> <a e' c'> <a e' c'> |
  <f c' a'>4 <f c' a'> <f c' a'> <f c' a'> |
  <c g' e' c'>4 <c g' e' c'> <c g' e' c'> <c g' e' c'> |
  <g' d' b'>4 <g d' b'> <g d' b'> <g d' b'> |

% bar 26
  <a e' c'>4 <a e' c'> <a e' c'> <a e' c'> |
  <f c' a'>4 <f c' a'> <f c' a'> <f c' a'> |
  <c g' e' c'>4 <c g' e' c'> <c g' e' c'> <c g' e' c'> |

% bar 29
  \repeat unfold 2 \relative c, {
    <g' d' b'>4 <g d' b'> <g d' b'> <g d' b'> |
    <a e' a>4 <a e' a> <a e' a> <a e' a> |
    <f c' a'>4 <f c' a'> <f c' a'> <f c' a'> |
    <g d' b'>4 <g d' b'> <g d' b'> <g d' b'> |
  }

  a'8 e' <e c' e>2 r4 |

  \set Staff.midiInstrument = "synthbrass 1"
  R1*3 \clef "tenor"
% bar 41
  r2. e'8^"II" g~ |
  g4 e8 d~ d4 c8 e8~ |
  e2. a,8 c~ |
  c4 a8 d~ d4 a8 e'~ |
  e2. e8 g~ |

% bar 46
  g4 e8 d~ d4 c8 e8~|
  e2. a,8 c~ |
  c4 a8 d~ d4 a8 a~ |
  a2. e'8 g~ |

% bar 50
  g4 e8 d~ d4 c8 e8~ |
  e2. a,8 c~ |
  c4 a8 d~ d4 a8 e'~ |
  e2. e8 g~ |

% bar 54
  g4 e8 d~ d4 c8 e8~|
  e2. a,8 c~ |
  c4 a8 d~ d4 g,8 g~ |
  g2. r4 |

}

second = \relative c {
  \global
  \clef "bass"

  \set Staff.midiInstrument = "acoustic bass"
% bar 1
  a8^"pizz." e' <e c' e>2 r4 |
  c8 e <e c' e>4 d8 a' <a fs'>4 |

% bar 3
  \repeat unfold 5 {
    a,8 e' <e c' e>2 r4 |
    c8 e <e c' e>4 d8 a' <a fs'>4 |
  }

% bar 13
  a,8 e' <e c' e>2 \breathe r4 |
  c8 e <e c' e>4 d8 a' <a fs'>4 |

% bar 15
  \repeat unfold 3 {
    a,8 e' <e c' e>2 r4 |
    c8 e <e c' e>4 d8 a' <a fs'>4 |
  }

  <g, d' b'>4 <g d' b'> <g d' b'> <g d' b'> |
  \set Staff.midiInstrument = "synthbrass 2"
% bar 22
  g'8^"arco"\ffff e4 e4 d8 c8 c~ |
  c2. r4 |
  r4 c4 d8 c4 d8~ |
  d2. r4 |

% bar 26
  g8 e4 e d8 c c~ |
  c2 c4 a8 c~ |
  c2 d4 g,8 g~ |
  g2. r4 |

% bar 30
  d'8 c4 e d8 c c~ |
  c2. r4 |
  d2 d4 g,8 d'~ |
  d2. r8 d |

% bar 34
  d8 c4 e d8 c c~ |
  c2. r4 |
  d4. c8 e4 g,8 a~ |
  a2. r4 | \pageBreak

  \set Staff.midiInstrument = "acoustic bass"
% bar 38
  \repeat unfold 9 {
    c8 e <e c' e>4 d8 a' <a fs'>4 |
    a,8 e' <e c' e>2 r4 |
  }
  c8 e <e c' e>4 d8 a' <a fs'>4 |

% bar 57

}

\score {
  \new StaffGroup <<
    \new Staff {
      \first
    }
    \new Staff {
      \second
    }
  >>
  \layout { }
  \midi { }
}
