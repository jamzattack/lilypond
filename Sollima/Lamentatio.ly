\version "2.18.2"
\language "english"

\header {
  title = "Lamentatio"
  composer = "Giovanni Sollima"
  tagline = ""
}

\paper {
  #(set-paper-size "a4")
  #(set-global-staff-size 20 )
  }
\layout {
  \context {
    \Score
    \override StaffGrouper.staff-staff-spacing.padding = 3
    \remove "Bar_number_engraver"
    \RemoveEmptyStaves
    }
}

global = {
  \key c \major
  \time 4/4
  \clef tenor
}

auxiliary = \relative c' {
  \tempo "Adagio e libero"
  \global
% line one, page one
  \grace s4 r2 \grace g4\glissando a2~^\markup {\bold \small "voce (nasale)"}
  a1
  a2 bf
  a b!4( c)
  a4( r4)\fermata a2
  bf b \break
% line two, page one
  c g
  a1~\>
  << a2\glissando {s8 s\! s s} >>g8 r r4 \bar "!"
  r4\fermata \grace g4\glissando a2.~
  a4 a2 bf4
  a( bf) c2
  bf4~ bf2 a4~ \break
% line three, page one
  a4\>( r4\!)\fermata a2
  g1~
  g8 r\fermata r4 bf2
  b! c4 g\glissando
  a1~\>
  << a2\glissando {s8 s\! s s} >> g8 r r4 \break \bar "||"
% line four, page one
  \bar ".|:"
  \repeat volta 2 {
    R1*4*5/8
  }
% line five, page one
a16^\markup{\bold \small "arco"}[\accent a a] a\accent a a a[\accent a] a\accent a
  a16[\accent a a] a\accent a a a[\accent a] a\accent a
  a16[\accent a a] a\accent a a a[\accent a] a\accent a
  a16[\accent a a] s8. s8 s \pageBreak
% line one, page two
  \time 6/8
  R2.*4
% line two, page two
  \time 5/8
  \repeat volta 2 {
   R1*4*5/8
  }
% line three, page two
  R1*4*5/8
% line four, page two
  \time 6/8
  R1*4*6/8
% line five, page two
  \time 7/8
  R1*2*7/8
  \time 4/8
  R1*4/8
  \time 7/8
  R1*7/8
% line six, page two
  R1*7/8
  \time 13/16
  R1*2*13/16
  \time 5/8
  R1*5/8
% line seven, page two
  \time 3/8
  R1*3/8
  \time 5/8
  R1*3*5/8
% line one, page three
  \time 3/8
  R1*3/8
  \time 5/8
  R1*5/8
  \time 7/8
  R1*7/8
% line two, page three
  \teeny
  <e b'>8_\markup{\teeny "IV"}^\markup{\lower #1 \teeny "III"}^\markup{\teeny \bold \raise #3 "pizz (mano destra) tra il pont. e la cordierra"}
         <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>

% line three, page three
  <d' a'> <d a'> <d a'> <d a'> <d a'> <d a'>
  <d a'> <d a'> <d a'> <d a'> <d a'> <d a'>
  <d a'> <d a'> <d a'> <d a'> <d a'> <d a'>
  <d a'> <d a'> <d a'> <d a'> <d a'> <d a'>
  <d a'> <d a'> <d a'> <d a'> <d a'> <d a'>
  <d a'> <d a'> <d a'> <d a'> <d a'> <d a'>
  <d a'> <d a'> <d a'> <d a'> <d a'> <d a'>
  <d a'> <d a'> <d a'> <d a'> <d a'> <d a'>
  % line four, page three
  <e, b'>_\markup{\teeny "IV"}^\markup{\teeny "III"} <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'>
  <e b'> <e b'> <e b'> <e b'> <e b'> <e b'> \break
% line five, page three
  \time 23/16 \stemUp \normalsize
  r8 <d a'>16-.->^\markup{\small "col legno"}_\markup{\lower #5 \small "funky!"} r r8 r <d a'>16-.-> r8 <d a'>16-.-> r16 r4 r8 r4
  r8 <d a'>16-.-> r r8 r <d a'>16-.-> r8 <d a'>16-.-> r16 r4 r8  r4
% fin
}


cello = \relative c {
  \global
% line one, page one
  \set glissandoMap = #'((0 . 1))
  \grace d'4\glissando <d,~ a''~>1\mf\<\fermata
  <d a''>2\!-\markup {\italic "espress."} <d bf''>
  <d a''> <d g'>
  <d f'> <d g'>
  <a' d>2\fermata\breathe <d, e'>
  <d f'> <d g'> \break
% line two, page one
  \set glissandoMap = #'((1 . 1))
  <d e'>\glissando <d c'>
  <d d'>1~\<
  << <d d'>2\glissando {s8 s\! s s}>> <d c'>8\accent\staccato r r4 \bar "!"
  <d a''>1\fermata
  <d c''>4 <d bf''> <d a''> <d g'>
  <d fs'> <d g'> <d a''>2
  <d g'>4 <d g'>2 <d~ f'!~>4 \break
% line three, page one
  <d f'>4(\> r\!)\fermata  <d f'>2
  \grace {ef'8( d)} <d, ef'>2 <d d'>~
  <d d'>8 r\fermata <d e'!>4 <d f'>2
  <d g'> <d e'>4 <d c'>\glissando
  <d d'>1~\<
  << <d d'>2\glissando {s8 s\! s s}>> <d c'>8\accent r r4 \break \bar "||"
% line four, page one
  \set Timing.defaultBarType = "|"
  \time 5/8 \tempo "Allegro" \bar ".|:"
  \repeat volta 2 {
    <d a''>16[\accent\f <d a'> <d a'>] <d bf''>\accent <d a'> <d a'> <d a''>[\accent <d a'>] <d g'>\accent <d a'>
    <d f'>16[^\markup {\italic "sim."} <d a'> <d a'>] <d g'> <d a'> <d a'> \stemUp <d d'>[ <d a'>] <d e'> <d a'>
    \stemNeutral <d f'>16[ <d a'> <d a'>] <d g'> <d a'> <d a'> \stemUp <d e'>[ <d a'>] <d c'> <d a'>
    \clef bass \stemNeutral
    <f d'>[ <d a'> <d a'>] <bf f'> <g d'> <g d'> <ef bf'>[ <c g'>] <f c'> <c g'>
  }
% line five, page one
  \clef tenor
  e''!16[\accent-\markup {\teeny "II"} d, d] f'\accent d, d e'[\accent d,] d'\accent d,
  cs'[ d, d] d' d, d a'[ d,] bf' d,
  cs'[ d, d] d' d, d bf'[ d,] g d
  a'[ d, d] <f c'> <d a'> <d a'> <g d'>[ <d a'>] <af' ef'> <d, a'> \pageBreak
% line one, page two
  \time 6/8 <<
    \new Voice = "top" { \stemUp
      a''16[^> a, a] c' bf a g[ a,] fs'[ a,] g' a, \noBreak
      a'[ a, g'] g a, f'! f^>[ a,] ef'^>[ a,] d^> a \noBreak
      ef'[ a, f'] f a, d ef[ ef] c[ c] a a \noBreak
    }
    \new Voice = "bottom" { \stemDown
      d,16[ d d] d d d d[ d] d[ d] d d \noBreak
      d[ d d] d d d d[ d] d[ d] d d \noBreak
      d[_> d d] d_> d d d[_._> d]_._> d[_._> d]_._> d_._> d_._> \noBreak
    } >>
  \clef bass
  <d d'>[-> a'-. f-. d]-. g[-> d-. bf-. g-.] c-> g-. ef-. c-. \break
% line two, page two
  \time 5/8
  \clef tenor \bar ".|:"
  <d' a''>16[-> <d a'> <d a'>] <d bf''>-> <d a'> <d a'> <c' a'>[-> <d, a'>] <bf' g'>-> <d, a'> \noBreak
  <a' fs'>16[ <d, a'> <d a'>] <b'? g'> <d, a'> <d a'> \stemUp <fs d'>[ <d a'>] <gs e'> <d a'> \stemNeutral \noBreak
  <a' f'!>16[ <d, a'> <d a'>] <b'! g'> <d, a'> <d a'> \stemUp <g! e'!>[ <d a'>] <e! c'!> <d a'> \stemNeutral \noBreak
  \clef bass
  <f! d'>[ <d a'> <d a'>] <bf f'> <g d'> <g d'> <ef bf'>[ <c g'>] <f c'> <c g'> \break
% line three, page two
  <<
    \new Voice = "top" { \stemUp
      d''16[^\markup{\teeny "II"} d, d]  a'_\markup{\teeny "I"} d, d d'[ d,] c' d,
      b'![ d, d] c' d, d g[ d] a' d,
      b'![ d, d] c' a a a[ d,] f d
      g[ d d] f d d
   }
    \new Voice = "bottom" { \stemDown
      f16[_\markup {\lower #6 \dynamic mp}_\markup{\teeny "III"} g, g] ef''^\markup{\teeny "II"} g,,\< g f'[ g,] ef' g,
      d'\![ g, g] a' g, g\< b![ g] c g
      d'!\![ g, g] a' d, d c[ g] a g
      b[ g g] bf g g
   }
  >>
  \stemDown <ef bf'>16[ < c g'>] \stemNeutral <f c'> <c g'> \break
% line four, page two
  \time 6/8 \clef tenor
  <<
    \new Voice = "top" { \stemUp
      a'''16[^> a, a] c' bf a g[ a,] fs'[ a,] g' a, \noBreak
      a'[ a, g'] g a, f'! f^>[ a,] ef'^>[ a,] d^> a \noBreak
      ef'[ a, f'] f a, d ef[ ef] c[ c] a a \noBreak
   }
    \new Voice = "bottom" { \stemDown
      d,16[\f d d] d d d d[ d] d[ d] d d \noBreak
      d[ d d] d d d d[ d] d[ d] d d \noBreak
      d[_> d d] d_> d d d[_> d]_> d[_> d]_> d_> d_> \noBreak
   }
  >>
  \clef bass
  <d d'>[-> a'-. f-. d]-. g[-> d-. bf-. g-.] c-> g-. ef-. c-. \break
% line five, page two
  \time 7/8
  <d a'>8-.->[_\markup{\teeny \bold "come 'distorted'"}
               <f' c'>-.->] <g d'>-.->[ <c,, g'>16-.->] <d a'>8-.->[ <f' c'>16-.->] <af ef'>8-.->[ <c,, g'>-.->]
  <d a'>8-.->[ <f' c'>-.->] <g d'>-.->[ <c,, g'>16-.->] <d a'>8-.->[ <f' c'>16-.->] <af ef'>8-.->[ <c,, g'>-.->]
  \time 4/8 \clef tenor 	\once \override DynamicLineSpanner.outside-staff-priority = #600
  \tupletUp \tuplet 3/2 8 {d'''16\<_\markup{\teeny "I"}[_( c a] g_\markup{\teeny "II"} f d c_\markup{\teeny "III"}[
                           bf g] f\!_\markup{\teeny "IV"} ef c_)}
  \time 7/8 \clef bass
  <d, a'>8-.->[ <f' c'>-.->] <g d'>-.->[ <c,, g'>16-.->] <d a'>8-.->[ <f' c'>16-.->] <af ef'>8-.->[ <c,, g'>-.->] \break
% line six, page two
  <f c'>8-.->[ <af' ef'>-.->] <bf f'>-.->[ <c,, g'>16-.->] <g'? d'?>8-.->[ <bf'? f'>16-.->] <c g'>8-.->[ <c,, g'>-.->] \noBreak
  \time 13/16
  <g' d'>8-.->[ <c' g'>-.->] <d a'>-.->[ <c,, g'>16-.->] <a' e'>8-.->[ <c' g'>16-.->] <ef bf'>8-.->[ <c,, g'>16-.->] \noBreak
  <g' d'>8-.->[ <c' g'>16-.->] <d a'>8-.->[ <c,, g'>16-.->] <a' e'>8-.->[ <c' g'>16-.->] <ef bf'>8-.->[ <c,, g'>-.->] \noBreak
  \time 5/8
  <d' a' f'>16_\markup{\teeny \bold "rude e alla corda"}[ <d a'> <d a'>] <d bf' g'> <d a'> <d a'> <d a' f'>[ <d a'>] <d bf' g'> <d a'> \break
% line seven, page two
  \time 3/8
  < \parenthesize c, c' g' e'>[ <d' a'> <d a'>] <c a' f'> <d a'> <d a'>
  \time 5/8
  <bf f' d'>[ <d a'> <d a'>] <bf g' e'?> <d a'> <d a'> <bf f' d'>[ <d a'>] <bf g' e'> <d a'>
  <f, c' a'>[ a' a <a b?>->] <g, d' a' b>4\arpeggio r8
  <d' a' f'>16-.->[ a'-.-> a-.->] <d, bf' g'>-.-> a'-.-> a-.-> <d, a' f'>-.->[ a'-.]-> <d, bf' g'>-.-> a'-.-> \pageBreak
% line one, page three
  \time 3/8
  < \parenthesize c,, c' g' e'>->-.[ <d' a'>->-. <d a'>->-.] <c a' f'>->-. <d a'>->-. <d a'>->-.
  \time 5/8
  <bf f' d'>-.->[ a'-.-> a-.->] <bf, g' e'?>-.-> a'-.-> a-.-> <bf, f' d'>-.->[ a'-.->] <bf, g' e'>-.-> a'-.->
  \time 7/8
  <f, c' a'>[ a' a <a b?>->] <g, d' a' b>8\arpeggio r16
            \override NoteHead.no-ledgers = ##t \grace {g'16( d' a g' d b' g)}
            \revert NoteHead.no-ledgers {\once \override NoteHead.style = #'cross d'8\fermata}
                      r16 r4 \break
% line two, page three
  \time 6/8
  <d, a'>8.->_\markup{\teeny \bold "pizz (mano sinistra)"}_\markup{\lower #3 \teeny "I"}_\markup{\lower #4 \teeny "II"}
           <d a'>-> <c g'>-> <c g'>->
  <d a'>-> <d a'>-> <c g'>-> <c g'>->
  <bf f'>-> <bf f'>-> <c g'>-> <c g'>->
  <f, c'>->_\markup{\teeny "II"}_\markup{\lower #2 \teeny "III"} <f c'>-> <g d'>-> <g d'>->
  <d' a'>->_\markup{\teeny "I"}_\markup{\lower #2 \teeny "II"} <d a'>-> <c g'>-> <c g'>->
  <d a'>-> <d a'>-> <c g'>-> <c g'>->
  <bf f'>-> <bf f'>-> <ef, bf'>->_\markup{\teeny "II"}_\markup{\lower #2 \teeny "III"} <ef bf'>->
  <f c'>-> <f c'>-> <g d'>-> <g d'>->
% line three, page three
  <b? fs'>8.->_\markup{\teeny "I"}_\markup{\lower #2 \teeny "II"} <b fs'>-> <a e'>-> <a e'>->
  <b fs'>-> <b fs'>-> <a e'>-> <a e'>->
  <g d'>-> <g d'>-> <a e'>-> <a e'>->
  <d, a'>-> <d a'>-> <e b'>-> <e b'>->
  <b' fs'>-> <b fs'>-> <a e'>-> <a e'>->
  <b fs'>-> <b fs'>-> <a e'>-> <a e'>->
  <g d'>-> <g d'>-> <c, g'>-> <c g'>->
  <d a'>-> <d a'>-> <e b'>-> <e b'>->
% line four, page three
  <d' a'>8.->_\markup{\teeny "I"}_\markup{\lower #2 \teeny "II"} <d a'>-> <c g'>-> <c g'>->
  <d a'>-> <d a'>-> <c g'>-> <c g'>->
  <bf f'>-> <bf f'>-> <c g'>-> <c g'>->
  <f, c'>->_\markup{\teeny "II"}_\markup{\lower #2 \teeny "III"} <f c'>-> <g d'>-> <g d'>->
  <d' a'>->_\markup{\teeny "I"}_\markup{\lower #2 \teeny "II"} <d a'>-> <c g'>-> <c g'>->
  <d a'>-> <d a'>-> <c g'>-> <c g'>->
  <bf f'>-> <bf f'>-> <ef, bf'>->_\markup{\teeny "II"}_\markup{\lower #2 \teeny "III"} <ef bf'>->
  <f c'>-> <f c'>-> <g d'>-> <g d'>->
% line five, page three
\time 23/16 \stemDown
  d,8[^>^+ r16^\markup{\smaller "pizz (mano sin)" } a'] c[ g a8] r16 d,8[^> r16 a'] c[ g a8]
        \tuplet 3/2 { <d c'>16-.(^\markup{\raise #5 \small "arco ord."} <d b'>-. <d a'>-.)}
        <c g'>8_([\glissando <g d'>_)]
  d8[^>^+ r16^\markup{\smaller "pizz (mano sin)" } a'] c[ g a8] r16 d,8[^> r16 a'] c[ g a8]
        \tuplet 3/2 { <d c'>16-.(^\markup{\raise #5 \small "arco ord."} <d b'>-. <d a'>-.)}
        <c g'>8_([\glissando <g' d'>_)\flageolet_\markup{\teeny "II"}_\markup{\lower #2 \teeny "III"}]
% fin
}


\score {
  \new StaffGroup{
    \set Timing.defaultBarType = ""
    <<
    \new Staff \with {instrumentName = ""} \auxiliary
    \new Staff \with {instrumentName = ""} \cello
  >>
  }
}
