\version "2.19"
\language "english"

\header {
  title = "Concerto in E minor"
  subtitle = "for Violoncello and Orchestra"
  composer = "Edward Elgar, Op. 85"
  tagline = ##f
}

\paper {
  #(set-default-paper-size "a4")
}

global = {
  \key e \minor
  \time 4/4
  \tempo "Adagio"
}

music = \relative c' {
  \global
  \clef bass
  % Music goes here
  <g, e' b'>2\ff_\markup{\italic "nobilmente"} <e b' g' e'> |
  <c g' e' d'>8\sf c''^\markup{\italic "largamente"} b[ c] << <ds,, b' fs'>4( {a''4} >> a) |
  
}

\score {
  \new Staff \music
  \layout {}
}

