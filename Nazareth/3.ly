\version "2.20"
\language "english"

III = \relative c' {
  \key g \major
  \time 2/4
  \clef "bass"
  \set Staff.instrumentName = #"Cello III"
  \repeat volta 2 {
    \repeat volta 2 {
% bar 1
      r8\f <g b>8 r <g b> |
      r8 <fs c'> r <fs c'> |
      r8 <g b>\p r <g b> |
      r8 <fs c'> r <fs c'> |
      r8 g[ g g] |
      fs8. fs16 r fs8 fs16 |

% bar 7
      g8 g g g |
      fs8. fs16 r8 fs |
      r8 g[ g g] |
      fs8. fs16 r fs8 fs16 |
      g8 g g g |
      fs8. fs16 r8 fs |

% bar 13
      r8 b\f[ b\> b] |
      b4\! r |
      r8 b\f[ b\> b] |
      b4\! r |
      r8 g4 r8 |
      e4 ef |
      fs8. fs16 r fs8 fs16 |
      g4 r |
      r16 <g b>8\p <g b>16 <g b>8 <g b> |

% bar 22
      r16 <fs c'>8 <fs c'>16 r8 <fs c'> |
      r16 <g b>8 <g b>16 <g b>8 <g b> |
      r16 <fs c'>8 <fs c'>16 <fs c'>4 |
      r16 <g b>8 <g b>16 <g b>8 <g b> |
      r16 <fs c'>8 <fs c'>16 r8 <fs c'> |

% bar 27
      r16 <g b>8 <g b>16 <g b>8 <g b> |
      r16 <fs c'>8 <fs c'>16 <fs c'>8 <fs c'> |
      r16 <fs b>8 <fs b>16 <fs b>8 <fs b> |
      <g b>16 <g b>8 <g b>16 <g b>8 b |
      r16 <e, a>8 <e a>16 <e a>8 <e a> |

% bar 32
      r16 <fs a>8 <fs a>16 <fs a>8 <fs a> |
      r16 g8 g16 g8 g |
      r16 e8 e16 ef4 |
      r16 d8 d16 <d a'>8 <d a'> |
    }
    \alternative {
      { d4 <b d> | }
      { d4 r8 cs' | }
    } \bar "||" \key d \major

% bar 38
    d16 a8 a16 fs8 a |
    <g a>16 <g a>8 <g a>16 e8 g |
    fs16 d8 d16 fs8 f |
    e16 g8 g16 e8 g |
    fs16 d8 d16 a8 d |
    fs16 a8 a16 fs8 a |

% bar 44
    r8 <es b'> r <es b'> |
    <fs a>8 <fs a> e g |
    fs16 d8 d16 fs8 a |
    <g a>16 <g a>8 <g a>16 e8 g |
    fs16 d8 d16 fs8 f |

% bar 49
    e16 g8 g16 e8 r |
    fs16 a8 a16 r as8 as16 |
    r16 b8 b16 r b8 b16 |
    r16 a8 a16 r <g a>8 <g a>16 |
    <d a'>4 <d a'> | \pageBreak
  }
}
