\version "2.19"
\language "english"

\header {
  title = "Recuerdos de la Alhambra"
  subtitle = "Transposizione per violoncello"
  composer = "Francisco Tàrrega"
  % piece = "Carlo Maria Barato"
  tagline = ##f
}

\paper {
  #(set-default-paper-size "a4")
}

global = {
  \key f \minor
  \time 3/4
  \tempo "Andante"
  \set Timing.beamExceptions =
    \beamExceptions { 32[ 32 32 32] 32[ 32 32 32] 32[ 32 32 32]
      32[ 32 32 32] 32[ 32 32 32] 32[ 32 32 32] }
}

music = \relative c {
  \global
  \clef bass
  \repeat volta 2 {
%bar 1
    f,32-.( c''-. c-.) c-. c,-.( c'-. c-.) c-. af^"simili" c c c
      c, c' c c g bf bf bf c, bf' bf bf |
    f,^1 af'^2 af af c, af' af af f af af af c, af' af af g bf bf bf c, bf' bf bf | \break

%bar 3
    f, c'' c c c, c' c c af c c c c, c' c c af c c c c, c' c c |
    f,, c'' c c c, c' c c af c c c c, c' c c bf!^2 df! df df af, df' df df | \break

%bar 5
    af,^1 ef'' ef ef ef, ef' ef ef c ef ef ef ef, ef' ef ef bf df df df ef, df' df df |
    af, c' c c ef, c' c c af c c c ef, c' c c bf df df df ef, df' df df | \break

%bar 7
    af, ef'' ef ef ef, ef' ef ef c ef ef ef ef, ef' ef ef c ef ef ef ef, ef' ef ef |
    af,, c' c c ef, c' c c af c c c ef, c' c c bf df df df ef, df' df df | \break

%bar 9
    df,^1 af''! af af af,^1 af' af af f^2 af^4 af af af, af' af af ef g! g g af, g' g g |
    df,!^2 f' f f af, f' f f df^4 f f f af, f' f f bf,,!^1 g'' g g df!^1 g g g | \break

%bar 11
    \stemDown
    c,,,\flageolet f''^3 f f g, f' f f c^4 e e e g, e' e e c e e e g, e' e e |
    c,, e'' e e bf e e e c e e e bf e e e c e e e bf e e e | \pageBreak

%bar 13
    f,,^1 gf''^3 gf gf a, gf' gf gf c,^4 gf' gf gf a, gf' gf gf c,^4 f!^3 f f a,! f' f f |
    a,,^1 ef'' ef ef f, ef' ef ef c ef ef ef f, ef' ef ef df^2 f f f f, f' f f | \break

%bar 15
    bf,,^1 ef'^3 ef ef c^1 ef ef ef bf^4 df^2 df df f, df' df df bf df df df f, df' df df |
    bf,^1 df' df df f, df' df df bf^4 df df df ef, df' df df bf df df df ef, df' df df | \break

%bar 17
    \stemNeutral
    bf,^2 c' c c f, c' c c g c c c f, c' c c g^3 bf^1 bf bf f bf bf bf |
    df,,!_1 af''! af af b, af' af af f!^1 af af af b, af' af af f^1 b b b b, b' b b | \break

%bar 19
    c,, af''!_3 af af c, af' af af e^4 g^2 g g c, g' g g e g g g c, g' g g |
    c,, g'' g g c, g' g g e g g g c, g' g g e g g g c, g' g g | \break
  }
  \key f \major
  \repeat volta 2 {
%bar 21
    f,^1 c'' c c c, c' c c a c c c c, c' c c g bf bf bf c, bf' bf bf |
    f, a' a a c, a' a a f a a a c, a' a a g bf bf bf c, bf' bf bf | \break

%bar 23
    f,^1 c''^4 c c c, c' c c a c c c c, c' c c a c c c c, c' c c |
    f,, c'' c c c, c' c c a c c c c, c' c c a c c c c, c' c c | \break

%bar 25
    f,,^1 d''^4 d d d,^1 d'^4 d d bf d d d d, d' d d bf d d d d, d' d d |
    \stemDown
    f,,^1 bf''^4 bf bf bf, bf' bf bf g bf bf bf
      bf, bf' bf bf bf,^2 d^4 d d d,^1 d'^4 d d | \pageBreak

%bar 27
    f,,^1 d''^4 d d bf d d d a c c c c,^1 c' c c a c c c c, c' c c |
    f,, c'' c c c, c' c c a c c c c, c' c c a c c c c, c' c c | \break

%bar 29
    d,,^1 f''^4 f f d,\flageolet f' f f e,^1 f' f f a, f' f f f, f' f f d, f' f f |
    e,,!^2 e''!^4 e e e, e' e e gs, e' e e b^1 e e e gs,^3 b^1 b b e, b' b b | \break

%bar 31
    a, d' d d e, d' d d a^4 c^2 c c e, d' d d a c c c e, d' d d |
    a, c' c c e, c' c c a c c c e, c' c c a c c c e, c' c c | \break

%bar 33
    g,\flageolet bf'^4 bf bf df, bf' bf bf df, bf' bf bf
      bf, bf' bf bf c, bf' bf bf df, bf' bf bf |
    c,, a'' a a c, a' a a c, a' a a e a a a c,^2 g'^2 g g bf,^1 g'^2 g g | \break

%bar 35
    f,^2 g' g g c, g' g g c, f^1 f f c f f f d^3 f f f e^4 f f f |
  }
  \alternative {
    { f,-1 f'-4 f f c f f f a, f' f f c^1 g'^1 g g f a a a g bf bf bf | \break }
%bar 37
    { f,^1 f' f f c f f f c f f f e g g g f af af af g bf bf bf  | }
}
  f,-2 f'-1 f f c f f f c f f f c f f f c f f f c f f f | \break

%bar 39
  f, f' f f df f f f df f f f df f f f df f f f df f f f |
  f,^1 g'^1 g g df g g g f af^2 af af df, af' af af f^4 bf^3 bf bf df, bf' bf bf | \pageBreak

%bar 41
  f, c'' c c c, c' c c a^2 c c c c, c' c c a c c c c, c' c c |
  f,, c'' c c c, c' c c a c c c c, c' c c a c c c c, c' c c | \break

%bar 43
  c,,\flageolet c''^4 c c e,^3 c'^4 c c e, c' c c e, c' c c e, c' c c e, c' c c |
  c,, bf'' bf bf d, bf' bf bf c,^2 a'^3 a a e^4 a a a c,^2 g' g g bf,^1 g' g g | \break

%bar 45
  f,-2 f'-2 f f c f f f c f f f cs f f f d f f f cs f f f |
  f, f' f f c! f f f c f f f c f f f c f f f c f f f | \break

%bar 47
  f,-2 f' f f df f f f df f f f df f f f df f f f df f f f |
  f,-1 g'-1 g g df g g g f af^2 af af df, af' af af f^4 bf^3 bf bf df, bf' bf bf | \break

%bar 49
  f, c'' c c c, c' c c a c c c c, c' c c a c c c c, c' c c |
  f,, c'' c c c, c' c c a c c c c, c' c c a c c c c, c' c c | \break

%bar 51
  d,, e''^2 e e c,^1 e' e e c^4 e e e c,^1 e' e e c^4 e e e c,^1 e' e e |
  c,,\flageolet d''^4 d d c,^1 d' d d a^2 c c c c, c' c c g bf bf bf c, bf' bf bf | \break

%bar 53
  g, a' a a c, a' a a f a a a c, a' a a d,^2 a' a a f a a a |
  f, a' a a c, a' a a f a a a c,,\flageolet a''^4 a a gs, a' a a a, a' a a | \break

%bar 55
  \override Glissando.breakable = ##t
  \override Glissando.after-line-breaking = ##t
  f, a' a a c, a' a a f a a a c, a' a a d,^2 a' a a f a a a |
  f, a' a a c, a' a a f a a a a^2 a\flageolet a a c^4 a a a f' a, a a\glissando | \break
  a'2.\flageolet |
  <f,, c' f a>^\markup{\italic"pizz."} | \bar "|."
}

\score {
  \new Staff \music
  \layout {}
}
