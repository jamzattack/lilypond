\version "2.20"
\language "english"

cello_two = \relative c'' {
  \global

  \time 3/4
  \tempo "Moderato" 4 = 66
  \clef "treble"

% bar 1
  R1*3/4*13 |
  R1*3/4 | \time 2/2 %TODO ← add horn & string cues
  R1 |
  a4-1\p a b8( c) b( c) |
  a4 a a2 |
  g4.( f8) e4-4( c) |

% bar 19
  g'4.-4( f8) e-4( c df c) | \clef "tenor"
  b?4-1( e-4) c8-1( a?-0 df-3 c) |
  b?4-1( e-4)_\markup{\italic "cresc."} c8-2( b a?-4 g) |
  f4 f( bf) bf |
  ef,2_\markup{\dynamic mf \italic "dim."} e'-4 | \clef "treble" \break

% bar 24
  a4-1\upbow\p a b8( c) b( c) |
  a4 a a2-2\< |
  g?4.\mp\>( a8) b4-1\<( d) |
  g,?4.-1\>( a8) b-1\<( d-2 ef d) |
  cs4.-2\>( a8\thumb) b-1\< d ef d-3\! | \break

% bar 29
  cs4.( d8) c4.-2( df8) |
  bf4.-1( cf8) af4.-2( a8) |
  gf2-1 f?8-4( ef d?4) |
  gf2-2_\markup{\italic "cresc."} f8-4( ef d?4) | \clef "tenor"
  df4-1\mf^\markup{\italic "tenuto"}( a?8-4) a-1( c a) c a | \break

% bar 34
  df4-4_\markup{\italic "dim."}( a8) a( c a) c a |
  ef8-3_"II"\p( a,4-1) a8-3( d!-4 a d a) |

}