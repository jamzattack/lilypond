\version "2.20"
\language "english"

II = \relative c {
  \key g \major
  \time 2/4
  \clef "bass"
  \set Staff.instrumentName = #"Cello II"
  \repeat volta 2 {
    \repeat volta 2 {
% bar 1
      g8 d'' b, d' |
      a,8 d' c, d' |
      g,,8\p d'' b, d' |
      a,8 d' c, d' | \clef "tenor"
      r8 b[ b b] |

% bar 6
      c8. c16 r c8 c16 |
      b8 b b b |
      c8. c16 r8 c |
      r8 b[ b b] |
      c8. c16 r c8 c16 |

% bar 11
      b8 b b b |
      c8. c16 r8 c |
      r8 ds\f[ ds\> ds] |
      e4\! r |
      r8 cs\f[ cs\> cs] |
      c!4\! r | \clef "bass"

% bar 17
      r8 b4 r8 |
      a4 a |
      c8. c16 r c,8 c16 |
      b16 d8 g16 \clef "treble" b16 d8 g16 |
      b8\p b b b |

% bar 22
      c8. c16~ c16 c8 c16 |
      b8 b g g |
      fs8. d16 d4 |
      r8 b' b b |
      c8. c16~ c16 c8 c16 |

% bar 27
      b8 b g g |
      fs8. d16 d4 | \clef "tenor"
      r8 ds[ ds ds] |
      e4 r |
      r8 cs8[ cs cs] |
      c!4 r |

% bar 33
      r8 b4 r8 |
      a16 a8 a16 a8 r |
      r8 fs fs c' |
    }
    \alternative {
      { b4 g' | }
      { e4 r8 \clef "treble" a |}
    } \bar "||" \key d \major

% bar 38
    d16 cs8 b16 a8 b |
    cs16 b8 a16 g16 fs8 e16 |
    d16 cs8 b16 a8 b |
    cs4 r8 a' |
    d16 cs8 b16 a8 b |

% bar 43
    cs16 cs,8 b16 \clef "bass" a16 gs8 fs16 |
    es16 d8 cs16 b16 a8 gs16 |
    fs4 r8 \clef "treble" a'' |
    d16 cs8 b16 a8 b |

% bar 47
    cs16 b8 a16 g16 fs8 e16 |
    d16 cs8 b16 a8 b |
    cs4 r8 \clef "tenor" a |
    fs'16 g8 es16 fs16 bs,8 cs16 |

% bar 51
    e16 d8 as16 cs16 b8 g16 |
    fs16 g8 gs16 a16 b8 cs16 |
    d4 d | \pageBreak
  }
}
