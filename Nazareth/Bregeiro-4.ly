\version "2.20"
\language "english"

\header {
  title = "Bregeiro"
  composer = "Ernesto Nazareth"
}

\include "4.ly"

breaks = {
% bar 1
  \repeat unfold 4 { s2 } \break

% bar 5
  \repeat unfold 4 { s2 } \break

% bar 9
  \repeat unfold 4 { s2 } \break

% bar 13
  \repeat unfold 5 { s2 } \break

% bar 18
  \repeat unfold 6 { s2 } \break

% bar 24
  \repeat unfold 5 { s2 } \break

% bar 29
  \repeat unfold 5 { s2 } \break

% bar 34
  \repeat unfold 6 { s2 } \break

% bar 40
  \repeat unfold 5 { s2 } \break

% bar 45
  \repeat unfold 4 { s2 } \break

% bar 49
  \repeat unfold 5 { s2 } \break

}

\score {
  \new Staff << \IV \breaks >>
}

