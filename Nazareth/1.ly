\version "2.20"
\language "english"

I = \relative c {
  \key g \major
  \time 2/4
  \clef "bass"
  \set Staff.instrumentName = #"Cello I"
  \repeat volta 2 {
    \repeat volta 2 {
% bar 1
      <<
	{ % just rests
	  \stemUp
	  \repeat unfold 4 { r2 | } \clef "tenor"
	  r8
	}
	{ % cue from IV
	  \new CueVoice {
	    \stemDown
	    g8 d'16 d, b'8 d16 d, |
	    a'8 d16 d, c'8 d16 d, |
	    g8 d'16 d, b'8 d16 d, |
	    a'8 d16 d, c'8 d16 d, | \clef "tenor"
	    d'8
	  }
	}
      >>
      \stemNeutral
      g'8[ e e] |

% bar 6
      fs8. fs16 r d8 d16 |
      e8 e b b |
      d 8. d16 r8 d |
      r8 g[ e e] |
      fs8. fs16 r d8 d16 |

% bar 11
      e8 e b b |
      d 8. d16 r8 d |
      r8 b'\f[ fs\> fs] |
      a16 g8 fs16 e16 ds8 e16 |

% bar 15
      r8 a\f[ e\> e] |
      g16 fs8 e16\! d16 cs8 d16 |
      r8 g-- r16 fs8 e16 |
      d16 c8 b16 a16 gs8 a16 |
      d8. d16 r a8 b16 |

% bar 20
      g16\> b8 d16 \clef "treble" g16 b8 d16 |
      g8\f g[ e e] |
      fs8. fs16~ fs d8 d16 |
      e8 e[ b b] |
      d4 r |

% bar 25
      r8 g8 e e fs8. fs16~ fs d8 d16 |
      e8 e[ b b] |
      d4 r |

% bar 29
      r8 b fs fs a16 g8 fs16 e16 ds8 e16 |
      r8 a[ e e] |
      g16 fs8 e16 d16 cs8 d16 |

% bar 33
      r8 g r16 fs8 e16 |
      d16 cs8 b16 \clef "tenor" a16 gs8 a16 |
      r8 d r16 a8 b16 |
    }
    \alternative {
      { g4 g' | }
      { g,4 r8 e'8 |}
    } \bar "||" \key d \major

% bar 38
    d16 cs8 b16 a8 b |
    cs16 b8 a16 \clef "bass" g16 fs8 e16 |
    d16 cs8 b16 a8 b |
    cs4 r8 \clef "tenor" e' |
    d16 cs8 b16 a8 b |

% bar 43
    cs16 cs8 b16 a16 gs8 fs16 | \clef "bass"
    es16 d8 cs16 b16 a8 gs16 |
    fs16 g!8 gs16 a16 b8 cs16 |
    d16 cs'8 b16 a8 b |

% bar 47
    cs16 b8 a16 g16 fs8 e16 |
    d16 cs8 b16 a8 b |
    cs4 r8 \clef "tenor" a' |
    fs'16 g8 es16 fs bs,8 cs16 |

% bar 51
    e16 d8 as16 cs16 b8 g16 |
    fs16 g8 gs16 a16 b8 cs16 |
    d4 d | \pageBreak
  }
}
