\version "2.19"
\language "english"

\header {
  title = "Concerto No. 1"
  subtitle = "in C Major"
  composer = "Josef Haydn"
  % Remove default LilyPond tagline
  tagline = ##f
}

\paper {
  #(set-paper-size "a4")
}

global = {
  \key c \major
  \time 2/2
}

cello = \relative c' {
  \global
  \tempo "Allegro Molto"
  \teeny \set Score.markFormatter = #format-mark-box-alphabet
  \clef treble \stemUp \compressFullBarRests

  c'2~^\markup{"Vln. I"} c8 f e d
  c r a' r g r r4
  c,2~ c8 f e d
  c8 r g16( f e f) e8 \clef bass r8 r4
  \normalsize
  R1*16 \break \small \mark \default
% line two, page one
  \clef treble g'2~^\markup{"Vln. I"} g8 e d c
  b r d16( c b c) d8 \clef bass r8 r4
  R1*8 \mark \default
  R1*4 \clef treble
  c16[^\markup{"Vln. I"} d e f] g a b c a,[ b c d] e f g a \break
% line three, page one
  g4 f16 e d c d2\trill
  c16[ d e f] g a b c a,[ b c d] e f g a
  g4 f16 e d c d2\trill
  c8[ e16 f] g8 e c[ e,16 f] g8 e
  c4 <c' e> <c e> r \break \mark \default
% line four, page one
  \normalsize \clef tenor \stemNeutral
  \override NoteHead.color = #black
  \override Stem.color = #black
  \override Beam.color = #black
  \override Rest.color = #black
  c,1~\p\upbow^\markup{\bold \teeny "II"}
  c~
  c~\downbow
  c8[\mf d16\downbow e] f g a b c8 c,-!\upbow c-! c-!
  c2~ c8 f-! e-! d-!
  c8-! r <f a>-! r <e g>-! r r4 \break
% line five, page one
  c2~ c8 f8-! e-! d-!
  c[ d16\downbow e] f g a b c8 g-!\upbow g-! g-!
  gs8( a) a4~ a8 c,-! c-! c-!
  c8[ d16 e] f g! a b c8 a-!-3 a-! a-! \break
  fs8( g) g4~ g8 c,-! c-! c-!
  c[ d16 e] f! g a b c8 g-!^\markup{\bold \teeny "I"} g-! g-!
  e8( f) f4~ f8[ g,16^\markup{\bold \teeny "III"} a] b c d e
  f[ d e f] g a b c d4. f,8\upbow \break
  e16[( d c d)] c8 c c4 r	\clef bass
  c,,16\upbow c'( b c d[ c b c)] c,8 r r4
  \stemDown g'16[ f'( e f] g f e f) \stemNeutral g,8 r r4
  e'16[ c'( b c] d c b c) f, c'( b c d[ c b c]) \break
  g16[ c( b c] d c b c) a c( b c d[ c b c])
  g8[ e, f a'] g e, d[ c''32( b a b)]
  c8[ e,, f a'] g e, d[ c''32( b a b)] \break
  c8[ c] d16( c b c d8[) d] e16( d c d
  e8[) e] f16( e d e f8[) f] g16( f e f
  g8[) g] a16( g f g) a4.( c,8)
  b16[ g fs e] d c b a g4 r \pageBreak

  d''2~ d8 b a g
  <fs a>4 <g b> <a c> r

}

\score {
  \new Staff \with {
    instrumentName = "Cello"
  } {\clef bass \cello }
  \layout {
      \context{
        \Score
        \override NonMusicalPaperColumn.line-break-permission = ##f
      }
  }
}
