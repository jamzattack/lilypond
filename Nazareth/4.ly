\version "2.20"
\language "english"

IV = \relative c {
  \key g \major
  \time 2/4
  \clef "bass"
  \set Staff.instrumentName = #"Cello IV"
  \repeat volta 2 {
    \repeat volta 2 {
% bar 1
      g8->\f d'16\upbow d, b'8-> d16 d, |
      a'8-> d16 d, c'8-> d16 d, |
      g8->\p d'16 d, b'8-> d16 d, |
      a'8-> d16 d, c'8-> d16 d, |

% bar 5
      \repeat unfold 4 {
	g8-> d'16 d, b'8-> d16 d, |
	a'8-> d16 d, c'8-> d16 d, |
      }

% bar 13
      b'8\f ds16\> b a8 b16 a |
      g16 b8 g16 e b'8 e,16\! |
      a16\f cs8\> a16 g8 a16 g |
      fs16 d'8 fs,16 c'!16 d8 c16\! |
      b8 d16 b b8 b'16 b, |

% bar 18
      c8 c,16 c' a8 a'16 a, |
      d8 d,16 d' d,8 d'16 d, |
      g4\> r |
      g16\p d'8 d16 d,8 d' |
      a16 d8 d16 d,8 d' |
      g,16 d'8 d16 d,8 d' |

% bar 24
      a16 d8 d16 d,8 d' |
      g,16 d'8 d16 d,8 d' |
      a16 d8 d16 d,8 d' |
      g,16 d'8 d16 d,8 d' |
      a16 d8 d16 d,8 d' |

% bar 29
      b16 a'8 a16 b,8 b' |
      e,,16 e'8 e16 g,8 g' |
      cs,,16 g''8 g16 a,8 g' |
      d,16 d'8 d16 fs,8 a |
      b16 g'8 g16 b,8 d |

% bar 34
      c16 e8 e16 c4 |
      a16 c!8 c16 d,8 c' |
    }
    \alternative {
      { <g d'>4 <g d'> | }
      { <g d'>4 r8 a | }
    } \bar "||" \key d \major
    d,16 fs'8 fs16 a,8 fs' |
    e,16 cs'8 cs16 a8 g' |

% bar 40
    d,16 fs'8 fs16 fs,8 f |
    e16 cs'8 cs16 a8 cs |
    d,16 fs'8 fs16 fs,8 a |
    e16 a8 a16 a8 a |
    gs8 b cs, b' |

% bar 45
    fs8 a e a |
    d,16 fs'8 fs16 fs,8 d' |
    e,16 cs'8 cs16 a8 g' |
    d,16 a'8 a16 fs8 f |

% bar 49
    e16 cs'8 cs16 a8 cs |
    d16 fs8 fs16 cs16 fs8 fs16 |
    b,16 fs'8 fs16 g,16 g'8 g16 |
    a,16 fs'8 fs16 a,8 g' |
    fs4 d, | \pageBreak
  }
}
