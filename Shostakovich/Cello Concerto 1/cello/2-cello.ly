\version "2.20"
\language "english"

global = {
  \compressFullBarRests
}

\include "2.ly"

\markup \fill-line {
  \center-column {
    \fontsize #5 { "II." }
  }
}
\score {
  \new Staff {
    \cello_two
  }
  \midi { }
  \layout { }
}
