\version "2.20"
\language "english"

\header {
  title = "Bregeiro"
  composer = "Ernesto Nazareth"
}

\include "3.ly"

breaks = {
% bar 1
  \repeat unfold 6 { s2 } \break

% bar 7
  \repeat unfold 6 { s2 } \break

% bar 13
  \repeat unfold 9 { s2 } \break

% bar 22
  \repeat unfold 5 { s2 } \break

% bar 27
  \repeat unfold 5 { s2 } \break

% bar 32
  \repeat unfold 6 { s2 } \break

% bar 38
  \repeat unfold 6 { s2 } \break

% bar 44
  \repeat unfold 5 { s2 } \break

% bar 49
  \repeat unfold 5 { s2 } \break

}

\score {
  \new Staff << \III \breaks >>
}


