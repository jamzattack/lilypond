\version "2.20"
\language "english"

cello_one = \relative c {
  \global
  
  \key ef \major
  \time 2/2
  \tempo "Allegretto" 2=116

  \clef "bass"

% bar 1
  r4 g\p\upbow-4-.-- ff cf'_"IV" |
  bf1~ |
  bf4-. g-4 ff cf' |
  bf1~ |
  bf4-. cf af ef'-2_"III" |
  d4 f d a'-3 |
  af4 ef8-1 f gf4 a,_"IV" | \break

% bar 8
  d4 c8 d ef4 gf,-1 |
  c4-1 g!-4 ff cf' |
  bf1~ |
  bf4-. g-4 ff cf' |
  bf1~ |
  bf4-. af'-3_"III" g f |
  e4-3 d8 e f?4 c_"III" |
  ef4 af,-4 g f | \break

% bar 16
  e4-3 d8 e f?4 c |
  ef!4 ef c g'-2 |
  fs1~ |
  fs4 g8 af? bf4( f'!-4) |
  e1~ |
  e4 e8_\markup {\italic "cresc."}\upbow e e f g-1_"II" a |
  bf4 bf8-1_"II" bf bf c df-1 ef | \clef "treble" \break

% bar 23
  e4-1 <c g'>\f <df-4 ff-1> <d-1 cf'-3> |
  <ef bf'>4 ef'2-3->\downbow d4 |
  c4-3 <ef,-1 c'-3> <e c'> <f-2 df'-3> |
  <fs-2 c'-1> b8-1 c d4 a-1 |
  c4 <ef,-1 c'-3> <e c'> <f-2 df'-3> |
  <fs-2 c'-1> b8-1 c d4 a\thumb | \time 3/2 \break

% bar 29
  c4-1 ef2->\upbow d->\< c4->-3~ | \time 2/2
  c4 <ef,-1 c'-3>\! <e c'> <f-2 df'-3> |
  <fs-2 c'-1> b8-1 c d4 a\thumb |
  c4 <f, c'> <fs a> <g-1 e'-3> |
  <gs-1 ds'-2>1~ |
  <gs ds'>4 <f c'> <fs a> <g-1 e'-3> | \time 3/2
  <af!-1 ef'!-2> gf'2->-3\upbow f->\upbow ef4 | \time 2/2 \break

% bar 36
  d4-3 d d8 c b4 |
  d4 d d8 c b4 | \time 3/2
  d4-2 ef2-> d-> c?4 | \time 2/2
  b4-3 b b8 a gs4 |
  b4 b b8 a gs4 | \time 3/2
  b4 c2->-3 b-> a4\< | \time 2/2 \break

% bar 42
  af4-4\sf af f c'-3 | \time 5/4 |
  b2 af?4-4 f c'-3 |
  b2_\markup {\italic "dim."} g?4-4 e b'? | \time 2/2
  bf1\p~ |
  bf4 g-4 ff cf'-3 |
  bf1~ |
  bf4 bf8\upbow bf bf af g-4 f | \break

% bar 49
  ff4 ff-4 df af'-4 |
  g4 g8 g g f! ef-4 d! | \clef "tenor"
  df4 df df df |
  df4 c8 df ef4 bf |
  df4-2 df df df |
  df4 c8 df ef4 bf-1 |
  df2->-4_\markup{\italic "cresc."} df-> | \break

% bar 56
  df2-> df-> |
  df4-> df c? bf |
  a4 r r2 | \clef "bass"
  r4 <g,-0 ef'-1>8 <g-0 g'-3> <g g'> \clef "tenor"
  <g'-3 ef'-4> <g ef'> <bf-2 g'-4> |
  <bf g'>4 r r2 | \clef "bass"
  r4 <g,-0 ef'-1>8 <g-0 g'-3> <g g'> \clef "tenor"
  <g'-3 ef'-4> <g ef'> <bf-2 g'-4> | \break

% bar 62
  <bf g'>4 <d,-2 cf'-4>8 <d d'!> <d d'> <cf'-4 ef-1> <cf ef> <cf-2 af'-4> |
  <cf af'>4 <af-2 bf\thumb>8\downbow <af-2 cf\thumb>
  <af cf> <af-2 d!-1> <af d> <af-2 f'-4> |
  <af f'>4 r af8-2\downbow af <g!-1 a-1> <g a> |
  <f-2 d'-4>4 r \clef "bass" a,8-1\downbow a gf' gf |
  cf4 r r2 | \pageBreak

% PAGE TWO

% bar 67
  r4 <g,-0 ef'-1>8 <g-0 g'-3> <g g'> \clef "tenor"
  <g'-3 ef'-4> <g ef'> <bf-2 g'-4> |
  <bf g'>4 r r2 | \clef "bass"
  r4 <g,-0 ef'-1>8 <g-0 g'-3> <g g'> \clef "tenor"
  <g'-3 ef'-4> <g ef'> <bf-2 g'-4> |
  <bf g'>4 r r2 |
  R1*3 |
  r4 <bf g'>\ff\downbow <bf g'>\downbow <bf g'>\downbow | \break

% bar 75
  <bf g'>4\downbow ef,-3_"IV"\downbow ef\downbow ef\downbow |
  ef4\downbow <d-0 fs'-3>\downbow <d fs'>\downbow <d fs'>\downbow |
  <d fs'>4\downbow \clef "bass"
  af-2_"IV"\downbow af\downbow af\downbow |
  af2\trill af\trill |
  af2\trill af\trill |
  af2\trill af\trill |
  bf2\trill bf\trill |
  R1 | \time 3/2 \break

% bar 83
  R1*3/2 | \time 2/2
  R1 | \time 3/2
  R1*3/2 | \time 2/2 \clef "treble"
  g''?2-2\ff g | \time 3/2
  g2 g4 af g2 | \time 2/2
  g2 g | \time 3/2
  g2 g4 af g2 | \time 2/2
  fs2-1 cs-1 | \break

% bar 91
  g'?2-2\ff g | \time 3/2
  g2 g4 af g2 | \time 2/2
  g2 g | \time 3/2
  g2 g4 af g2 | \time 2/2
  gs2 cs-3 | \time 3/2
  b4->-2( gs\thumb) b( gs) c!8->-3( b a gs\thumb) | \break

% bar 97
  b4->( gs) b( gs) c!8->( b a gs) |
  b4 as2->-1 fs->-3_"II" ds4->-1 |
  b'4->-2( gs\thumb) b( gs) c8->( b a gs) |
  b4->( gs) b( gs) c!8->( b a gs) | \break

% bar 101
  b4 c2->-1 d-> ef?4-2 | \time 2/2
  d2 ef4-1( f) | \time 3/2
  gf4->( ef) gf->( ef) g8->-3( gf-3 ff ef) |
  gf4->( ef) gf->( ef) g8->( gf ff ef) |
  gf4-. ef2-> f!-> gf4-2~ | \time 2/2 \break

% bar 106
  gf?4 f2 gf!4 |
  g!2-2\downbow g2 | \time 3/2
  g2 g4 af g2 | \time 2/2
  g2 g | \time 3/2
  g2 g4 af g2 | \time 2/2
  fs2-3 cs-1 | \time 3/2 \break

% bar 112
  e2-3 f,!4-1 g af? e8-1 fs |
  g4->\downbow( e) g( e) af?8->-4( g-4 fs e) |
  g4->( e) g( e) af8->-4( g-4 fs e) | \time 2/2
  g4-4( af8-1 bf) cf4( bf8 af) | \time 3/2 \break

% bar 116
  g4->-4( e) g( e) af8->-4( g-4 fs e) | \clef "tenor"
  ef!4->-4( c) ef->( c) ff8->-4( ef-4 df c) | \time 2/2
  b2-1\downbow b | \time 3/2
  b2 b4 c b2 | \time 2/2
  bf!-2 f-4 | \break

% bar 122
  e2_\markup {\italic "dim."} e | \time 3/2
  e2\f e4 f e2 | \clef "bass" \time 2/2
  ef!2 bf-4 | \time 3/2
  ef2-2 e,2.-1_\markup {\italic "dim."}( fs4) |
  g4-1\mf( af) g( af) g8( af g af) |
  g4-1( d!) ef( d) ef8( d ef d) | \break

% bar 127
  g4-1( af) g( af) g8( af g af) |
  bf4-2_"IV"( cf) bf( cf) bf8( cf bf af) |
  g?4-1( af) g( af) g8( af g af) |
  bf4-2_"IV"( cf) bf( cf) bf8( cf bf af-1) | \break

% bar 131
  g?4-1( c,) g'( c,) df8-1( c df c) |
  g'4-1( c,) g'( c,) df8-1( c df c) |
  g'4-. c,8( df) ef4-. df8( ef) ff4
  ef8-1\upbow( ff | \key c \major \time 2/2
  gf4-.) r4 r2 |
  R1*3 | \pageBreak

% PAGE THREE

% bar 138
  R1*3 |
  r4 fs'-1\upbow\f\> g-1 a |
  bf4\p bf g \clef "tenor" d'-0 |
  cs4 fs,-1 g-1 a |
  bf4 bf g d'-0 | \break

% bar 145
  cs4 <cs a'>\ff\downbow <cs a'>\downbow <cs a'>\downbow |
  <cs a'>4\downbow ef,-4_"IV"\downbow ef\downbow ef\downbow |
  ef4\downbow <d-0 fs'-3>\downbow <d fs'>\downbow <d fs'>\downbow |
  <d fs'>4\downbow gs8\upbow as b!4 as8-1_"I" b |
  cs4 bs cs-1_\markup{\italic "dim."} ds |
  e4\p e cs gs'-4 |
  g!4 c,!-1_"II" cs-1 ds | \break

% bar 152
  e4 e cs gs'-3_"II" |
  g!4 \clef "treble" <g-2 ef'-3>\ff\downbow
  <g ef'>\downbow <g ef'>\downbow |
  <g ef'>4\downbow \clef "bass" bf,-1_"II"\downbow bf\downbow bf\downbow |
  bf4\downbow <g, af'-1(>8\upbow bf') cf4 <d,-0 bf'-1(>8 c') |
  df4\downbow <g,, af'-1(>8\upbow bf') cf4 <d,!-0 bf'-1(>8 c') | \break

% bar 157
  df4 c!_\markup{\italic "dim."} cs-1 ds | \clef "treble" \time 3/2
  e4-1\p\downbow( g) e( g) af8-4( g-4 f? e) | \time 2/2
  g4-. bf-.->-2\upbow g-.->\thumb d'-.->-3 |
  cs4-.-> fs,8-1 gs a4-0 e8-1 fs | \time 3/2
  g!4( e) g( e) af8( g-4 f! e) | \clef "tenor" \time 2/2 \break

% bar 162
  ef4-4( c?) bf-( c) |
  a?4->-0 <a-1 fs'-.-3>\ff\downbow
  <fs-1 ds'-.-3>\downbow <bf-2 g'!-.-4>\downbow |
  <a-1 fs'-3>\downbow \clef "bass" <c,,? d'!-1(>8\upbow ef')
  f4-. <g,-0 ef'-1(>8 f') |
  fs4-. \clef "treble" <bf-2 g'-4>8\downbow <bf g'>
  <bf g'>4 <cf-2 af'-4>8\downbow <cf af'> |
  <cf af'>4 <ef-2 cf'-3> <cf-2 af'-4> <ef-1 c'-3> |
  <d-1 b'?-3>4 ef8-1_"I" f gf4 f8-1 gf | \break

% bar 168
  af4-4 <e-2 c'-3>\downbow <e c'>\downbow <e c'>\downbow |
  <e c'>4\downbow <f-2 a!\thumb>\downbow <f a>\downbow <f a>\downbow |
  <f a>4\downbow <d, d''-3>\downbow <g'-3 b-1>\downbow <d, d''>\downbow |
  R1*4 |
  R1*3 | \clef "tenor" \break

% bar 178
  r8 fs-1\f\upbow g-1 a bf bf g d'-1_"I" |
  cs8-1 fs,-1 g-1 a bf bf g d'-3_"II" |
  cs8 <cs a'> <cs a'> <cs a'> <cs a'> ef,-1_"III" ef ef |
  ef8 <d-0 fs'-3> <d fs'> <d fs'> <d fs'> gs-1 as b! | \break

% bar 182
  cs8-1_"I" ds e e e e cs gs'-4 |
  g!8 c,!-1_"II" cs-1 ds e e cs gs'-3_"II" | \clef "treble"
  g!8 <g-2 ef'-3> <g ef'> <g ef'> <g ef'> \clef "tenor" bf,-2_"II" bf bf |
  bf8 af bf cf df-2_"I" cf df ef | \clef "treble" \break

% bar 186
  e!8-1 g e g af-4 g-4 f e |
  g8-4 bf-2 g\thumb d'-3 cs-2 a!\thumb e-1 fs |
  g8 e g e af-4 g-4 f! e |
  ef8-4 c bf-1 c a!-0 fs'-3 ds-1 g?-4 | \pageBreak

% PAGE FOUR

% bar 190
  fs8-4 d!-1 ds e! fs-3 e fs g |
  gs8-1 b-3 gs c-3 b-2 es,-1 fs g |
  a8-1 <e-2 c'-3> <e c'> <e c'> <e c'> <f!-2 a\thumb> <f a> <f a> |
  <f a>8 <d, d''> <d b''> <d d''> <f' a> <d-1 b'-3> <d b'> <ef-1 c'-3> | \break

% bar 194
  <ef c'>8 <e-1 a\thumb> <e a> <f-2 df'-3>
  <f df'> <ef-1 c'-3> <ef c'> <df-1 bf'-3> |
  <df bf'>8 a' a af af g\thumb af a |
  bf8 a a af af g\thumb af bf |
  b8-1 d b d ef-3 d-3 c bf | \break

% bar 198
  d8 b d b ef-3 d-3 c b |
  d8-1 ef d ef f d_\markup{\italic "cresc."} ef-1 f |
  gf8-3 f-3 ef d d ef d ef |
  d8-4 b?-1 d b ef-3 d-3 c b | \break

% bar 202
  d8-1 ef d ef f d ef-1 f | %TODO ↓ fix chord fingering
  fs4 r \clef "tenor" <cs,,-3 d-0 cs'-1>->\downbow\ff r |
  <cs d cs'>4->\downbow r <cs d cs'>->\downbow r |
  <cs d cs'>4->\downbow r cs'->\downbow d-> |
  cs4-> r <cs, d cs'>->\downbow r | \time 3/2 \break

% bar 207
  <cs d cs'>4->\downbow r <cs d cs'>->\downbow
  r <cs d cs'>->\downbow r |
  <cs d cs'>->\downbow r cs'->\downbow d-> cs4-> r | \time 2/2
  r4 <bf g'-4>8\downbow <bf g'> r4
  <f'-2 af\thumb>8\downbow <f af> | \clef "treble"
  r8 fs-3\upbow_"I" g a!-1 bf a bf-1 c | \break

% bar 211
  cs4 r \clef "tenor" <cs,,-3 d-0 cs'-1>->\downbow r |
  <cs d cs'>4->\downbow r <cs d cs'>->\downbow r |
  <cs d cs'>4->\downbow r cs'->\downbow d-> |
  cs4-> r <cs, d cs'>->\downbow r | \time 3/2
  <cs d cs'>4->\downbow r <cs d cs'>->\downbow
  r <cs d cs'>->\downbow r |
  <cs d cs'>->\downbow r cs'->\downbow d-> cs4-> r | \time 2/2 \break

% bar 217
  r8 cs-1\upbow d e-1 f e f g |
  gs8-4 es gs es a-0 gs-4 fs es |
  gs8 es gs es a-0 gs-4 fs es |
  bf'8->-3( a) r4 <cs,,-2 d-0 d'-1>->\downbow r4 | \break

% bar 221
  <cs d d'>4->\downbow r <cs d d'>->\downbow r |
  <cs d d'>4->\downbow r d'-1->\downbow ef-> |
  d4-> r <cs,-2 d-0 d'-1>\downbow r | \time 3/2
  <cs d d'>4->\downbow r <cs d d'>->\downbow r <cs d d'>->\downbow r |
  <cs d d'>4->\downbow r d'->\downbow ef-> d-> r | \time 2/2 \break
  
% bar 224
  r4 <c!-2 af'-3>8\downbow <c af'> r4 <cs-2 a'-3>8\downbow <cs a'> |
  r4 <d-2 bf'-3>8\downbow <d bf'> r4 <ds-2 b'-3>8\downbow <ds b'> |
  R1*11 | \pageBreak %TODO ← add horn cue?

% PAGE FIVE
  
% bar 239
  r4 cf-1\upbow\ff af-4 ef'-2 |
  d4 f d a'!-3 |
  af4 ef8-1 f gf4 a,-0 |
  d4-3 c8_\markup{\italic "dim."} d ef4 gf,-3 |
  c4-1 g! ff cf'-3_"II" | \key ef \major
  bf1~\p |
  bf4 g-4 ff cf' |
  bf1~ | \break

% bar 247
  bf4 af g-4 f |
  e4 d8-0 e f?4 c |
  ef4-2_"III" \clef "bass" af,-2 g f-4 |
  e4 d8 e f?4 c |
  ef!4 ef c g'-2 |
  fs1~ |
  fs4 g8-1 af! bf4( f'!-4) |
  e1~ | \break

% bar 255
  e?4 e8_\markup{\italic "cresc."} e e f g-1 af |
  bf4 bf8-1 bf \clef "tenor" bf cf df-2 ef |
  df4 c! df ef |
  e4-2 ds8\upbow ds e-1 e fs fs |
  g4 r \clef "treble" g'-4\downbow r | \clef "bass" \break

% bar 260
  c,,,,4\ff r <d-2 g-0 ef'-1 c'-3>8\downbow <ef' c'> <ef c'>4 | \time 3/2
  <ef,-2 g-0>4\downbow <d' b'> <f,-2 g-0>
  <gf'-1 ef'-3> <f-1 d'-3> <ef-1 c'-3> | \time 2/2
  c,4 r <d-2 g-0 ef'-1 c'-3>8\downbow <ef' c'> <ef c'>4 | \time 3/2
  <ef,-2 g-0>4\downbow <d'-0 b'-1> <f,-2 g-0>
  <gf'-1 ef'-3> <f d'> <ef c'> | \time 2/2
  c,4 r <d g ef' c'>8\downbow <ef' c'> <ef c'>4 | \time 3/2 \break
  
% bar 265
  <ef,-2 g-0>4\downbow <d' b'> <f,-2 g-0>
  <gf'-1 ef'-3> <f-1 d'-3> <ef-1 c'-3> | \time 2/2
  c,4 r <d g ef' c'>8\downbow <ef' c'> <ef c'>4 | \time 3/2
  <ef,-2 g-0>4\downbow <d'-0 b'-1> <f,-2 g-0>
  <gf'-1 ef'-3> <f d'> <ef c'> | \time 2/2
  r4 <a, fs'>8\downbow <a fs'> <fs' a> <fs a> <g, bf'> <g bf'> |
  c,?4 r <d g ef' c'>8\downbow <ef' c'> <ef c'>4 | \time 3/2

% bar 270
  <ef, g>4\downbow <d' b'> <f, g> <gf' ef'> <f d'> <ef c'> | \time 2/2
  c,4 r <d g ef' c'>8\downbow <ef' c'> <ef c'>4 | \time 3/2
  <ef, g>4\downbow <d' b'> <f, g> <gf' ef'> <f d'> <ef c'> | \time 2/2
  r4 <fs-1 d'-2>8\downbow <fs d'> <g ef'> <g ef'> <gs e'> <gs e'> | \time 3/2
  <c,,-0 c'-1>4 <a''-3 f'-4> <c,, c'> <bf''-3 gf'-4>
  <c,, c'> <g''-1 ef'-2> | \break

% bar 275
  <c,, c'>4 <a'' f'> <c,, c'> <bf'' gf'> <c,, c'> <g'' ef'> |
  r4 <d,-1 g-0>8\downbow <e g> <f g>4 <ef-1 g-0>8
  <f g> <fs g>4 <ef-2 g-0>8 <df g> |
  <c-0 c'-1>4 <a''-3 f'-4> <c,, c'> <bf''-3 gf'-4>
  <c,, c'> <g''-1 ef'-2> |
  <c,, c'>4 <a''-3 f'-4> <c,, c'> <bf''-3 gf'-4>
  <c,, c'> <g'' ef'> | \clef "treble"
  c'4 cs2-1 d ef4 | \time 2/2 \break

% bar 280
  d2 ef4-1( f) | \time 3/2
  gf4->( ef) gf->( ef) g8->-3( gf-3 ff ef) |
  gf4->( ef) gf->( ef) g8->-3( gf-3 ff ef) |
  gf4 ef2-> f! gf4~-2 | \time 2/2
  gf4 f2 gf4 |
  g!2-2 g | \time 3/2 \break

% bar 286
  g2 g4 af g2 | \time 2/2
  g2 g2 | \time 3/2
  g2 g4 af g2 | \time 2/2
  fs2-3 cs-1 | \time 3/2
  e2 f,!4-1 g af? e8-1 fs | \time 2/2
  g2\downbow g | \time 3/2 \break

% bar 292
  g2 g4-2 af g2 | \time 2/2
  g2 g | \time 3/2
  g2 g4 af g2 | \time 2/2
  fs2 cs-1 | \clef "tenor" \time 3/2
  e2.-4 ef4-3 c!-1 g'-4 |
  fs2. df4-3 bf-1 f'-2 | \break

% bar 298
  e2._\markup{\italic "dim."} c4-2 a-0 e'?-4 | \time 2/2
  ef!4\mf ef ef ef |
  ef4 df8 ef e4 c-1 |
  ef!4-3 ef ef ef |
  ef4 df8_\markup{\italic "dim."} ef e4 c-1 |
  ef!4 ef c g'-4 |
  fs1~\p | \pageBreak

% PAGE  SIX

% bar 305
  fs4 ef-4 c g'-4 |
  fs1~ |
  fs4 f ef d-4 |
  c4 b8 c d4 a |
  c4 f-4 ef d |
  c4-2 b8 c d?4 a! |
  c4-1 ef c g'-4 |
  fs1~ |
  fs4 ef-4 c g' | \break

% bar 314
  fs1~ |
  fs4 f ef d-4 |
  c4 b8 c d4 g,-1 |
  bf!4-4 bf a g |
  fs4-3 e8 fs g4 c,-1 |
  ef!4 ef ef ef |
  ef4-2 df8 ef ff4 c-1 | \break

% bar
  ef4-2 ef ef ef |
  ef4 df8 ef ff4 c-1 |
  ef4 df c \clef "bass" bf-4 | \time 3/2
  a2. f4-4 d a' |
  af!2. ff4-3 df-1 af'-2 | \time 2/2
  g2 e | \break

  c2 d! |
  ef1~ |
  ef4 ff c g'-4 |
  ef1~-1 |
  ef4 ff c g'-4 |
  ef4 r r2 | \clef "treble" \time 3/2
  bf'''2-1\ff bf4 cf bf2\upbow\<( |
  ef4-3\!) r4 \clef "bass" <g,,,? ef'>->\downbow r4 r2 | \bar "|."
}


% Local Variables:
% compile-command: "make -B cello.pdf"
% End:
