\version "2.19"
\language "english"

Andante = \relative c' {
  \global
  \key a \major
  \time 4/4
  \tempo "Andante" 4 = 80
  \clef bass
  % Music goes here
  R1*7 |
  r1\fermata |
  \mark \default
  R1*8 | \bar "||"
  \mark \default
  \time 2/2
  R1*36 |
  R1*30 |
  R1*36 |
  \mark #13
  R1*14 |
  << \new Voice = "top" { \stemUp
    \tiny
    \tuplet 3/2 { a4^( fs d } \tuplet 3/2 { a' fs d) } |
    \tuplet 3/2 { a'^( fs d } \tuplet 3/2 { a' fs d) } |
    \time 4/4 \bar "||" \break

    \mark \default
    \tempo \markup \column {{
      \line {Allegro ma non}
      \line {troppo ( \note-by-number #2 #0 #UP = \note-by-number #1 #0 #UP ) }
    }}
%bar 135
    \tuplet 3/2 { a'8^( fs d } \tuplet 3/2 { a' fs d }
      \tuplet 3/2 { a' fs d } \tuplet 3/2 { a' fs d) } |
    \tempo "poco rit"
    \tuplet 3/2 { b'^( f d } \tuplet 3/2 { b' f d }
      \tuplet 3/2 { b' f d } \tuplet 3/2 { b' f d) } |
  }  \new Voice = "bottom" { \normalsize
    e,1\rest |
    e1\rest |
    \time 4/4 \bar "||" \break

    \mark \default
%bar 135
    e\rest | \noBreak
    e\rest | \noBreak
  } >>
  \stemNeutral \clef tenor \tempo 4 = 132
  e''2.\f->^1 e8.\upbow-- e16\upbow-. | \noBreak
  a1-> | \noBreak
  e2.\downbow-> e8.\upbow-- e16\upbow-. | \noBreak
%bar 140
  cs2.-> cs4^2^\markup{\small"II"} | \noBreak
  d2 cs4.( b8) | \noBreak
  a4^4( cs^1) gs'^2( fs) | \break

  b( gs) fs4.( e8) |
  e2 \tuplet 9/8 { r16 e,( fs gs a b cs d^3 ds^4) } |
  \mark \default
  e2.\sfz^1 e8. e16\upbow |
%bar 145
  a2. \tuplet 3/2 { gs8(_\markup{\italic"dim."} a^0 fs^3) } |
  e2~ e8. e16( fs8. cs16^1) |
  cs2.\> cs4\p |
  cs2 d4.( cs8) | \break

%bar 150
  cs4(_\markup{\italic"cresc."} fs^2) gs4.( fs8) |
  e4( gs) b4.^3( a8) |
  \tuplet 3/2 { gs8\>\downbow( a gs } \tuplet 3/2 { fs8\upbow gs fs) }
    e4\! r8. b16-.^4 |
  \mark \default
  \tuplet 3/2 { gs8( a gs } \tuplet 3/2 { fs8\upbow gs fs) }
    e4 r8. \clef bass b16-.^4_\markup{\small"IV"} |
  \tuplet 3/2 { gs8\downbow( a gs } \tuplet 3/2 { fs8\upbow gs fs) }
    e4 r8. b''16\p-.^4 | \break

%bar 155
  \tuplet 3/2 { gs8( a gs } \tuplet 3/2 { fs^2 gs fs) }
    \tuplet 3/2 { e^2( fs e } \tuplet 3/2 { ds e ds) } |
  cs8( b as b) e( fs gs cs) |
  \tuplet 3/2 { e^2( fs e } \tuplet 3/2 { ds\upbow e ds) }
    cs4 r8. gs,16-._4\upbow |
  \tuplet 3/2 { e8( fs e } \tuplet 3/2 { ds\upbow e ds) }
    cs4 r8. gs'''16-. | \break

  \tuplet 3/2 { e8( fs e } \tuplet 3/2 { ds e ds) }
    \tuplet 3/2 { cs8( ds cs } \tuplet 3/2 { b cs b) } |
%bar 160
  as8^1( gs' fs e) ds^4( cs \clef treble cs'8.^3 b16) |
  \mark \default
  as2 gs4_2( fs) |
  fs2 b4.( fs8_2) |
  fs2~ fs8 e_4( ds cs) | \break

  gs'4_2( fs) r fs |
%bar 165
  as4._2( b16 as gs4\upbow) fs_\thumb\downbow( |
  b2) gs4 -._4( e-.) |
  cs2 e4.( ds8_4) |
  cs4( b) r \clef bass b^1_\markup{\small"II"} |
  ds2^3( gs,4^2) gs16\upbow( fss gs as |
%bar 170
  gs2\downbow fss4) ds^1_\markup{\small"III"}( | \break

  ds'4.^3_\markup{\small"II"}) b8^4\downbow( gs4) gs16^2( fss gs as |
  gs2\downbow fss4) ds8.( ds'16^4_\markup{\small"I"}) |
  \mark \default
  ds2-> cs4( b) |
  as4( gs8.) gs'16^4-.\downbow gs4.->\upbow( fs16 e) |
%bar 175
  ds4( cs as\upbow ds) |
  cs2( b8) r b8.( cs16) | \break

  ds2 cs16( b cs ds cs8 b) |
  as8.^2_\markup{\italic"cresc."}( gs16) \tuplet 3/2 { gs8( cs^4 e^1) }
    gs4( fs8.\upbow e16) |
  fs2_\markup{\dynamic"p" \italic"sub."}( e4\upbow d!8 cs ) |
%bar 180
  cs2. b8.( e16) |
  d2( cs8) r r8. a,16-.\downbow | \break

  \mark \default
  gs16^1( as bs^1 cs ds^1_\markup{\small"III"} e fs^1 gs a-.) r
    \afterGrace gs4.->\downbow\trill( { fss16 gs) } |
  cs,16^2\upbow( ds e^1 fs gs a^0 b cs ds) r
    \afterGrace cs4.->\trill( { bs16 cs) } | \clef treble
  gs16^4( as^1_\markup{\small"I"} bs^2 cs^3 ds^1 e fs^1 gs a-.) r
    \afterGrace gs4.->\trill( { fss16 gs) } | \pageBreak

%page break

%bar 185
  cs,16_2( ds e_1 fs gs_1 a b_1 cs ds) r
    \afterGrace cs4.->\trill\downbow( { bs16 cs } |
  ds16) r \afterGrace cs4.->\trill\p( { bs16 cs } ds) r
    \afterGrace cs4.->\trill( { bs16 cs } |
  ds16_\markup{\italic"cresc."}) r \afterGrace cs4.->\trill( { bs16 cs } ds) r
    \afterGrace cs4.->\trill( { bs16 cs) } | \break

  cs8.\upbow b!16( cs8.) b16( cs8.) b16( cs8.) b16( |
  cs4)^\markup{\italic"rit."} b( gs fs8.\upbow e16) |
%bar 190
  \mark \default
  \tempo "a tempo"
  e2_\markup{\dynamic f \italic"con espress."}~ e4\upbow~ e8.\downbow e16\downbow |
  a1->\upbow |
  e2.~ e8 e-. |
  cs2.->\>~ cs4-.\upbow |
  d2\! cs4--( b--) | \break

%bar 195
  a4( cs\< gs'_2\upbow fs) |
  b^3\!( gs\>) fs4._3( e8) |
  e2\! \clef bass \tuplet 9/8 { r16 e,( fs gs a b cs d^3 ds^4) } |
  \mark \default
  e2->~ e4.\upbow e8-.\upbow |
  a1-> |
%bar 200
  g2->~ g4. g8\upbow |
  e2. e4 | \break

  \key c \major
  f!8 c16( b c^1 df c af f8\downbow) c16( b c_1 df c af_4) |
  \tuplet 3/2 { f8_1( af) c_1( } \tuplet 3/2 { d) f^1( af) }
    \tuplet 3/2 { c8^1( d) f^1( } \tuplet 3/2 { af) g\upbow( f }
  f8\downbow) c16^2( b c^1 df c af f8\downbow) c16\upbow( b c_1 df c af_4) | \break

%bar 205
  \tuplet 3/2 { f8_1-. af-. b_3-. } \tuplet 3/2 { c-. e^1-. f-. }
    \tuplet 3/2 { af^4-. b^1-. c-. } \clef treble \tuplet 3/2 { e_1-. f-. af_1-. }
  \mark \default
  c2->^3~ c4.\upbow c8-.\upbow |
  g2.-> g4-. |
  a4\downbow( g e_4\upbow c) | \clef bass
  a8^2( g e^4 c) a16_4( g a g f_4 e d c) | \break

%bar 210
  d4. e8( f16 g f e) d\downbow( f a c |
  b4.\upbow) c8_1( d16 e d c) b^1( d f^1 a |
  g4.\downbow) a8^1_\markup{\italic"dim."}( b4.) c8( |
  d4.) e8( f4.) e8-.\upbow | \bar "||"
  \key a \major
  \mark \default
  e2\pp~ e4.\upbow e8-.\upbow |
%bar 215
  a2.\<~ a4--\!\upbow | \break

  g2\>~ g4.\upbow g8-.\upbow |
  e2.\! e4 |
  e8-. b16^2( as b^1 cs b gs e8)\downbow b16_2( as b_1 cs b gs) |
  \tuplet 3/2 { e8_1( gs) b_4_\markup{\small"IV"}( } \tuplet 3/2 { gs) b( e_2) }
    \tuplet 3/2 { b8^2^\markup{\small"III"}( e^1^\markup{\small"II"}) gs( }
    \tuplet 3/2 { e) gs^1 b( } | \break

%bar 220
  e8) b16^2( as b^1 cs b gs e8)\downbow b16_2\( as b_1 cs b gs_4 |
  e_1\)( gs_4\() cs( b) gs_1( b\)) fs'_4\(( e) b^2( e) a^4\)( gs\() e( gs) cs (b\)) |
  \mark \default
  as4( gs'^4) fs4.( e8) | \break

  \tuplet 3/2 { ds8-.\downbow( fs-.\upbow ds-. } \tuplet 3/2 { b-. fs-.^1 ds-.^4) }
    b4-- r8. b'16^2^\markup{\small"II"} |
  as4( gs'^\markup{\small"I"}) fs4.( e8) |
%bar 225
  ds4( b'4.^3) gs8( \tuplet 6/4 {fs16^4 ds b fs^1 ds^4 b^1) } |
  as4_1( gs'^3^\markup{\small"III"}\< fs\upbow e ) | \break

%bar 227
  ds4\!( as b\>\upbow cs\downbow) |
  b4\upbow( b'^\markup{\small"II"} as\p\downbow gs) |
  fs4\upbow\<( es ds'4. cs8\!) | \clef tenor
  \mark \default
  \omit TupletNumber
%bar 230
  \tuplet 3/2 { cs-._\markup{\italic"spiccato"} ds-. cs-. } \tuplet 3/2 { fs-.^1 gs-. fs-. }
    \tuplet 3/2 { cs-.^2_\markup{\small"II"} ds-. cs-. }
    \tuplet 3/2 { fs-._\markup{\small"I"} gs-. fs-. } | \break

  \tuplet 3/2 { e-.^1 fs-. e-. } \tuplet 3/2 { b'-.^1 cs-. b-. }
    \tuplet 3/2 { e,-.^1^\markup{\small"II"} fs-. e-. } \tuplet 3/2 { b'-.^1 cs-. b-. } |
  \tuplet 3/2 { bf^3( bf,\thumb) bf-. } \tuplet 3/2 { f'^3( f,\thumb) f-. } \clef bass
    \tuplet 3/2 { d'^3_\markup{\small"II"}( d,\thumb_\markup{\small"III"}) d-. }
    \tuplet 3/2 { bf'^3( bf,\thumb) bf-. } |
  b'8-.^3 as16^2( gs^1 fs\thumb e^3 ds cs b8\thumb) r r4 | \pageBreak

%page break

  fs'8-.\f gs16( fs) b8-. cs16( b) fs8-. gs16( fs) b8-. cs16( b) |
%bar 235
  a8-. b16( a) e'8-. fs16( e) a,8-. b16( a) e'8-. fs16( e) | \clef treble
  \undo \omit TupletNumber
  \tuplet 3/2 { ds8_\thumb( as'_\thumb) as-. } \tuplet 3/2 { ds,8( cs') cs-. }
    \tuplet 3/2 { ds,8( as') as-. } \tuplet 3/2 { ds,8( ds') ds-. } | \break

  \tuplet 3/2 { e,8_\markup{\italic"dim."}^\thumb( b') b-. } \tuplet 3/2 { e,8( cs') cs-. }
    \tuplet 3/2 { e,8( b') b-. } \tuplet 3/2 { e,8( fs') fs-. } |
  \mark \default
  \omit TupletNumber
  \tuplet 3/2 { g,8^\thumb( d') d-. } \tuplet 3/2 { g,8( e') e-. }
    \tuplet 3/2 { g,8( d') d-. } \tuplet 3/2 { g,8( g') g-. } |
  \tuplet 3/2 { g,8_\thumb( c) c-. } \tuplet 3/2 { g8( d') d-. }
    \tuplet 3/2 { g,8( cs) cs-. } \tuplet 3/2 { g8( g') g-. } | \break

%bar 240
  \tuplet 3/2 { fs,8_\thumb( cs'!) cs-. } \tuplet 3/2 { fs,8( ds') ds-. }
    \tuplet 3/2 { fs,8( cs') cs-. } \tuplet 3/2 { fs,8( fs') fs-. } |
  \tuplet 3/2 { fs,8_\thumb( b) b-. } \tuplet 3/2 { fs8( cs') cs-. }
    \tuplet 3/2 { fs,8( b) b-. } \tuplet 3/2 { fs8( fs') fs-. } |
  \tuplet 3/2 { f8^3( e d) } \tuplet 3/2 { d( c^\thumb b^3^\markup{\small"II"}) }
    \tuplet 3/2 { b( a gs!) } gs8[ r16 gs] | \break

  f'16^2( g f e) d^2( e d c) b_\thumb( c b a) gs!8-. r |
  \tuplet 3/2 { g'8^3( f e) } \tuplet 3/2 { e( d^\thumb c^3^\markup{\small"II"}) }
    \tuplet 3/2 { c( b a) } a8[ r16 a] |
%bar 245
  g'16^2( a g f) e^2( f e d) c^2( d c b) a8[ r16 a] | \break

  \mark \default
  c2_\markup{\italic"con espress."} g4_4( f) |
  e4_4( d) c4.( g'8_4) |
  g2~ g8 f( e f) |
  f2( g4\upbow a) |
%bar 250
  bf2( a4.\upbow g8) |
  g4( f) e( f8 g) |
  a2 g4.( f8) | \break

  f4( e) d( e8 f) |
  g2 f4.( e8) |
%bar 255
  e4( d) cs( d8 e) |
  \mark \default
  d2.\downbow r8 d |
  f2.-> f8. f16 |
  b,2.-> b4( |
  d2.) d8. d16\upbow | \break

%bar 260
  gs!2.->_\markup{\italic"più"\dynamic"f"} gs4\upbow( |
  b2.\downbow) b8. b16\upbow |
  es,2._1_\markup{\italic"cresc."} es4\upbow( |
  gs2.\<) gs8.\downbow gs16\upbow |
  d'2.->^3\f bf4-> |
%bar 265
  f4-> d-> \clef bass bf-> f-> |
  d2->( c4.\upbow bf8) |
  g2._\markup{\dynamic"fpp"}\<( es4--^\markup{\italic"poco"}) | \break

  fs1\upbow\>^\markup{\italic"a poco rit."} |
  b1\pp~ |
%bar 270
  b2 b'4.^2^\markup{\small"II"}\<~ b8-.\! |
  \mark #30
  \undo \omit TupletNumber
  \tempo "a tempo, tranquillo"
  b2._\markup{\italic"dolce"}( cs4 |
  b4 e,8^1\upbow fs^2 a4.^4 gs8) |
  fs2~ fs8 g( \tuplet 3/2 { b8 a gs) } |
  fs2 r4 fs8._\markup{\italic"sempre"\dynamic"pp"}( b16) |
%bar 275
  b2.( cs4\upbow | \break

  b4 e,8\downbow fs \grace{fs16^1\upbow gs} a4. gs8) |
  fs2\downbow~ fs8 gs( b16 a gs a) |
  fs2 r4 b8.--_\markup{\small"I"}\upbow~ b16-. |
  \mark \default \omit TupletNumber
  b2~ \tuplet 3/2 { b8 e^2( ds } \tuplet 3/2 { cs^4 b as) } |
%bar 280
  b2~ \tuplet 3/2 { b8-. e-.( ds-. } \tuplet 3/2 { cs-. b-. as-.) } | \break

  b8( \clef treble b'4^2) cs16( b as4.) b16( as |
  a!4.) b16( a gs4.) a16\upbow( gs) |
  fs16_2\downbow( gs) gs( fs) e( fs) fs( e) ds_3( e) e( ds) cs( ds) ds( cs) |
  \undo \omit TupletNumber \tupletSpan 4 \clef bass \break

  \tuplet 3/2 { b8^2( fs ds b e^1 gs) fs( b cs b ds^1 e) } |
%bar 285
  fs16_2\downbow( gs) gs-.( fs-.) e( fs) fs-.( e-.)
    ds_3( e) e-.( ds-.) cs( ds) ds-.( cs-.) |
  \tuplet 3/2 { b8^2( fs ds b e^1 gs) fs( b ds b ds^1 fs) } | \break \clef tenor

  g16^2( a) a( g) f( g) g( f) e^3( f) f( e) d( e) e( d) |
  \tuplet 3/2 { c8_2( g e g_2 f_1 a_4) g( c^1 d c e^1 f) }
  g16^2( a) a-.( g-.) f( g) g-.( f-.) e^3( f) f-.( e-.) d( e) e-.( d-.) | \pageBreak

%page break

%bar 290
  \tuplet 3/2 { c8_2( g e g_2 f_1 a_4) g( c^1 e c e^1 g^4) } | \clef treble
  \mark \default
  b2.^2_\markup{\italic"con espress."}( cs4\upbow) |
  b4\downbow( e,8_1 fs) a4.( gs8) |
  fs2~ fs8 gs( \tuplet 3/2 { b8 a gs) } | \break

  fs2 r4 fs8.( b16) |
%bar 295
  b2.( cs4\upbow) |
  b4\downbow( e,8--\upbow fs--\downbow) a4.\upbow( gs8) |
  fs2~ fs8 gs( b16 a gs a) |
  fs2 r4 fs |
  b2^1~ \tuplet 3/2 { b8 e^3( ds cs^3 b as) } | \break

%bar 300
  \tupletSpan \default
  b2~ \tuplet 6/4 { b8[ e-.( ds-. cs-. b-.^3 bf-.)] } |
  a2~ \tuplet 6/4 { a8[ d( cs b a gs)] } |
  a2~ \tuplet 6/4 { a8[ d-.( cs-. b-. a-.^3 af-.)] } |
  g2~ \tuplet 6/4 { g8[ c( b a g_2 fs)] } |
  g2~ \tuplet 6/4 { g8[ c-.( b-. a-. g-._2 fs-.)] } | \break

%bar 305
  \mark \default
  g2_1\p c4.^3( b8-._\markup{\italic"poco a poco cresc."}) |
  a2. d8^3( c) |
  b2 e4.^3( d8-.) |
  c2. f8^3( e) |
  \tupletSpan 4
  \tuplet 3/2 { ds8-.\fp^\markup{\italic"spiccato"} fs-. ds-.
  cs-.^2_\markup{\italic"dim."} ds-. cs-.  b-. cs-. b-. fs-._2 gs-. fs-. } |
%bar 310
  \tuplet 3/2 { ds_1-. fs-. ds-. cs_2-. ds-. cs-.
    \clef bass b-. cs-. b-. fs-. gs-. fs-. } | \break

  ds4.^4( css16 bs) ds'4.^4( css16 bs) |
  g4.^3^\markup{\small"II"}( fs16 e) \clef treble
    g'4._4^\markup{\small"I"}( fs16 e) |
  \tuplet 3/2 { d8-._1 e-. d-. g-._1 a-. g-.
    d-._2^\markup{\small"II"} e-. d-. g-. a-. g-. } |
  \tuplet 3/2 { e-._1^\markup{\small"II"} fs-. e-.
    a-._\thumb^\markup{\small"I"} b-. a-.  e-. fs-. e-. a-. b-. a-. } | \break

%bar 315
  \tuplet 3/2 { fs8-. g-. fs-. b-. c-. b-. fs-. g-. fs-. b-. c-. b-. } |
  \tuplet 3/2 { g_2-. a-. g-. c^1-. d-. c-. g-. a-. g-. } c16( d c b^2) |
  \mark \default
  as8. as16( e'2^3) cs8\downbow( as\upbow) |
  b2( gs4_4\upbow e) |
  cs2( fs4.\upbow e8) | \break

%bar 320
  ds8_4-. cs-. b2 cs8( ds) | \clef bass \noBreak
  e8( ds cs b a\upbow gs fs e) | \noBreak
  fs2( a4^1\upbow c) | \noBreak
  b8( a gs^4 fs e\upbow ds_4 cs b) | \noBreak
  cs2( e4_1_\markup{\small"III"}\upbow g) | \noBreak
%bar 325
  fs8( e\< ds^4 cs b\upbow as^4 gs fs) | \break

  \tuplet 3/2 { b8_3\!( as gs fs_\thumb gs as) b( as gs fs gs as) } |
  \omit TupletNumber
  \tuplet 3/2 { b( as gs fs gs as) b( as b_1_\markup{\small"IV"} ds_3 cs b) } |
  \tuplet 3/2 { c_0( b_4 a g a b) c_0( b a g a b) } | \break

  \stemUp
  \tuplet 3/2 { c( b a g_0^\markup{\small"III"} a b) c( b c_1 e d c) } |
  \mark \default \undo \omit TupletNumber
%bar 330
  d8.-> a16 a4-> \tuplet 3/2 { r8 a_3^\upbow( b c d_1 e) } | \stemNeutral
  f8.-> d16 d4-> \tuplet 3/2 { r8 d^0( e f^1 g gs) } |
  a8.^4 f16^1( d'8.^2) a16^0( \clef treble  f'8._4) d16_1( a'8._\thumb) f16_2( | \break

  d'2\<) c4.( b8) |
  b2\>( e4_2\upbow d) |
%bar 335
  f2.\downbow_\markup{\dynamic"sfpp"}\!~ f8. a,,16\downbow( |
  bf2.) \tuplet 6/4 { b16\upbow( c df_1 ef f_1 fs) } |
  gs2.( gs8.) \clef bass bs,16\downbow( |
  cs2) ds4.( as8^1_\markup{\small"II"}) | \break

  b4.-> gs16^2\upbow( ds^4 b^1 d gs b^1_\markup{\small"II"})
    \tuplet 3/2 { ds8^3( cs--\downbow b--\upbow) } |
%bar 340
  cs4.-> gs16^4\upbow( e cs e gs^3 cs^1)
    \tuplet 3/2 { e8^3( ds--\downbow cs--\upbow) } |
  ds4.-> bs16^1\upbow( a fs a b^1 ds^4)
    \tuplet 3/2 { fs8^4( e--\downbow ds--\upbow) } | \clef treble
  e2( fs4\upbow fss) | \break

  \mark \default
  gs2~ gs8 fs-.( es-. fs-.) |
  gs4( fs) ds( b) |
%bar 345
  gs'2~ gs8 fs-.( es-. fs-.) |
  gs8_2\<( fs b_3 fs) ds( cs b8. fs'16) |
  fs2->\p e4.( ds8 |
  cs4) b2-> b4( | \pageBreak

%page break

  c4) e( a4.) g8~ |
%bar 350
  g8 g4->\< g-> g-> a16^1( b) |
  c8_3\!( b a g) f( e d_2 c) |
  d2( f4_1\upbow af) |
  g8_4( f e d) \clef bass c( b a g) | \break

  a2( c4\upbow ef) |
%bar 355
  d8( c b a) g( f e d) |
  e2( g4\upbow bf) |
  \mark \default
  a2~ a4.\upbow a8-.\upbow |
  d2. d8.--\upbow~ d16-. | \break

  a2~ a4.\upbow a8-.\upbow |
%bar 360
  fs2. fs8.( fs'16) | \clef treble
  fs2~ fs4.\upbow fs8_1\upbow | \break

  b2.->^3 b8.--~ b16-. |
  fs2~ fs4.\upbow fs8_4\upbow |
  ds2. ds8.( ds'16_3) | \break

%bar 365
  ds2\f cs4.( bs8) |
  bs4( a!4._3) gs8( fss16 gs b_1 e_2) |
  e2-> ds4.( cs8) |
  cs4( b4.^3\<) as8( gss16 as cs^1 es^2) |
  f2^3\ff e4.( d!8^3) | \break

%bar 370
  d2 c4.( b8^3) |
  b2_\markup{\italic"dim."} a4.( gs8_3) |
  gs2^\markup{\italic"rit."} g4.( fs8_3) |
  \mark \default \tempo"a tempo"
  fs2\p~ fs8 e-.( ds-. e-.) |
  fs4( e) cs( a) |
%bar 375
  fs'2~ fs8 e-.( ds-. e-.) | \break

  fs4( e) a4._3( cs,8_3) |
  cs2~ cs8 bs-._2( as-._1 bs-.) |
  ds4_4( cs) bs( cs) |
  e2_4~ e8 ds-.( cs-. ds-.) |
%bar 380
  fs4_4_\markup{\italic"poco a poco cresc."}( e) ds( e) |
  gs2_4~ gs8 fss-._3( es-._1 fss-.) | \break

  as4_3\f( gs) fss( gs_1) |
  \mark \default
  b2\p( gs4\upbow e) |
  cs2_\markup{\italic"dim."} fs4.( e8) |
%bar 385
  ds8-.\pp\upbow( cs-.\downbow) b2 cs8( ds) |
  e4--( ds--_4) \clef bass cs--( bs--) | \pageBreak

%page break

  cs4.\p( a16 e cs e a cs^\markup{\italic"string."}) \tuplet 3/2 { e8( d cs) } |
  d4.( a16 fs d fs a d) \tuplet 3/2 { fs8( e d) } |
  e4.( cs16^3 as^1 g^4 a cs e^1) \tuplet 3/2 { g8( fs e) } |
%bar 390
  f2\< g4-> gs->\! | \break \clef treble

  \mark \default
  \tempo "Meno (Tempo I.)"
  a2_3\downbow\fp~ a8 g-.( fs-. g-.) |
  a4( g) e_4( c) |
  a'2_3~ a8 g-.( fs g) |
  a4_2\<( g) c( e,_1) |
%bar 395
  e2\pp~ e8 ds-._3_\markup{\italic"poco a poco cresc."}( cs-. ds-.) |
  fs4( e) a4._3( gs8_3) |

  gs2~ gs8 fss-.( es-. fss--) |
  as4_3( gs) cs4.^3( b8) |
  b2~ b8 as-.( gs-._\thumb as-.) |
%bar 400
  cs4\<( b) e4.^2( ds8\!) |
  \acciaccatura fs8 e2\fp cs4( a) |
  fs2_1_\markup{\italic"dim."} b4.^3( a8) | \break


  gs8_4-. fs-. e2 fs8_1( gs) |
  a4->\<^\markup{\italic"rit."}( gs->) fs->\>( es->) | \clef bass
  \mark \default
%bar 405
  fs2.\pp fs8.--~ fs16-. |
  cs2.\< cs4^2->\>^\markup{\small"II"}
  d2.^3-> d4\pp |
  cs->( bs-> a->\upbow gs->) |
  fs8-.( d-.) a'^4( a,-.) fs'-.( d-.) a'-.( a,-.) | \break

%bar 410
  \tuplet 3/2 { fs'8( d d-.) a'^4( a,^1) a-. fs'8( d) d-. a'( a,) a-. } |
  fs'8^1-.( d^0-.) a'^4( b,^1-.) fs'-.( d-.) a'-.( b,-.) |
  \tuplet 3/2 { fs'8( d d-.) a'( b,) b-. fs'8( d) d-. a'( b,) b-. } | \break

  fs8. e32( d a'8.) gs32( fs c'8.) b32( a ef'8.) d32( c | \clef treble
  fs'8.) e32( d a'8.) gs32( fs c'8.) b32( a ef'8.) d32( c |
%bar 415
  fs4^3->\ff) f16( e ef^3 d) cs4-> c16^3( b bf a^0) | \break

  gs4->_4 g16_4( fs f_\markup{\italic"dim."} e) ds4->_1 d16_4( cs c b) |
  \clef bass \repeat tremolo 4 { b16^2\p^\markup{\italic"rit."}( as_\markup{\italic"dim."} }
    \repeat tremolo 4 { b as) } |
  \repeat tremolo 4 { b16^3( a }
    \repeat tremolo 4 { b a )} |
  \mark \default \tempo "Meno allegro"
  gs4\pp r r8 cs4--\upbow\<( cs8-.) |
%bar 420
  cs2\>( bs8\!) r8 r4 | \break \clef treble

  r2 r8 fs'4--\<\upbow^\markup{\small"I"}~ fs8-. |
  fs2\>( es8)\! r r4 |
  r2^\markup{\italic"ad libitum"} r8 fs4--\downbow~ fs8-.\upbow |
  r2 r8 fs4--\downbow~ fs8-.\upbow |
%bar 425
  r2 r8 fs4--\downbow~ fs8-.\upbow |
  fs2\downbow~ fs8 e( d_4 b) | \break

  b'4\fermata~  b32[ gs\(( fs gs] b gs fs gs b[ gs fs gs]
    b[ gs fs gs]\) b[\() gs_2( fs gs] a_0) fs_2( e fs  | \clef bass \noBreak
  gs[) e^2( d e] fs[) d^2( cs d] e[) cs^2( b cs] d^3[) b^1( a b]
    cs[) a^2( gs a] b[) gs^2( fs gs] a[) fs^2( e fs] gs[) e^1( d e]\)) | \break
  fs16^1( d^4 cs b) e( b_4 a gs) b_1( gs_4 fs e) e8-> ds_2-> |
%bar 430
  \tempo\markup\small {"a tempo"}
  \afterGrace ds1\trill\<( { cs16 ds\!) } | \bar "||"
  \mark \default \time 2/2 \tempo "Allegro assai"
  e4 r r2 |
  R1*21 | \pageBreak
  \key c \major \time 4/4

%page break

  \mark \default \tempo "Allegro ma non troppo"
  f''2.\mf f8.--~ f16-. |
  f4( a,) a--\upbow~ a--\downbow |
%bar 455
  bf4.\upbow( c8) \tuplet 6/4 { d16\downbow( c bf g f d) } bf8.( bf'16^1) |
  bf8 r r4 r2 | \break

  af'2.->\fp af8.--~ af16-. |
  af4( c,) c--\upbow~ c--\downbow |
  df4.\upbow( ef8) \tuplet 6/4 { f16^4( ef df bf^4 af f^1) } df8.^4( df'16^2) |
%bar 460
  df8 r r4 r2 | \bar "||" \clef tenor \key a \major

  \mark \default
  b'2. b8.--~ b16-. |
  b4( ds,) ds-.~ ds-. |
  e4.^1( fs8) \tuplet 6/4 { gs16( fs e cs^4 b gs^4) } e8.( e'16^4) |
  b4 r r2 | \break

%bar 465
  b'2.-> b8.^1( bs16) |
  cs4( ds,) ds-> ds-> |
  e4.( fs8) \tuplet 6/4 { gs16( fs e cs^4 b gs) } e8.( e'16) |
  d4. r8 r2 | \break

  \mark \default
  b'2._\markup{\dynamic"p"\italic"cresc. poco a poco"} b8.--~ b16-. |
%bar 470
  b4( d,^\markup{\small"II"}) d-> d-> |
  b'2.-> b8.--~ b16-. |
  b4( cs,^\markup{\small"I"}) cs-> cs-> | \break

  cs4.^2( a8^0) fs16\(( a cs fs \tuplet 3/2 { a8) gs--\downbow fs--\upbow\) } |
  fs4( cs') a( gs8.) fs16\upbow |
%bar 475
  cs4.( a8) fs16\(( a cs fs \tuplet 3/2 { a8) gs-- fs--\) } |
  fs4( cs') a( gs8.) fs16\upbow | \break

  \mark \default \undo \omit TupletNumber
  fs2.\p e8( d |
  g4.) a,8( bf16 g e' d) cs( e gs^1 bf) |
  a4(~ a16 g\upbow e cs) \clef bass a4(~ a16 g\upbow e^4 cs) |
%bar 480
  a4_4~ \tuplet 3/2 { a8 cs_1-.\upbow e-.\upbow
    g^2-.a-. cs^1-. e^4-. g^1-. a-. } | \pageBreak

%page break

  b4(~ b16 a\upbow fs^4 d) b4(~ b16 a^4\upbow fs d) |
  a4~ \tuplet 3/2 { a8 d-.\upbow fs-.\upbow
    a^0-. d^1-. e-. \clef treble fs-. a-._\thumb b-. } |
  cs2.-> d8^2( e) | \break

  e4_\markup{\dynamic"fsfz"}( d8) r16 cs-. \tuplet 3/2
    { b8_1( fs_2_\markup{\small"II"} e) } d16_0( cs_4 b16._2 fs'32) |
%bar 485
  cs2.->-1^\markup{\small"I"}_\markup{\italic"sub."\dynamic"p"} d8( e)  |
  e4( d8) r r4 r8. d16 |
  e2.\pp d8( c | \break

  f4.) \clef bass g,8^3( af16 f d'^4 c) b( c \clef tenor af'^3 g) |
  \mark \default
  fs2.^3 e8^2( d |
%bar 490
  g4.^4) a,8^3( bf16 g e'^4 d) cs( d bf'^3 a) |
  gs2.-> fs8->^2( e->) | \break

  a2^3 f4( d^4) | b2 e4.^4( d8) |
  c8->-.\downbow( b->--\upbow) a2 c'8->\upbow( b->\upbow) |
%bar 495
  a8-. e16^2( ds e f e^4 c a8\downbow) \clef bass e16^2( ds e^1 f e c | \break

  \tuplet 3/2 { a8-.\downbow) c-. e-. c-. e-. a-. e-. a-. c-. a-. c-.^1 e-. } |
  a8-.^0 e16^2( ds e^1 f e^4 c a8) e16^2( ds e^1 f e c) | \break

  a16( c) f-. e-. c( e) b'-. a-. e( a) d-. c-. a( c) f-.^3 e-. |
  \clef tenor \mark \default
  ds4( cs'!^3) b4.( a8) |
%bar 500
  \tuplet 3/2 { gs8-.\downbow( b-.\upbow gs-.^2 e-. b-. gs-. } e4\downbow) r8. e'16^2 | \break

  ds4( cs') b4.( a8) |
  gs4--\thumb e'4.->^3( cs8) \tuplet 6/4 { b16\thumb( gs^2\thumb_\markup{\small"II"}
    e b^1_\markup{\small"III"} gs^3 e^1_\markup{\small"IV"}) } |
  es4_1_\markup{\italic"con espress."}( d'^4 cs\upbow b^2) |
  a4\downbow( es^1_\markup{\italic"cresc."} fs\upbow gs) |
%bar 505
  fs\downbow( fs'^4\< es\upbow d) | \break

  cs\!^2\downbow( bs a'4.^4\f\downbow fs8) | \clef treble
  \tuplet 3/2 { fs8-._\markup{\dynamic"p"\italic"sub."} gs-. fs-. b-.^1 cs-. b-.
    fs-._2^\markup{\small"II"} gs-. fs-. b-. cs-. b-. } |
  \omit TupletNumber
  \tuplet 3/2 { a-._\thumb b-. a-. e'-.^2 fs-. e-.
    a,-._\thumb b-. a-. e'-. fs-. e-. } | \break

  \tuplet 3/2 { ef8_3( ef,_\thumb) ef-. bf'_3( bf,_\thumb) bf-.
    g'( g,_\thumb) g-. \clef bass ef'_3( ef,_\thumb) ef-. } | \stemUp
%bar 510
  e8_1 ds16_4--\upbow cs-- b-- a_4-- gs_4-- fs-- e8_4\upbow r r4 | \pageBreak

%page break

  \undo \omit TupletNumber \stemNeutral \clef treble
  b''8--_2_\markup{\small"II"} cs16( b) e8-._1_\markup{\small"I"} fs16( e)
    b8-. cs16( b) e8-. fs16( e) |
  d8-._\thumb^\markup{\small"II"} e16( d) a'8-._\thumb^\markup{\small"I"} b16( a)
    d,8-. e16( d) a'8-. b16( a) |
  \tuplet 3/2 { gs8_2^\markup{\small"II"}( b gs) fs_1( es' gs,) }
    fs16( ds' fs, gs \tuplet 3/2 { fs8 a gs_3) } | \break

  \tuplet 3/2 { fs8( gs fs) e( cs' fs,) } e16( cs' e, fs \tuplet 3/2 { e8 fs e) } |
%bar 515
  \mark \default
  \tuplet 3/2 { c8_\thumb( g'_\thumb) g-. \omit TupletNumber
    c,( a') a-. c,( g') g-. c,( c') c-. } |
  \tuplet 3/2 { c,( f) f-. c( g'_\thumb) g-. c,( f) f-. c( c') c-. } | \break

  \tuplet 3/2 { b,8_\thumb( fs'_\thumb) fs-. b,( gs') gs-. b,( fs') fs-. b,( b') b-. } |
  \tuplet 3/2 { b,8_\thumb( e_3) e-. b( gs') gs-. b,( e) e-. b( b') b-. } |
  \tuplet 3/2 { bf8->_3_\markup{\italic"con molta forza"}( a_3 g)
    g->( f_\thumb e_3^\markup{\small"II"}) e->( d cs) } cs8-.[ r16 cs-.] | \break

  \undo \omit TupletNumber
%bar 520
  bf'16_2\fz( c bf a) g_2\fz( a g f) e_2\fz( f e d) cs8_1 r |
  \tuplet 3/2 { c'8->^3( bf a) a->( g_\thumb f_3^\markup{\small"II"})
    f->( e d) } d8-.[ r16 d-._\thumb] |
  c'16->^3( d c bf) a->_2( bf a_0 g_4) f->_2( g f e_2) d8-._1[ r16 d-.] | \break

  \clef bass
  f2->_\markup{\dynamic"p"\italic"dolce"}( c4\upbow bf) |
  a^4^\markup{\italic"poco rit."}( g f4.\upbow c'8^4) |
%bar 525
  c2~ c8 bf( a bf) |
  bf2( c4^1\upbow_\markup{\small"II"} d) |
  \mark \default
  ef2^3 e4.^3( c8) | \break

  c4^4( bf) a( bf8^1 c |
  d2\downbow) c4.( bf8^4) |
%bar 530
  bf4( a) f( a8 bf |
  c2\downbow) bf4.( a8) |
  a4^4( g fs\upbow g8 a) | \break

  g2 r4 r8. g16^1 |
  bf2.-> bf8.-- bf16-.\upbow |
%bar 535
  e,2.->^1 e4 |
  g2.->_\markup{\italic"poco a poco cresc."} g8.--^2 g16-.\upbow |
  cs?2.-> cs4-. |
  e2.-> e8.-- e16\upbow |
  as,2.^1 as4( | \break

%bar 540
  cs2.\<) cs8. cs16\upbow |
\mark \default
  g'2.->\ff ef4->^2 |
  bf^4-> g-> ef-> b-> |
  g2->_4^\markup{\italic"meno"} f4.( ef8) |
  c'2.\>^\markup{\small"IV"}^\markup{\italic"rit."}( b4) |
%bar 545
  b1\pp\upbow |
  e^\markup{\small"III"}~ |
  e2 \clef tenor e'4.^1_\markup{\dynamic"mp"\italic"dolce"}~ e8-. | \break

  \mark \default \tempo "Un poco meno allegro"
  e2.\( fs4 |
  e a,8^0\upbow b^1 e4. cs8\) |
%bar 550
  b2~ b8 cs^1( \tuplet 3/2 { e8 d cs^2) } |
  b2^1 r4 b8.( e16) |
  e2.\( fs4\upbow |
  e a,8\downbow b \grace {b16\upbow cs} d4. cs8\) | \pageBreak

%page break

  b2~ b8 cs^1( e16 d cs d) |
%bar 555
  b2 r4 e8.--\upbow~ e16-. |
  e2~ \tuplet 3/2 { e8 a^3( gs fs^4 e ds) } |
  \tupletSpan \default
  e2~\( \tuplet 6/4 { e8[ a-.\)( gs-.^4\upbow\( fs-. e-. ds-.^1]\)) } | \break

  \tupletSpan 4 \clef treble
  e8\downbow( e'4^2) fs16\upbow( e ds4.^2) e16\downbow( ds |
  d4.^2) e16\upbow( d cs4.^2) d16\upbow( cs) |
%bar 560
  \mark \default
  b16( cs) cs( b) a^2( b) b( a) gs( a) gs( a) fs^2( gs) fs( gs) | \break

  \clef tenor
  \tuplet 3/2 { e8( b^1 gs e a_0 cs) b( e^1 fs e gs^1 a) } | \clef treble
  b16( cs) cs( b) a^2( b) b( a) gs( a) gs( a) fs^2( gs) fs( gs) | \break

%bar 565
  \clef tenor
  \tuplet 3/2 { e8( b^1 gs e a cs) b( e^1 fs e gs^1 a) } | \clef treble
  c16( d) d( cs) bf( c) c( bf) a( bf) bf( a) g( a) a( g) | \break

  \clef tenor
  \tuplet 3/2 { f8^2( c^4 a f^2 bf^1 d) c( f g f a^\thumb bf) } | \clef treble
  c16( d) d( cs) bf( c) c( bf) a( bf) bf( a) g( a) a( g) | \break

  \clef tenor
  \tuplet 3/2 { f8( c^4 a f bf d) c( f g f a^\thumb c) } | \clef treble
  \mark \default
  e2.->^2_\markup{\italic"con espress"} fs4( |
  e) a,8_\thumb\downbow( b\upbow d4.) c8^1\upbow( |
%bar 570
  b2~ b8) cs^2\upbow( \tuplet 3/2 { e d cs } | \break

  b2\downbow) r4 r8. e,16_1 |
  e2.-> fs4( |
  e) a,8_0( b d4.) cs8_1( |
  b2~ b8) cs8_1( e16 d cs d) |
%bar 575
  b2 r4 r8. e16--\mf | \pageBreak

%page break

  e2~ \tuplet 3/2 { e8 a_3( gs fs_4 e ds) } | \tupletSpan \default
  e2~ \tuplet 6/4 { e8[ a-._3( gs-.\upbow fs-. e-. ds-._2]) } |
  d!2_1~ \tuplet 6/4 { d8[ g_4( f e_4 d cs]) } |
  d2~ \tuplet 6/4 { d8[ g-.( f-.\upbow e-. d-._3 cs-.]) } | \break

%bar 580
  c2_1~ \tuplet 6/4 { c8[( f_2 e d c b]) } |
  c2~ \tuplet 6/4 { c8[( f-. e-. d-. c-. b-.]) } |
  \mark \default
  c2 f4._4( e8-.) |
  d2. g8_4( f) |
  e2_\markup{\italic"cresc."} a4._3( g8) | \break

%bar 585
  f2. bf8_3( a) | \tupletSpan 4
  \tuplet 3/2 { gs-._1_\markup{\dynamic"f"\italic"spiccato"} b-. gs-.
    fs-._2 gs-.  fs-._\markup{\italic"dim."} \omit TupletNumber e-. fs-. e-.
    b-._2^\markup{\small"II"} gs-. b-. } | \clef bass
  \tuplet 3/2 { gs-._1 b-. gs-. fs-._2 gs-. fs-. e-. fs-. e-. b-. cs-. b-. } | \break

  gs!4._4( g16 f) af'4.^4( g16 f) |
  c4._4( b16 a) c'4.^4( b16 a) |
%bar 590
  \tuplet 3/2 { g8-.^2 a-. g-. c-.^1 d-. c-. g-. a-. g-. c-. d-. c-. } | \break

  \tuplet 3/2 { a-.^2 b-. a-. d-.^1 e-. d-. a-. b-. a-. d-. e-. d-. } |
  \clef treble
  \tuplet 3/2 { b-._3 c-. b-. e-._1 f-. e-. b-. c-. b-. e-. f-. e-. } | \break

  \tuplet 3/2 { c-._2 d-. c-. f-._1 g-. f-. c-. d-. c-. } f16( g f_3 e) |
  \mark \default
  ds8. d16( a'2->) fs8_4\downbow( d\upbow) |
%bar 595
  e2( cs'4^3\upbow a_\markup{\italic"dim."}) |
  fs2( b4.^3\upbow a8) | \break

  gs8-.\downbow( fs-.\upbow) e2 fs8_1( gs) |
  a8( gs fs e d_4\upbow cs b a) |
  b2( d4_1\upbow f) | \clef bass
%bar 600
  e8( d cs b a\upbow gs fs e) |
  fs2( a4^1\upbow c) | \break

  b8( a gs fs e\upbow ds cs b) |
  \tuplet 3/2 { e_3( ds cs b_\thumb cs e) e\<( d cs b cs d\!) } |
  \tuplet 3/2 { e\>( ds cs b cs e) e\!( ds e^1 g^3 fs-- e--) } | \break

  \tuplet 3/2 { f( e d^0 c^4 d e) f\<( e d c d e\!) } |
  \tuplet 3/2 { f\>( e d c d e\!) f( e f^1 a g-- f--) } |
  \mark \default \tupletSpan \default \undo \omit TupletNumber
  g8.^1 e16^3\upbow d4\downbow \tuplet 6/4 { r8
    d^0_\markup{\italic"cresc."}[( e f g a] } | \break

  bf8.\downbow) g16\upbow g4\upbow  \tuplet 6/4 { r8 g( a b c cs } |
  d8.\downbow) bf16^2\<( g'8.^4) d16^2( \clef treble
    bf'8._3) g16( d'8.^3\!) bf16( |
%bar 610
  d2) f,4.( e8) |
  e2\< a4_2( g\!) |
  bf2.\ff\>~ bf8. \clef bass d,,16\upbow | \pageBreak

  ds2.^2\> \tuplet 6/4 { ds16^1\pp^\markup{\small"II"}( e^1 fs
    gs as^1^\markup{\small"I"} b) } |
  cs2.^4~ cs8. ds,16\upbow( |
%bar 615
  fs2^2) gs4.^1( ds8^3\upbow) |
  d4.( cs16_2 gs_4) e( gs cs e_1 \tuplet 3/2 {
    gs8^3_\markup{\small"III"}) fs--\downbow e--\upbow } |
  fs4.( cs16_4 a fs_4) a_1( cs_3 fs_1 \tuplet 3/2 {
    a8^4) gs--^4 fs-- } | \break

  gs4.^4_\markup{\italic"cresc."}( es16^1 d!^0) b^2( d e gs
    \tuplet 3/2 { b!8^4) a-- gs-- } |
  a2\<( b4^2\!\upbow bs\f) |
  \mark \default
%bar 620
  cs2^4\p~ cs8 b( as b) |
  cs4( b gs'\upbow e) |
  cs2~ cs8 b( as b) | \break

  cs8( b) e^1( b) gs^4( fs) e8.( b'16^4) |
  b2-> a4.( gs8 |
%bar 625
  fs4) e2-> e4 |
  f( a) d4.^2^\markup{\small"II"}( c8~ |
  c8\<) \clef treble c'4^3-> c-> c-> d16^1\!( e) | \break

  f8\f( e d^3 c) bf( a g_2 f) |
  g2( bf4^1\upbow df) |
%bar 630
  c8_3_\markup{\italic"morendo"}( bf a_0 g_4) f( e d_2 c) |
  d2\>( f4\upbow af\!) | \break

  g8( f e d) \clef tenor c( b a g) |
  a2( c4\upbow ef) |
  \mark \default
  d2\pp~ d4.\upbow d8\upbow |
%bar 635
  g2.\downbow g8.--\upbow g16-.\upbow | \break

  d2\downbow_\markup{\italic"poco a poco cresc."}~ d4.\upbow d8\upbow |
  b2. b8.\upbow( b'16) | \clef treble
  b2~ b4.\upbow b8\upbow |
  e2.^3 e8.-- e16-.\upbow | \break

%bar 640
  b2~ b4.\upbow b8^3\upbow |
  gs2. b8.( a16) |
  gs2\f fs4.( es8) |
  es4( d4._3) cs8-.( bs16 cs e_1 a) | \break

  a2_3 gs4.( fs8) |
%bar 645
  fs4( e~ e8) ds( css16 ds fs_1 bf) |
  bf2\sfz a4.( g8) |
  g2_\markup{\italic"dim."} f4.( e8) | \pageBreak

%page break

  e2 d4.( cs8) |
  cs2 c4.( \grace cs16 b8) | \clef bass
%bar 650
  \mark \default
  b2^3\pp~ b8 a^2( gs a) |
  b4^4( a) fs'^4( d) |
  b2^4~ b8 a( gs a) |
  b4( a) d4.^\markup{\small"II"}( fs,8^3) | \break

  fs2_\markup{\italic"poco a poco cresc."}~ fs8 es^2( ds es) |
%bar 655
  gs4( fs) b4.( a8) |
  a2~ a8 gs^4( fs gs) |
  b4^1( a^0) d4.( cs8) |
  cs2~ cs8 bs^2( as bs) |
  ds4^4\<( cs) fs4.^3\!( e8) | \break

%bar 660
  \mark \default
  e2\f( cs4\upbow a^0) |
  fs2\> b4.^3( a8\!) |
  gs8-. fs-. e2 fs8^1( gs) |
  a4( gs^4) fs( es) |
  fs4.\p( e16 a) fs( a e fs_1 \tuplet 3/2 { a8) g--\downbow fs--\upbow } | \break

%bar 665
  g4.( d16 b) g( b d g_1 \tuplet 3/2 { b8) a-- g-- } |
  a4._\markup{\italic"cresc."}( fs16^4 ds) bs^4( ds^1 fs^3 a^1
    \tuplet 3/2 { c8^4) b-- a-- } |
  bf2\< c4->( cs->\!) |
  \mark \default
  d2^4_\markup{\dynamic"pp"\italic"dolce"}~ d8 c( b! c) | \break

  d4( c) a'^3( f) |
%bar 670
  d2^4~ d8 c( b c) |
  e4( c) f4.( a,8^1) |
  a2_\markup{\italic"cresc. poco a poco"}~ a8 gs^3( fs gs^1) |
  b4^4( a) d4.^1( cs8^3) |
  cs2~ cs8 bs^2( as bs^1) | \break

%bar 675
  ds4^4( cs) fs4.^3( e8) |
  e2~ e8 ds^3( cs ds) |
  fs4^3( e) a4.^3( gs8) |
  a2\fp fs4( d^4) |
  b2 e4.^2( d8) |
%bar 680
  cs8-.\downbow( b-.\upbow) a2 b8( cs) | \break

  d4\>( cs) b\upbow( as) |
  b2.\pp b8.--~ b16-. |
  \mark \default
  fs2. fs4^2^\markup{\small"III"} |
  g2.^3 g4_\markup{\italic"cresc."} |
%bar 685
  fs-> e-> d-> cs-> |
  b8-._\markup{\italic"con molta forza"}[( g-.]) d'-.[( d,-.])
    b'-.[( g-.]) d'-.[( d,-.]) | \break

  \tupletSpan 4
  \tuplet 3/2 { b'( g) g-. d'( d,) d-. b'( g) g-. d'( d,) d-. } |
  b'-.[( g-.]) d'-.[( e,-.]) b'-.[( g-.]) d'-.[( e,-.]) |
  \tuplet 3/2 { b'( g) g-. d'( e,) e-. b'( g) g-. d'( e,) e-. } | \break
  b'8._4 a32( gs! d'8._4) cs32_4( b f'8.^1) e32^1( d af'8.^4) g32( f |
  b8.^4) a32^4( gs d'8.^2_\markup{\italic"cresc."}) cs32^3( b
    \clef treble f'8._4) e32( d af'8._4) g32( f |
  b4->^3\ff) bf16( a gs_3 g) fs4-> f16_2( e ef_3 d) | \pageBreak

%page break

  \clef bass
  cs4-> c16^3( b bf a^0) gs4->^3 g16( fs f^3 e) |
  \repeat tremolo 4 { e16_\markup{\italic"dim."}( ds }
    \repeat tremolo 4 { e ds) } |
  \repeat tremolo 4 { d16^1( e^3^\markup{\italic"rit."} }
    \repeat tremolo 4 { d e )} |
  \mark \default \tempo \markup {"Meno allegro"\small"(quasi recitativo)"}
  cs8\pp r r4 r r8 \clef tenor cs'8-. |
  fs2^3( es8) r r4 | \break

  r2 r4 r8 fs-. |
  b2^3( as8) r r4 |
%bar 700
  r2 r8 b4--\downbow~ b8-.\upbow |
  r2 r8 b4--\downbow~ b8-.\upbow |
  r2 r8 a4\downbow a8\upbow |
  a2\upbow^\markup{\italic"rit. assai"}~ a8 g_\markup{\italic"attacca"}( e^4 c) |
  \bar "||" \time 3/4 \key f \major \break
}

% Local Variables:
% LilyPond-master-file: "Concerto-A-Major.ly"
% End:
