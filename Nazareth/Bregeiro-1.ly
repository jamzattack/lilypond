\version "2.20"
\language "english"

\header {
  title = "Bregeiro"
  composer = "Ernesto Nazareth"
}

\include "1.ly"

breaks = {
  \time 2/4
% bar 1
  \repeat unfold 5 { s2 } \break

% bar 6
  \repeat unfold 5 { s2 } \break

% bar 11
  \repeat unfold 4 { s2 } \break

% bar 15
  \repeat unfold 5 { s2 } \break

% bar 20
  \repeat unfold 5 { s2 } \break

% bar 25
  \repeat unfold 4 { s2 } \break

% bar 29
  \repeat unfold 4 { s2 } \break

% bar 33
  \repeat unfold 5 { s2 } \break

% bar 38
  \repeat unfold 5 { s2 } \break

% bar 43
  \repeat unfold 4 { s2 } \break

% bar 47
  \repeat unfold 4 { s2 } \break

% bar 51
  \repeat unfold 3 { s2 } \break
}

\score {
  \new Staff << \I \breaks >>
}

