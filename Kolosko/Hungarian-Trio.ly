\version "2.19"
\language "english"


\header {
  title = "HUNGARIAN TRIO"
  subtitle = "for Carl & Ben"
  composer = "Nathan Kolosko"
  tagline = ##f
}

\paper {
  #(set-default-paper-size "a4")
}

global = {
  \key c \major
  \time 3/4
  \tempo "Largo"
}

music = \relative c' {
  \global
  \compressFullBarRests
  \clef bass
  R1*3/4*4
  R1*3/4*1^\markup{\italic "rit."}
  r4\fermata r4 b8-- as-- | \bar "||"
  \time 4/4
  fs2^\markup{\italic "a tempo"} ds8 e a!8. g16 |
  fs2 r4 b8 as | \break
%bar 9
  fs2 ds8 e c8. ds16 |
  b4.-> c16( b) as4. fs8 |
  g2.\pp r4\fermata^\markup{\italic "a tempo"} | \bar "||"
  as4\mp^\markup{\italic "pizz."} as b2 |
  as4 as g2 |
  as4 as b2 | \break
%bar 15
  cs2\> <fs c>\! | \bar "||"
  R1^\markup{\italic "rubato"}
  \time 5/4
  R1*5/4
  \time 3/4
  e2\mf^\markup{\italic "arco"}\< fs4 |
  g2\! a4 |
  b2\f g8 fs |
  e4.-- r8 r4 | \break
%bar 22
  r8 e\>^\markup{\italic "rit."}~ e[ e] c b\! |
  e,4. r8 ds''8.--\f e16 | \bar "||"
  \time 4/4
  fs32[( e ds e fs8])~ fs4~ fs8 ds e8.[ g16] | \break
%bar 25
  fs32\(---> e df e fs---> e ds e fs4\)~ fs8 ds e8. as16 |
  fs32\f( e ds8.) \acciaccatura e16 ds4~ ds8 c b[ as] |
  b32( c b8.) \acciaccatura as16\> b4 c2\! | \break
%bar 28
  b4.\f as8 fs4. r8 |
  ds4. g8 fs4. e8 |
  d2 e4. a,8 |
  << {b1} \\ {s4\> s^\markup{\italic "rit."} s s\pp} >> |
  R1 | \bar "|."
  \pageBreak
  a
}

\score {
  \new Staff \music
  \layout {}
  \midi {}
}

