\version "2.19"
\language "english"

Andante-Cantabile = \relative c' {
  \global
  \key f \major
  \time 3/4
  \tempo "Andante cantabile" 4 = 60
  \clef tenor

  df2.->^2\p |
  c2^1~ c8. f,16\upbow | \stemDown
  bf4.^4( a8) g16( a bf g) | \stemNeutral
  e'2^3 f8.^2( g16^1\<) |

%bar 5
  g4->( a->\upbow bf->\upbow\!) |
  bf2\pp a8.( gs16) |
  gs4\downbow\<( e'^3 d\upbow) |
  d2\! c8.^2( b16) | \break


  c4. r8 r8. a16^3 |
%bar 10
  \mark #66
  a4. e8^1( f g) |
  e8.( d32^4 c b4) e,8.( d'16^3^\markup{\small"II"}) |
  d2 c8.( b16) |
  c2 b16^3( c d^1^\markup{\small"I"} e^2) |
  f2 g8.( d16) |
%bar 15
  e4(~ \tuplet 5/4 { e16 d^4 c g e } c8.) d16_1 | \break
  

  e4\<(~ e16 g_1 b^\markup{\italic"rit."} a) g( e\! a^3_\markup{\small"II"} d) |
  \mark \default \tempo \markup{\small"a tempo"}
  df2.^2\pp |
  c4.( f,8) \tuplet 6/4 { f16( g e f g_1 a) } |
  e4.( a8) g16( a b g) |
%bar 20
  e'2^3 f32( g f e f16 g) |
  g4->\downbow( a->_1\upbow b_2\upbow) | \break
  

  b2^3\> gs8.^4( fs16\!) |
  e2 e,32\pp( fs gs a b cs^1 ds e) |
  \mark \default
  f4.^2( e8^3) d16\<( f bf^1 c) |
%bar 25
  d4.\! a8^1( bf c) |
  a8.\>( g32^4 f e4\!) a,8.^1( g'16) | \break
  

  g2 f8.( e16) |
  f2 e16\<( f g^1 a^2) |
  bf2^2\! c8.^3( g16^1) |
%bar 30
  a4~ a16 f^2( c^4 a^4 f8. g16) |
  a4^\markup{\italic"rit."}(~ a16 c e d) f( e a g) | \break
  

  \mark \default \tempo \markup {\small"a tempo"}
  f2.\pp |
  e4.( a,8^0) \tuplet 6/4 { a16( b gs a^0 b c) } |
  d4.( cs8_\markup{\italic"cresc."}) b16\upbow( cs d b) |
  gs'2\upbow a32( b\< a gs a16 b) |
  b4->\upbow cs-> d->\! |
  e4.\downbow( d8\>\upbow b^3 a) |
  g2.\p~ |
  g2. | \pageBreak

%page break

%bar 40
  \mark \default
  g4\pp\(~ g32 e a^0 g^4 e c^2 g e c8.\)( e16_1) |
  e4.( g8^2\upbow_\markup{\small"III"} c^1_\markup{\small"II"} e) |
  e4^2 f8.^3\upbow( d16^2) g4^1\downbow_\markup{\small"I"}~ |
  g4.\< a16^1^\markup{\italic"rit."}( b c8\! g) |
  g4^4 f16^\markup{\italic"a tempo"}( e f g e8.) e16( | \break
  

%bar45
  g4) f16^4( e d g e8.) e16( |
  g4) f16( e d g e8.) e16( |
  g4) f16\>( e d c b a g\! f) |
  \mark \default
  e32\p\downbow[( gs_1 a gs] a b^3 c b c[)
    ds^1\upbow_\markup{\small"I"}( e ds] e gs^1 a gs) a^1[( b c b a8]~ | \break
  

  a16) gs( a^0 e fs) e( fs d^4) d[( c b32 e^4) r e,]^1\upbow |
%bar 50
  e32\p[( gs a gs] a b^3 c b c[) ds^1( e ds] e gs^1 a gs) a^1[( b c b a8]~ | \break
  

  \tupletSpan 4
  \tuplet 6/4 { a16) gs\upbow( a gs a e) fs\downbow( e fs e fs cs)
    d\upbow( cs d b e e,) } |
  c'8.^4 b16( a4) \tuplet 6/4 { e16_1( fs gs a_0 b c) } |
  d8. e16\downbow( f4) e8\upbow( d) | \break
  

  d4^\markup{\italic"stringendo"}(~ d16 c\upbow b c) d\downbow( c b c) |
%bar 55
  f4^4\<(~ f16 ef\downbow d ef) f\upbow( ef d\! ef) |
  \mark \default \tempo \markup{\small"a tempo"}
  af2.^3\pp |
  g2~ g8. c,16-.\upbow |
  f4.^4( e8) d16_\markup{\italic"cresc."}( e f d) | \break
  

  \clef treble
  b'2 c32^2( d c b c16 e) |
%bar 60
  d4\<-> e^1-> f->\upbow |
  g4.\f( f8\upbow)( d^3 bf) |
  bf2._\markup{\dynamic"sf"\italic"dim."}~ |
  bf2. |
  \mark \default
  bf4^2\p(~ bf32 g_0 c bf g_4 ef bf_2 g_4 \clef bass ef8.\upbow)( f16) | \break
  

%bar 65
  g4. \clef treble bf8_2( ef_1 f) |
  g4_2( af8.\upbow f16) bf4\downbow~ |
  bf4. c16^1\upbow( d ef8 bf) |
  bf4^3 af16( g af bf g8.) g16( |
  bf4) af16( g f^\thumb bf g8.) g16\upbow | \break
  

%bar 70
  gf32_1\pp[( df_2_\markup{\small"II"} ef d c16 df])
    f32_\thumb( df_2 c df ef16 f) gf!32( df! ef! df c16\upbow df) | \clef bass
  gf,32^1[( df^2 ef d c df^1 ef df]) f^4( df^2 ef df c df ef df)
  gf^1( df^2 ef df c df ef df) | \break
  

  \mark \default
  c4_\markup{\dynamic"mf"\italic"molto espress."}(~ c16 e\upbow g c)
    \clef treble e\<( g c_3\upbow b\!) |
  bf4.\sfz\downbow( d,8_1\downbow) d8.\upbow\> \clef bass g,,16_3\downbow( |
  c4_1\!) c16( e g\< c) \clef treble e( g c\upbow b\!) |
%bar 75
  bf!4.\downbow\sfz( d,8\downbow) d8.\>( g16) | \break
  

  f4( e8\!) r r8. g16-. |
  g4_\markup{\italic"cresc."}( f8) r r8. af16_2-. |
  af4( g c\upbow) |
  bf4.\< af8->\upbow( g->^4\downbow f->\!\upbow) |
%bar 80
  \mark \default
  e2._2_\markup{\dynamic"pp"\italic"sub."} |
  ef2_1~ ef8. af,16\upbow \pageBreak

%page break

  ds4._3^\markup{\small"II"}( c8) bf16( c d bf) |
  g'2_2^\markup{\small"I"} af32_1( bf af g_\thumb af16 bf) |
  bf4->\< c-> cs^2->\!\upbow |
%bar 85
  d2\f b8._3\>( a16) |
  << g2 { s4 s\mf} >> g,32_4\>( a_0 b c d_1 ef f_1 g) | \break
  

  \mark \default
  af2.\pp |
  g4( af4. g8_3) |
  fs2. |
%bar 90
  f!4( gf4. g8_2) |
  f4_\markup{\italic"sempre"\dynamic"pp"}( df4._3\upbow c8) | \clef bass
  bf4.( f8\downbow) g32( f e f bf^1 df^3 f^1 g) | \clef treble
  a2~ a8. bf16\downbow | \break
  

  c2.\upbow~ |
%bar 95
  \mark \default
  c4\downbow~ c32\f a_\thumb( d c a_0 f_2 c_4 a_1 f8._4\upbow) g16_1\upbow |
  a4._3 c8_4_\markup{\italic"molto espress."}^\markup{\small"I"}( f_2 g) |
  a4_2 bf8.( g16) c4~ |
  c4. d16^1( e f8 c) | \break
  

  c4 bf16( a bf c) a8. a16 |
%bar 100
  c4 bf16_2( a g_\thumb c) a8.\downbow a16 |
  af32\<_\thumb( ef_2^\markup{\small"II"} f ef d16 ef)
  g32_\thumb( ef f ef f16 g) af32_1( ef f\! ef d16\upbow ef) | \break
  

  \clef bass
  af,32^1\ff( ef^2 f ef d ef f ef) g^0( ef^2 f ef d ef f ef) af^1( ef f ef d ef f ef) |
  ef4\ff(~ ef16 g\upbow bf ef) \clef treble g_1( bf ef_3\upbow d) |
  fs2\sfz c8.^3( bf16) |
%bar 105
  a4.--\sfz r8 f8.\ff( g16) | \break
  

  a4-> af4.->\>( ff8_1) |
  \mark \default
  df2.^4\!_\markup{\dynamic"sfp"\italic"dim."}^\markup{\small"II"} |
  c2~ c8. f,16\upbow |
  bf4._4( a8) g16( a bf g) |
%bar 110
  << e'2 { s4 s\< } >> f32_2( g f e f16 g) |
  g4-> a->_2 bf->\!~ |
  bf8 g->_4^\markup{\italic"quasi cadenza"} e-> d->_4 c-> bf-> | \break
  

  \clef bass
  \tuplet 3/2 { g8( e d) c( bf g) f--( e--\upbow d--\upbow) } |
  c4\fermata \tuplet 6/4 { c32( d e f bf_1_\markup{\small"IV"} d_3 } c8_0)
    \tuplet 6/4 { c32^4^\markup{\small"III"}( d^0 e f bf^1^\markup{\small"II"}
    d^3 } c8) | \clef treble
%bar 115
  \clef treble
  \tuplet 6/4 { c32\downbow( d_\thumb e f bf d_\markup{\small"I"} } c8)
    \tuplet 6/4 { d,32_\thumb( e f g bf d } c8)
  \tuplet 6/4 { d,32( f g a bf d } c8) | \break
  

  \tupletSpan 4
  \tuplet 6/4 { fs,16^\markup{\italic"stringendo"}( g a bf d c)
    fs,( g a bf d c) fs,( g a bf d c) } |
  b32^1\<( c df c) b( c d c) b( c df c) b( c d\! c)
  b( c df c) b( c) d\downbow e\upbow | \break
  

  \mark \default
  f2^2\ff \tuplet 6/4 { e16( f g f e f) } |
  f,2_2 \tuplet 6/4 { e16( f g f e f) } | \clef bass
  f,2\> \tuplet 6/4 { e16( f g f e f\!) } |
  << \stemUp{ r4 c2 | r4 c2 | r4 c2~ | c2. }
    \\
     \stemDown { f,2.\p | f_\markup{\italic"dim."} | f~ | f\ppp }
  >> \bar "||" \key a \major \time 6/8 \pageBreak
}

% Local Variables:
% custom-lilypond-master-file: "Concerto-A-Major.ly"
% End:
