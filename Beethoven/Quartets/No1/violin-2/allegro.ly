violin-two-allegro = \new Voice \relative c' {
  \set Staff.instrumentName = #"Violin 2 "

  f4~ f8( g16 f) e8-. f-. |
  c4 r r |

  f4~ f8( g16 f) e8-. f-. |
  d4 r r |

  bf'2.~\< |
  bf2\>( g4\!) |

  f2( bf8. g16) |
  f2( e4) |
  
  
  f4~ f8( g16 f) e8-. f-. |
  c4 r r |

  f4~ f8( g16 f) e8-. f-. |
  d4 r r |

  r2. |
  << e'2.\p { s4\< s\> s\! } >> |

  r2. |
  
}
