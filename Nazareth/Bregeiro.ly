\version "2.20"
\language "english"

\include "1.ly"
\include "2.ly"
\include "3.ly"
\include "4.ly"

\header {
  title = "Bregeiro"
  composer = "Ernesto Nazareth"
}

\score {
  \new StaffGroup <<
    \I
    \II
    \III
    \IV
  >>
  \layout { }
  \midi { }
}
